package com.runeroyale.world.content.battleroyale.costumes;

public enum CostumePowerUp {
    INVISIBILITY, SMUGGLE, LEECH_HP_RANGE, GILDED
}
