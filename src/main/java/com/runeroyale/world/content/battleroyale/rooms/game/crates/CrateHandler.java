package com.runeroyale.world.content.battleroyale.rooms.game.crates;

import com.runeroyale.util.Misc;
import com.runeroyale.util.Stopwatch;
import com.runeroyale.world.content.battleroyale.objects.Crate;
import com.runeroyale.world.content.battleroyale.rooms.game.Game;
import com.runeroyale.world.content.battleroyale.rooms.game.GameEnumerator;
import com.runeroyale.world.content.battleroyale.rooms.game.storm.Storm;
import com.runeroyale.world.content.battleroyale.rooms.game.storm.StormHandler;

import static com.runeroyale.KotlinFunctions.minutesToMillis;

public class CrateHandler {

    private final StormHandler stormHandler;
    private final GameEnumerator gameEnumerator = new GameEnumerator();
    private final Stopwatch transitionTimer = new Stopwatch();
    private boolean isActive = true;

    private static final long DISPENSING_TIME = minutesToMillis(1); // secondsToMillis(6);

    public CrateHandler(StormHandler stormHandler) {
        this.stormHandler = stormHandler;
    }

    public void handleCrateSending(Game game) {
        if (game.isOver()) {
            isActive = false;
        }
        if (transitionTimer.elapsed(DISPENSING_TIME)) {
            if (isActive) {
                sendCratePossiblyForStormState(game);
            }
            transitionTimer.reset();
        }
    }

    private void sendCratePossiblyForStormState(Game game) {
        switch (game.getStorm().getStormState()) {
            case 0:
                sendCratePossibly(game, 20);
                break;
            case 1:
                sendCratePossibly(game, 50);
                break;
            case 2:
                sendCratePossibly(game, 30);
                break;
            case 3:
            case 4:
                sendCratePossibly(game, 15);
                break;
        }
    }

    private void sendCratePossibly(Game game, int probability) {
        int chance = Misc.getRandom(100);
        if (chance < probability) {
            Storm storm = game.getStorm();
            Crate crate = new Crate(storm.getStormCenter().getRandomPostionWithinRadiusClipped(stormHandler.distanceForState(storm.getStormState())));
            if (!game.containsCrate(crate)) {
                sendCrate(game, crate);
            }
        }
    }

    private void sendCrate(Game game, Crate crate) {
        game.registerCrate(crate);
        gameEnumerator.forEachPlayerInGameDoNotAllowNull(game, player ->
                player.getPacketSender().sendMessage("A new crate has spawned!"));
    }
}
