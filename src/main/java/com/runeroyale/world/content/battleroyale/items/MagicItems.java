package com.runeroyale.world.content.battleroyale.items;

import java.util.Collections;
import java.util.List;

public class MagicItems {
    public static final List<Integer> REGULAR_ITEMS = Collections.unmodifiableList(List.of(
            1381, 1383, 1385, 1387,                         // regular elemental staves
            1391, 1393, 1395, 1397, 1399                    // battlestaves
    ));
    public static final List<Integer> CHEST_ONLY_ITEMS = Collections.unmodifiableList(List.of(
            4675,                                           // ancient staff
            15486                                           // staff of light
    ));
    public static final List<Integer> SPECIAL_ITEMS = Collections.unmodifiableList(List.of(
            22207, 22209, 22211, 22213                      // colored staff of lights
    ));
}
