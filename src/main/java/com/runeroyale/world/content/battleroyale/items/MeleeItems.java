package com.runeroyale.world.content.battleroyale.items;

import java.util.Collections;
import java.util.List;

public class MeleeItems {
    public static final List<Integer> REGULAR_ITEMS = Collections.unmodifiableList(List.of(
            1329, 1331, 1333, 4587,                         // scimitars
            1209, 1211, 1213,                               // daggers (no dragon)
            1313, 1315, 1317, 1319                          // 2h swords
    ));
    public static final List<Integer> CHEST_ONLY_ITEMS = Collections.unmodifiableList(List.of(
            4151,                                           // whip
            5698,                                           // dds
            4153                                            // g maul
    ));
    public static final List<Integer> SPECIAL_ITEMS = Collections.unmodifiableList(List.of(
            14484,                                          // dragon claws
            11694, 11696, 11698, 11700                      // godswords
    ));
}
