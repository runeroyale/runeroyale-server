package com.runeroyale.world.content.battleroyale.objects;

import com.runeroyale.model.GameObject;
import com.runeroyale.model.Item;
import com.runeroyale.model.Position;
import com.runeroyale.util.Misc;
import com.runeroyale.world.content.CustomObjects;
import com.runeroyale.world.content.battleroyale.items.MagicItems;
import com.runeroyale.world.content.battleroyale.items.MeleeItems;
import com.runeroyale.world.content.battleroyale.items.MiscellaniousItems;
import com.runeroyale.world.content.battleroyale.items.RangedItems;
import com.runeroyale.world.content.battleroyale.rooms.game.Game;
import com.runeroyale.world.content.battleroyale.rooms.game.GamePopulator;
import com.runeroyale.world.entity.impl.player.Player;

import java.util.Arrays;
import java.util.Objects;

public class Chest extends GameObject {

    private final int CHEST_ITEM_CHANCE = 60;
    private final int REGULAR_ITEM_CHANCE = 35;
    private final int SPECIAL_ITEM_CHANCE = 5;

    private final int ITEMS_IN_CHEST = 4;

    private Item[] items = new Item[ITEMS_IN_CHEST];

    public Chest(int id, Position position, int type, int face) {
        super(id, position, type, face);
    }

    public void open(Player player) {
        Game game = (Game) player.getCurrentRoom();

        populateItems(); // populate the chest only when clicked, as that's more efficient than doing in constructor

        Arrays.stream(items).filter(Objects::nonNull).forEach(item -> // spawn the stuff in the chest
                GamePopulator.spawnGroundItem(player, item, player.getPosition(), game));

        CustomObjects.deleteGlobalObject(this);
        game.registerEmptyChest(getPosition()); // replace chest with a blank game object (so it does no actions)
    }

    private void populateItems() {
        for (int i = 0; i < 2; i++) { // the first two items are random fighting items
            items[i] = randomFightingChestItem();
        }
        items[2] = randomMiscellaniousItem(); // the third item is a misc item
        items[3] = Misc.inclusiveRandom(1) == 0 ? randomMiscellaniousItem() : null;
        // ^ the fourth item has a 50% chance of appearing and would be a misc item if appeared.
    }

    private Item randomFightingChestItem() {
        int type = Misc.inclusiveRandom(2);
        int chance = Misc.getRandom(100);
        boolean chest = chance < CHEST_ITEM_CHANCE;
        boolean regular = chance >= CHEST_ITEM_CHANCE && chance < CHEST_ITEM_CHANCE + REGULAR_ITEM_CHANCE;
        switch (type) {
            case 0:
                return new Item(Misc.randomElement(chest ? MeleeItems.CHEST_ONLY_ITEMS
                        : regular ? MeleeItems.REGULAR_ITEMS
                        : MeleeItems.SPECIAL_ITEMS));
            case 1:
                return new Item(Misc.randomElement(chest ? MagicItems.CHEST_ONLY_ITEMS
                        : regular ? MagicItems.REGULAR_ITEMS
                        : MagicItems.SPECIAL_ITEMS));
            case 2:
                return new Item(Misc.randomElement(chest ? RangedItems.CHEST_ONLY_ITEMS
                        : regular ? RangedItems.REGULAR_ITEMS
                        : RangedItems.SPECIAL_ITEMS));
        }
        return null;
    }

    private Item randomMiscellaniousItem() {
        int chance = Misc.getRandom(100);
        boolean chest = chance < CHEST_ITEM_CHANCE;
        boolean regular = chance >= CHEST_ITEM_CHANCE && chance < CHEST_ITEM_CHANCE + REGULAR_ITEM_CHANCE;
        return Misc.randomElement(chest ? MiscellaniousItems.CHEST_ONLY_ITEMS
                : regular ? MiscellaniousItems.REGULAR_ITEMS
                : MiscellaniousItems.SPECIAL_ITEMS);
    }

    public boolean isAt(Position position) {
        return getPosition().equals(position);
    }

    public Item[] getItems() {
        return items;
    }
}
