package com.runeroyale.world.content.battleroyale.rooms.lobbyroom;

import com.runeroyale.world.content.battleroyale.locations.RoomLocations;
import com.runeroyale.world.content.battleroyale.rooms.Room;
import com.runeroyale.world.entity.impl.player.Player;
import com.runeroyale.world.entity.impl.player.PlayerState;

import java.util.ArrayList;
import java.util.List;

public class LobbyRoom implements Room {
    private List<Player> players = new ArrayList<>();

    @Override
    public void enter(Player player) {
        player.moveTo(RoomLocations.LOBBY);
        player.setState(PlayerState.IN_LOBBY);
    }

    @Override
    public List<Player> getPlayersInRoom() {
        return players;
    }
}
