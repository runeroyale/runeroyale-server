package com.runeroyale.world.content.battleroyale.costumes;

public enum CostumeDefinition {
    CHICKEN("Chicken Costume"),
    NOOB("Noob Costume");

    private String friendlyName;

    CostumeDefinition(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }
}
