package com.runeroyale.world.content.battleroyale.locations;

import com.runeroyale.model.GameObject;
import com.runeroyale.model.Position;
import com.runeroyale.util.Misc;
import com.runeroyale.world.content.battleroyale.objects.Chest;

import java.util.Collections;
import java.util.List;

public class MiscellaniaGameLocation implements GameLocation {
    private final Position NORTH_1 = new Position(2535, 3894);
    private final Position NORTH_2 = new Position(2568, 3891);
    private final Position NORTH_3 = new Position(2546, 3877);
    private final Position EAST_1 = new Position(2604, 3880);
    private final Position EAST_2 = new Position(2614, 3875);
    private final Position EAST_3 = new Position(2602, 3864);
    private final Position EAST_4 = new Position(2616, 3846);
    private final Position SOUTH_1 = new Position(2550, 3853);
    private final Position SOUTH_2 = new Position(2538, 3846);
    private final Position SOUTH_3 = new Position(2525, 3852);
    private final Position WEST_1 = new Position(2508, 3861);
    private final Position WEST_2 = new Position(2516, 3855);
    private final Position WEST_3 = new Position(2517, 3876);
    private final Position WEST_4 = new Position(2517, 3844);

    private final List<Position> NORTH_POSITIONS = Collections.unmodifiableList(List.of(NORTH_1, NORTH_2, NORTH_3));
    private final List<Position> EAST_POSITIONS = Collections.unmodifiableList(List.of(EAST_1, EAST_2, EAST_3, EAST_4));
    private final List<Position> SOUTH_POSITIONS = Collections.unmodifiableList(List.of(SOUTH_1, SOUTH_2, SOUTH_3));
    private final List<Position> WEST_POSITIONS = Collections.unmodifiableList(List.of(WEST_1, WEST_2, WEST_3, WEST_4));

    private final Position ITEM_1 = new Position(2607, 3845);
    private final Position ITEM_2 = new Position(2612, 3841);
    private final Position ITEM_3 = new Position(2618, 3841);
    private final Position ITEM_4 = new Position(2617, 3860);
    private final Position ITEM_5 = new Position(2622, 3872);
    private final Position ITEM_6 = new Position(2616, 3899);
    private final Position ITEM_7 = new Position(2613, 3901);
    private final Position ITEM_8 = new Position(2598, 3897);
    private final Position ITEM_9 = new Position(2588, 3886);
    private final Position ITEM_10 = new Position(2603, 3871);
    private final Position ITEM_11 = new Position(2604, 3882);
    private final Position ITEM_12 = new Position(2611, 3885);
    private final Position ITEM_13 = new Position(2617, 3880);
    private final Position ITEM_14 = new Position(2617, 3879);
    private final Position ITEM_15 = new Position(2615, 3883);
    private final Position ITEM_16 = new Position(2619, 3880);
    private final Position ITEM_17 = new Position(2613, 3862);
    private final Position ITEM_18 = new Position(2611, 3865);
    private final Position ITEM_19 = new Position(2616, 3871);
    private final Position ITEM_20 = new Position(2612, 3870);
    private final Position ITEM_21 = new Position(2601, 3863);
    private final Position ITEM_22 = new Position(2599, 3873);
    private final Position ITEM_23 = new Position(2571, 3898);
    private final Position ITEM_24 = new Position(2553, 3893);
    private final Position ITEM_25 = new Position(2552, 3890);
    private final Position ITEM_26 = new Position(2552, 3898);
    private final Position ITEM_27 = new Position(2540, 3900);
    private final Position ITEM_28 = new Position(2541, 3896);
    private final Position ITEM_29 = new Position(2533, 3897);
    private final Position ITEM_30 = new Position(2534, 3895);
    private final Position ITEM_31 = new Position(2503, 3885);
    private final Position ITEM_32 = new Position(2513, 3870);
    private final Position ITEM_33 = new Position(2515, 3855);
    private final Position ITEM_34 = new Position(2512, 3848);
    private final Position ITEM_35 = new Position(2507, 3857);
    private final Position ITEM_36 = new Position(2498, 3865);
    private final Position ITEM_37 = new Position(2505, 3846);
    private final Position ITEM_38 = new Position(2501, 3866);
    private final Position ITEM_39 = new Position(2524, 3855);
    private final Position ITEM_40 = new Position(2539, 3857);
    private final Position ITEM_41 = new Position(2538, 3846);
    private final Position ITEM_42 = new Position(2549, 3844);
    private final Position ITEM_43 = new Position(2560, 3845);
    private final Position ITEM_44 = new Position(2545, 3853);
    private final Position ITEM_45 = new Position(2550, 3856);
    private final Position ITEM_46 = new Position(2557, 3852);
    private final Position ITEM_47 = new Position(2572, 3844);
    private final Position ITEM_48 = new Position(2582, 3845);
    private final Position ITEM_49 = new Position(2582, 3852);
    private final Position ITEM_50 = new Position(2573, 3859);
    private final Position ITEM_51 = new Position(2572, 3870);

    private final List<Position> ITEM_POSITIONS = Collections.unmodifiableList(List.of(ITEM_1, ITEM_2, ITEM_3, ITEM_4,
            ITEM_5, ITEM_6, ITEM_7, ITEM_8, ITEM_9, ITEM_10, ITEM_11, ITEM_12, ITEM_13, ITEM_14, ITEM_15, ITEM_16,
            ITEM_17, ITEM_18, ITEM_19, ITEM_20, ITEM_21, ITEM_22, ITEM_23, ITEM_24, ITEM_25, ITEM_26, ITEM_27, ITEM_28,
            ITEM_29, ITEM_30, ITEM_31, ITEM_32, ITEM_33, ITEM_34, ITEM_35, ITEM_36, ITEM_37, ITEM_38, ITEM_39, ITEM_40,
            ITEM_41, ITEM_42, ITEM_43, ITEM_44, ITEM_45, ITEM_46, ITEM_47, ITEM_48, ITEM_49, ITEM_50, ITEM_51));

    private final Chest CHEST_1 = new Chest(CHEST_ID, new Position(2552, 3898), 10, 6);
    private final Chest CHEST_2 = new Chest(CHEST_ID, new Position(2536, 3893), 10, 1);
    private final Chest CHEST_3 = new Chest(CHEST_ID, new Position(2540, 3900), 10, 6);
    private final Chest CHEST_4 = new Chest(CHEST_ID, new Position(2502, 3877), 10, 1);
    private final Chest CHEST_5 = new Chest(CHEST_ID, new Position(2498, 3870), 10, 4);
    private final Chest CHEST_6 = new Chest(CHEST_ID, new Position(2498, 3852), 10, 4);
    private final Chest CHEST_7 = new Chest(CHEST_ID, new Position(2523, 3848), 10, 3);
    private final Chest CHEST_8 = new Chest(CHEST_ID, new Position(2560, 3846), 10, 3);
    private final Chest CHEST_9 = new Chest(CHEST_ID, new Position(2562, 3849), 10, 1);
    private final Chest CHEST_10 = new Chest(CHEST_ID, new Position(2581, 3845), 10, 6);
    private final Chest CHEST_11 = new Chest(CHEST_ID, new Position(2597, 3881), 10, 4);
    private final Chest CHEST_12 = new Chest(CHEST_ID, new Position(2604, 3882), 10, 6);
    private final Chest CHEST_13 = new Chest(CHEST_ID, new Position(2602, 3870), 10, 1);
    private final Chest CHEST_14 = new Chest(CHEST_ID, new Position(2617, 3880), 10, 3);
    private final Chest CHEST_15 = new Chest(CHEST_ID, new Position(2617, 3870), 10, 3);
    private final Chest CHEST_16 = new Chest(CHEST_ID, new Position(2613, 3861), 10, 1);
    private final Chest CHEST_17 = new Chest(CHEST_ID, new Position(2617, 3898), 10, 3);
    private final Chest CHEST_18 = new Chest(CHEST_ID, new Position(2555, 3871), 10, 6);
    private final Chest CHEST_19 = new Chest(CHEST_ID, new Position(2558, 3872), 10, 4);
    private final Chest CHEST_20 = new Chest(CHEST_ID, new Position(2556, 3875), 10, 1);
    private final Chest CHEST_21 = new Chest(CHEST_ID, new Position(2554, 3873), 10, 3);

    private final List<GameObject> CHEST_OBJECTS = Collections.unmodifiableList(List.of(CHEST_1, CHEST_2, CHEST_3,
            CHEST_4, CHEST_5, CHEST_6, CHEST_7, CHEST_8, CHEST_9, CHEST_10, CHEST_11, CHEST_12, CHEST_13, CHEST_14,
            CHEST_15, CHEST_16, CHEST_17, CHEST_18, CHEST_19, CHEST_20, CHEST_21));

    private final Position STORM_CENTER_1 = new Position(2535, 3871);
    private final Position STORM_CENTER_2 = new Position(2554, 3852);
    private final Position STORM_CENTER_3 = new Position(2535, 3886);
    private final Position STORM_CENTER_4 = new Position(2606, 3875);
    private final Position STORM_CENTER_5 = new Position(2516, 3860);

    private final List<Position> STORM_CENTER_POSITIONS = Collections.unmodifiableList(List.of(STORM_CENTER_1,
            STORM_CENTER_2, STORM_CENTER_3, STORM_CENTER_4, STORM_CENTER_5));

    @Override
    public Position getRandomNorthLocation() {
        return NORTH_POSITIONS.get(Misc.getRandom(NORTH_POSITIONS.size() - 1)).getRandomPostionWithinRadiusClipped(DEFAULT_RADIUS);
    }

    @Override
    public Position getRandomEastLocation() {
        return EAST_POSITIONS.get(Misc.getRandom(EAST_POSITIONS.size() - 1)).getRandomPostionWithinRadiusClipped(DEFAULT_RADIUS);
    }

    @Override
    public Position getRandomSouthLocation() {
        return SOUTH_POSITIONS.get(Misc.getRandom(SOUTH_POSITIONS.size() - 1)).getRandomPostionWithinRadiusClipped(DEFAULT_RADIUS);
    }

    @Override
    public Position getRandomWestLocation() {
        return WEST_POSITIONS.get(Misc.getRandom(WEST_POSITIONS.size() - 1)).getRandomPostionWithinRadiusClipped(DEFAULT_RADIUS);
    }

    @Override
    public Position getRandomStormCenter() {
        return STORM_CENTER_POSITIONS.get(Misc.getRandom(STORM_CENTER_POSITIONS.size() - 1));
    }

    @Override
    public List<Position> getItemPositions() {
        return ITEM_POSITIONS;
    }

    @Override
    public List<GameObject> getChestObjects() {
        return CHEST_OBJECTS;
    }

    @Override
    public List<Position> getStormCenters() {
        return STORM_CENTER_POSITIONS;
    }

    @Override
    public String toString() {
        return "Miscellania";
    }
}
