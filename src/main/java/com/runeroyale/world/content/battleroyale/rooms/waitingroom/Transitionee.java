package com.runeroyale.world.content.battleroyale.rooms.waitingroom;

import com.runeroyale.world.content.battleroyale.locations.CardinalDirection;
import com.runeroyale.world.entity.impl.player.Player;

/**
 * Represents a player who is going to be transitioned from the waiting room to the game.
 */
public class Transitionee {
    private final Player player;
    private final CardinalDirection direction;

    Transitionee(Player player, CardinalDirection direction) {
        this.player = player;
        this.direction = direction;
    }

    public Player getPlayer() {
        return player;
    }

    public CardinalDirection getDirectionChosen() {
        return direction;
    }
}
