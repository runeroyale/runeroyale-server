package com.runeroyale.world.content.battleroyale.objects;

import com.runeroyale.world.content.dialogue.DialogueExecutor;
import com.runeroyale.world.content.dialogue.impl.LobbyPortal;
import com.runeroyale.world.entity.impl.player.Player;

import static com.runeroyale.world.content.BonusManager.enabledEquipmentIndices;

public class LobbyObjects {
    public static void handlePortalClick(Player player) {
        boolean mayEnter = playerCanEnter(player);
        DialogueExecutor.startDialogue(player, new LobbyPortal(player, mayEnter));
    }

    private static boolean playerCanEnter(Player player) {
        for (int enabledEquipmentIndex : enabledEquipmentIndices) {
            if (player.getEquipment().get(enabledEquipmentIndex).getId() != -1) {
                return false;
            }
        }
        if (!player.getInventory().isEmpty()) {
            return false;
        }
        return true;
    }
}
