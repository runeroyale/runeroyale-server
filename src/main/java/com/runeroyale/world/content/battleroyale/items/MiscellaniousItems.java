package com.runeroyale.world.content.battleroyale.items;

import com.runeroyale.model.Item;

import java.util.Collections;
import java.util.List;

public class MiscellaniousItems {
    public static final List<Item> REGULAR_ITEMS = Collections.unmodifiableList(List.of(
            new Item(2432),                             // defence potions
            new Item(385, 5)                    // sharks
    ));
    public static final List<Item> CHEST_ONLY_ITEMS = Collections.unmodifiableList(List.of(
            new Item(2442),                             // super defence potions
            new Item(385, 10),                  // sharks
            new Item(15272, 3)                  // rocktails
    ));
    public static final List<Item> SPECIAL_ITEMS = Collections.unmodifiableList(List.of(
            new Item(4049),                             // bandage (like med kit in fortnite)
            new Item(11459),                            // defence mix (1 dose) (gives 100 shield)
            new Item(1965)                              // cabbage (like chug jug LMAO)
    ));
}
