package com.runeroyale.world.content.battleroyale.rooms.game;

import com.runeroyale.world.World;
import com.runeroyale.world.entity.impl.player.Player;

import java.util.Objects;
import java.util.function.Consumer;

public class GameEnumerator {
    public void forEachGameInWorldDoNotAllowNull(Consumer<Game> action) {
        World.getGames().stream().filter(Objects::nonNull).forEach(action);
    }

    public void forEachPlayerInGameDoNotAllowNull(Game game, Consumer<Player> action) {
        game.getPlayersInRoom().stream().filter(Objects::nonNull).forEach(action);
    }
}
