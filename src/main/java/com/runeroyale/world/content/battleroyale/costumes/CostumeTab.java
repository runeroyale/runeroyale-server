package com.runeroyale.world.content.battleroyale.costumes;

import com.runeroyale.model.Flag;
import com.runeroyale.model.definitions.ItemDefinition;
import com.runeroyale.world.content.costumes.CostumeType;
import com.runeroyale.world.content.dialogue.DialogueExecutor;
import com.runeroyale.world.content.dialogue.impl.CostumeSelection;
import com.runeroyale.world.entity.impl.player.Player;
import com.runeroyale.world.entity.impl.player.PlayerState;

import java.util.ArrayList;
import java.util.List;

public class CostumeTab {

    public static boolean clickCostumeTab(Player player, int buttonId) {
        if (player.getState() == PlayerState.IN_GAME || player.getState() == PlayerState.DEAD) {
            return false;
        }
        if (buttonId == -23490) {
            List<Costume> costumes = getCostumeList(player, CostumeType.BOUGHT);
            if (costumes.size() > 0) {
                DialogueExecutor.startDialogue(player, new CostumeSelection(player, costumes));
            }
            return true;
        } else if (buttonId == -23487) {
            List<Costume> costumes = getCostumeList(player, CostumeType.ROYALE_PASS);
            if (costumes.size() > 0) {
                DialogueExecutor.startDialogue(player, new CostumeSelection(player, costumes));
            }
            return true;
        } else if (buttonId == -23484) {
            List<Costume> costumes = getCostumeList(player, CostumeType.FREE_PASS);
            if (costumes.size() > 0) {
                DialogueExecutor.startDialogue(player, new CostumeSelection(player, costumes));
            }
            return true;
        } else if (buttonId == -23483) {
            player.getEquipment().resetItems();
            player.getUpdateFlag().flag(Flag.APPEARANCE);
            return true;
        }
        return false;
    }

    public static void writeToCostumeTab(Player player) {
        sendString(player, 42045, "Purchased Costumes: " + getCostumeList(player, CostumeType.BOUGHT).size());
        sendString(player, 42048, "Royale Pass Costumes: " + getCostumeList(player, CostumeType.ROYALE_PASS).size());
        sendString(player, 42051, "Free Pass Costumes: " + getCostumeList(player, CostumeType.FREE_PASS).size());
    }

    private static List<Costume> getCostumeList(Player player, CostumeType pass) {
        List<Costume> costumes = new ArrayList<>();
        for (String costumeNames : player.getAttributes().getObtainedCostumes()) {
            Costume costume = ItemDefinition.getCostumes().get(costumeNames);
            if (costume.getType().equals(pass)) {
                costumes.add(costume);
            }
        }
        return costumes;
    }

    private static void sendString(Player player, int id, String str) {
        player.getPacketSender().sendString(id, str);
    }
}
