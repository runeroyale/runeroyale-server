package com.runeroyale.world.content.battleroyale.rooms.game;

import com.runeroyale.model.GameObject;
import com.runeroyale.model.GroundItem;
import com.runeroyale.model.Item;
import com.runeroyale.model.Position;
import com.runeroyale.util.Misc;
import com.runeroyale.world.content.battleroyale.items.MagicItems;
import com.runeroyale.world.content.battleroyale.items.MeleeItems;
import com.runeroyale.world.content.battleroyale.items.MiscellaniousItems;
import com.runeroyale.world.content.battleroyale.items.RangedItems;
import com.runeroyale.world.content.battleroyale.objects.Chest;
import com.runeroyale.world.entity.impl.player.Player;

import java.util.Objects;

public class GamePopulator {
    private final int CHEST_PERCENT_CHANCE = 20;
    private final int ITEM_PERCENT_CHANCE = 50;

    public static void spawnGroundItem(Player player, Item item, Position pos, Game game) {
        GroundItem groundItem = new GroundItem(item, pos, player.getAttributes().getUserName(), true, 60 * 60, false, 0);
        game.registerGroundItem(player, groundItem);
    }

    public void spawnChests(Game game) {
        for (GameObject chestObject : game.getLocation().getChestObjects()) {
            Chest chest = (Chest) chestObject;
            if (Misc.getRandom(100) < CHEST_PERCENT_CHANCE) {
                game.registerChest(chest);
            }
        }
    }

    public void spawnRegularItems(Game game) {
        for (Position position : game.getLocation().getItemPositions()) {
            if (Misc.getRandom(100) < ITEM_PERCENT_CHANCE) {
                Item item = randomRegularItem();
                game.getPlayersInRoom().stream().filter(Objects::nonNull).forEach(p -> spawnGroundItem(p, item, position, game));
            }
        }
    }

    private Item randomRegularItem() {
        int type = Misc.inclusiveRandom(3);
        switch (type) {
            case 0:
                return new Item(Misc.randomElement(MeleeItems.REGULAR_ITEMS));
            case 1:
                return new Item(Misc.randomElement(MagicItems.REGULAR_ITEMS));
            case 2:
                return new Item(Misc.randomElement(RangedItems.REGULAR_ITEMS));
            case 3:
                return Misc.randomElement(MiscellaniousItems.REGULAR_ITEMS);
        }
        return null;
    }
}
