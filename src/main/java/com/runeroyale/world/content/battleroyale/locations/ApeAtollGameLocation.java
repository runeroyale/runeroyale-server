package com.runeroyale.world.content.battleroyale.locations;

import com.runeroyale.model.GameObject;
import com.runeroyale.model.Position;
import com.runeroyale.util.Misc;

import java.util.Collections;
import java.util.List;

public class ApeAtollGameLocation implements GameLocation {
    private final Position NORTH_1 = new Position(2738, 2794);
    private final Position NORTH_2 = new Position(2750, 2799);
    private final Position NORTH_3 = new Position(2745, 2777);
    private final Position NORTH_4 = new Position(2718, 2797);
    private final Position EAST_1 = new Position(2790, 2792);
    private final Position EAST_2 = new Position(2803, 2779);
    private final Position EAST_3 = new Position(2760, 2775);
    private final Position EAST_4 = new Position(2781, 2782);
    private final Position SOUTH_1 = new Position(2769, 2750);
    private final Position SOUTH_2 = new Position(2805, 2707);
    private final Position SOUTH_3 = new Position(2780, 2707);
    private final Position SOUTH_4 = new Position(2756, 2695);
    private final Position SOUTH_5 = new Position(2728, 2706);
    private final Position WEST_1 = new Position(2703, 2718);
    private final Position WEST_2 = new Position(2704, 2787);
    private final Position WEST_3 = new Position(2713, 2763);

    private final List<Position> NORTH_POSITIONS = Collections.unmodifiableList(List.of(NORTH_1, NORTH_2, NORTH_3, NORTH_4));
    private final List<Position> EAST_POSITIONS = Collections.unmodifiableList(List.of(EAST_1, EAST_2, EAST_3, EAST_4));
    private final List<Position> SOUTH_POSITIONS = Collections.unmodifiableList(List.of(SOUTH_1, SOUTH_2, SOUTH_3, SOUTH_4, SOUTH_5));
    private final List<Position> WEST_POSITIONS = Collections.unmodifiableList(List.of(WEST_1, WEST_2, WEST_3));

    @Override
    public Position getRandomNorthLocation() {
        return NORTH_POSITIONS.get(Misc.getRandom(NORTH_POSITIONS.size() - 1)).getRandomPostionWithinRadiusClipped(DEFAULT_RADIUS);
    }

    @Override
    public Position getRandomEastLocation() {
        return EAST_POSITIONS.get(Misc.getRandom(EAST_POSITIONS.size() - 1)).getRandomPostionWithinRadiusClipped(DEFAULT_RADIUS);
    }

    @Override
    public Position getRandomSouthLocation() {
        return SOUTH_POSITIONS.get(Misc.getRandom(SOUTH_POSITIONS.size() - 1)).getRandomPostionWithinRadiusClipped(DEFAULT_RADIUS);
    }

    @Override
    public Position getRandomWestLocation() {
        return WEST_POSITIONS.get(Misc.getRandom(WEST_POSITIONS.size() - 1)).getRandomPostionWithinRadiusClipped(DEFAULT_RADIUS);
    }

    @Override
    public Position getRandomStormCenter() {
        return null;
    }

    @Override
    public List<Position> getItemPositions() {
        return null;
    }

    @Override
    public List<GameObject> getChestObjects() {
        return null;
    }

    @Override
    public List<Position> getStormCenters() {
        return null;
    }

    @Override
    public String toString() {
        return "Ape Atoll";
    }
}
