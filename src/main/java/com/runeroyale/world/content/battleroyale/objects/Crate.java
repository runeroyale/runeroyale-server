package com.runeroyale.world.content.battleroyale.objects;

import com.runeroyale.model.GameObject;
import com.runeroyale.model.Item;
import com.runeroyale.model.Position;
import com.runeroyale.util.Misc;
import com.runeroyale.world.content.CustomObjects;
import com.runeroyale.world.content.battleroyale.items.MagicItems;
import com.runeroyale.world.content.battleroyale.items.MeleeItems;
import com.runeroyale.world.content.battleroyale.items.MiscellaniousItems;
import com.runeroyale.world.content.battleroyale.items.RangedItems;
import com.runeroyale.world.content.battleroyale.locations.GameLocation;
import com.runeroyale.world.content.battleroyale.rooms.game.Game;
import com.runeroyale.world.content.battleroyale.rooms.game.GamePopulator;
import com.runeroyale.world.entity.impl.player.Player;

import java.util.Arrays;
import java.util.Objects;

public class Crate extends GameObject {

    private final int ITEMS_IN_CRATE = 4;
    private Item[] items = new Item[ITEMS_IN_CRATE];

    private final int MISCELLANEOUS_CHEST_ITEM_CHANCE = 70;
    private final int MISCELLANEOUS_SPECIAL_ITEM_CHANCE = 30;

    public Crate(Position position) {
        super(GameLocation.CRATE_ID, position);
    }


    public void open(Player player) {
        Game game = (Game) player.getCurrentRoom();

        populateItems();
        Arrays.stream(items).filter(Objects::nonNull).forEach(item ->
                GamePopulator.spawnGroundItem(player, item, getPosition(), game)
        );
        CustomObjects.deleteGlobalObject(this);
    }

    private void populateItems() {
        items[0] = randomSpecialItem();
        items[1] = randomMiscellaniousItem(true);
        for (int i = 2; i <= 3; i++) {
            items[i] = randomMiscellaniousItem(false);
        }
    }

    private Item randomSpecialItem() {
        int type = Misc.inclusiveRandom(2);
        switch (type) {
            case 0:
                return new Item(Misc.randomElement(MeleeItems.SPECIAL_ITEMS));
            case 1:
                return new Item(Misc.randomElement(MagicItems.SPECIAL_ITEMS));
            case 2:
                return new Item(Misc.randomElement(RangedItems.SPECIAL_ITEMS));
        }
        return null;
    }

    private Item randomMiscellaniousItem(boolean chanceOfSpecial) {
        int chance = Misc.getRandom(100);
        boolean chest = chance < MISCELLANEOUS_CHEST_ITEM_CHANCE;
        if (!chanceOfSpecial) {
            chest = true;
        }
        return Misc.randomElement(chest ? MiscellaniousItems.CHEST_ONLY_ITEMS : MiscellaniousItems.SPECIAL_ITEMS);
    }

    public boolean isAt(Position position) {
        return getPosition().equals(position);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Crate) {
            Position position = ((Crate) obj).getPosition();
            return isAt(position);
        }
        return false;
    }
}
