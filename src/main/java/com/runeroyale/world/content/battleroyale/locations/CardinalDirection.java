package com.runeroyale.world.content.battleroyale.locations;

import static com.runeroyale.util.Misc.getRandom;

public enum CardinalDirection {
    NORTH, EAST, SOUTH, WEST;

    public static CardinalDirection getRandomDirection() {
        return values()[getRandom(values().length - 1)];
    }
}
