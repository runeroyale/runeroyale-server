package com.runeroyale.world.content.battleroyale.rooms.waitingroom;

import com.runeroyale.net.SessionState;
import com.runeroyale.world.World;
import com.runeroyale.world.content.battleroyale.GameType;
import com.runeroyale.world.entity.impl.player.Player;
import com.runeroyale.world.entity.impl.player.PlayerState;

import static com.runeroyale.world.content.battleroyale.settings.BattleRoyaleGameSettings.*;

class WaitingRoomValidator {

    boolean playerIsAbsentFromRoom(Player player, WaitingRoom room) {
        return player.getSession().getState().equals(SessionState.LOGGED_OUT) || !player.getState().equals
                (PlayerState.IN_WAITING_ROOM) || player.getCurrentRoom() != room;
    }

    boolean playerRegisteredToAnyWaitingRoomWithGameType(Player player, GameType gameType) {
        if (player.getCurrentRoom() != null) {
            if (player.getCurrentRoom() instanceof WaitingRoom) {
                WaitingRoom room = (WaitingRoom) player.getCurrentRoom();
                return room.getGameType().equals(gameType);
            }
        }
        return false;
    }//sigh im trying to rush sh lol
    //its just nosuchmethoderrors
    //oh dw about that thats just jrebel
    //why not still fix though lol
    // its not an actual problem, just a problem with the hotswap or whatever

    boolean roomIsViable(WaitingRoom room) {
        int playersInRoom = room.getPlayersInRoom().size();
        if (playersInRoom == MAX_GAME_PLAYERS) {
            return true;
        }
        if (room.getTicks() >= MAX_TICKS_TILL_GAME_START || room.getTransitioner().isTransitionTimerActive()) {
            return playersInRoom >= MIN_GAME_PLAYERS && playersInRoom <= MAX_GAME_PLAYERS;
        }
        return false;
    }

    boolean worldHasNoWaitingRoomsForGameType(GameType gameType) {
        if (World.getWaitingRooms().size() == 0) {
            return true;
        } else {
            for (WaitingRoom room : World.getWaitingRooms()) {
                if (room == null)
                    continue;

                if (room.getGameType().equals(gameType))
                    return false;
            }
        }
        return true;
    }
}
