package com.runeroyale.world.content.battleroyale.rooms.spectatorroom;

import com.google.common.collect.Lists;
import com.runeroyale.GameSettings;
import com.runeroyale.model.Flag;
import com.runeroyale.world.content.battleroyale.rooms.Room;
import com.runeroyale.world.entity.impl.player.Player;
import com.runeroyale.world.entity.impl.player.PlayerState;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.runeroyale.world.World.LOBBY;

public class SpectatorRoom implements Room {
    private Object2ObjectOpenHashMap<Player, Player> spectators = new Object2ObjectOpenHashMap<>();
    public static final int SPECTATING_INTERFACE = 6014;
    public static final int EXIT_TEXT_ID = 6020;

    public void addSpectator(Player spectator, Player spectating) {
        spectators.put(spectator, spectating);
        spectator.moveTo(spectating.getPosition());
        followSpectating(spectator, spectating);
        sendSpectatingMessage(spectator, spectating);
    }

    public void removeSpectator(Player spectator) {
        spectators.remove(spectator);
        unfollowSpectating(spectator);
    }

    public void moveSpectator(Player spectator, Player newSpectating) {
        removeSpectator(spectator);
        addSpectator(spectator, newSpectating);
    }

    private void sendSpectatingMessage(Player spectator, Player newSpectating) {
        newSpectating.getPacketSender().sendMessage("@red@" + spectator.getAttributes().getUserName() + " is now spectating you.");
    }

    public List<Player> getSpectators(Player spectating) {
        return spectators.entrySet().stream().filter(playerPlayerEntry -> playerPlayerEntry.getValue() == spectating)
                .map(Map.Entry::getValue).collect(Collectors.toList());
    }

    public Player getSpectating(Player spectator) {
        return spectators.get(spectator);
    }

    private void followSpectating(Player spectator, Player spectating) {
        spectator.setEntityInteraction(spectating);
        spectator.getMovementQueue().setFollowCharacter(spectating);
    }

    private void unfollowSpectating(Player spectator) {
        spectator.setEntityInteraction(null);
        spectator.getMovementQueue().setFollowCharacter(null);
    }

    @Override
    public void enter(Player player) {
        player.getPacketSender().sendWalkableInterface(-1);
        player.setState(PlayerState.DEAD);
        player.setVisible(false);
        player.getUpdateFlag().flag(Flag.APPEARANCE);
        player.getPacketSender().sendTabInterface(GameSettings.INVENTORY_TAB, SPECTATING_INTERFACE);
        player.getPacketSender().sendString(EXIT_TEXT_ID, "     Exit");
    }

    public void moveToLobby(Player player) {
        player.getPacketSender().sendInterfaceRemoval();
        player.getPacketSender().sendWalkableInterface(-1);
        player.setVisible(true);
        player.setCurrentRoom(LOBBY);
        removeSpectator(player);
        player.getPacketSender().sendTabInterface(GameSettings.INVENTORY_TAB, 3213);
        player.getPacketSender().sendInteractionOption("null", 2, true);
        player.getPacketSender().clearMinimapIcons();
        player.getPacketSender().sendPositionalHint(player.getPosition(), -1);
        player.getLastDamagedFromStorm().reset();
        player.restart();
    }

    @Override
    public boolean handleButton(Player player, int buttonId) {
        switch (buttonId) {
            case EXIT_TEXT_ID:
                moveToLobby(player);
                return true;
        }
        return false;
    }

    @Override
    public void handleLogout(Player player) {
        moveToLobby(player);
    }

    @Override
    public List<Player> getPlayersInRoom() {
        return Lists.newArrayList(spectators.keySet());
    }
}
