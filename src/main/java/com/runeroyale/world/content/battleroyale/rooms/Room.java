package com.runeroyale.world.content.battleroyale.rooms;

import com.runeroyale.model.Item;
import com.runeroyale.world.entity.impl.player.Player;

import java.util.List;

public interface Room {
    List<Player> getPlayersInRoom();

    void enter(Player player);

    default void leave(Player player) {
    }

    default void dropItem(Player player, Item item, int itemSlot) {
    }

    default void handleLogout(Player player) {
        getPlayersInRoom().remove(player);
    }

    default void handleDeath(Player killer, Player victim) {
        victim.getInventory().resetItems().refreshItems();
        victim.getEquipment().resetEquipmentBesidesCostume().refreshItems();
    }

    default boolean handleButton(Player player, int buttonId) {
        return false;
    }
}