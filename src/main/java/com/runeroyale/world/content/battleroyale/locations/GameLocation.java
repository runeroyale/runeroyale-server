package com.runeroyale.world.content.battleroyale.locations;

import com.runeroyale.model.GameObject;
import com.runeroyale.model.Position;

import java.util.List;

/**
 * My reason for doing this is because I didn't want to hardcode the location determination using the enums. When you
 * read this, get back to me about this. I couldn't think of a better way.
 */
public interface GameLocation {
    int DEFAULT_RADIUS = 2;
    int CHEST_ID = 13291;
    int CRATE_ID = 21911;

    Position getRandomNorthLocation();

    Position getRandomEastLocation();

    Position getRandomSouthLocation();

    Position getRandomWestLocation();

    Position getRandomStormCenter();

    List<Position> getItemPositions();

    List<GameObject> getChestObjects();

    List<Position> getStormCenters();

    String toString();
}
