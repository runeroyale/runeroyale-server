package com.runeroyale.world.content.battleroyale.rooms;

public interface RoomUpdater {
    void updateRoom();
}