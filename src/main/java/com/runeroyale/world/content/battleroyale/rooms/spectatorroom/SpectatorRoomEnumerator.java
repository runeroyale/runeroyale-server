package com.runeroyale.world.content.battleroyale.rooms.spectatorroom;

import com.runeroyale.world.content.battleroyale.rooms.game.Game;
import com.runeroyale.world.entity.impl.player.Player;

import java.util.Objects;
import java.util.function.Consumer;

public class SpectatorRoomEnumerator {
    public void forEachSpectatorInGameDoNotAllowNull(Game game, Consumer<Player> action) {
        game.getSpectatorRoom().getPlayersInRoom().stream().filter(Objects::nonNull).forEach(action);
    }
}
