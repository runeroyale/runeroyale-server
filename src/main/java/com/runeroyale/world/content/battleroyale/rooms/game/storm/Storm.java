package com.runeroyale.world.content.battleroyale.rooms.game.storm;

import com.runeroyale.model.Position;
import com.runeroyale.util.Stopwatch;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.List;

public class Storm {

    private final Stopwatch transitionTimer;

    private Position stormCenter;
    private int stormState = -1;
    private List<ImmutablePair<Integer, Integer>> corners;

    public Storm() {
        this.transitionTimer = new Stopwatch();
    }

    public Position getStormCenter() {
        return stormCenter;
    }

    public void setStormCenter(Position stormCenter) {
        this.stormCenter = stormCenter;
    }

    public int getStormState() {
        return stormState;
    }

    public void setStormState(int stormState) {
        this.stormState = stormState;
    }

    public Stopwatch getTransitionTimer() {
        return transitionTimer;
    }

    public List<ImmutablePair<Integer, Integer>> getCorners() {
        return corners;
    }

    public void setCorners(List<ImmutablePair<Integer, Integer>> corners) {
        this.corners = corners;
    }
}
