package com.runeroyale.world.content.battleroyale.locations;

import com.runeroyale.model.Position;

public final class RoomLocations {
    public static final Position LOBBY = new Position(3363, 9638);
    public static final Position WAITING_ROOM = new Position(1642, 5600);
}
