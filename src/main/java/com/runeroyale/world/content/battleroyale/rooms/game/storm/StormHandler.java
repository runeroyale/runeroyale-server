package com.runeroyale.world.content.battleroyale.rooms.game.storm;

import com.runeroyale.model.CombatIcon;
import com.runeroyale.model.Hit;
import com.runeroyale.model.Hitmask;
import com.runeroyale.model.Position;
import com.runeroyale.world.content.battleroyale.rooms.game.Game;
import com.runeroyale.world.entity.impl.player.Player;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.ArrayList;
import java.util.List;

import static com.runeroyale.KotlinFunctions.minutesToMillis;
import static com.runeroyale.KotlinFunctions.secondsToMillis;

public class StormHandler {

    public static final long TRANSITION_TIME = minutesToMillis(3); // secondsToMillis(20); //

    public void handleStorm(Game game) {
        if (game.getStorm() == null) {
            addStormToGame(game);
        }

        if (game.getStorm().getTransitionTimer().elapsed(TRANSITION_TIME)) {
            if (game.getStorm().getStormState() == -1) {
                spawnStorm(game, game.getStorm(), distanceForState(0), false);
                game.getStorm().setStormState(0);
            } else {
                shrinkStorm(game);
            }
            game.getStorm().getTransitionTimer().reset();
        }
    }

    public void handleStormDamage(Player player, Game game) {
        if (game.getStorm() == null || game.getStorm().getCorners() == null) {
            return;
        }
        if (!isInStorm(player.getPosition(), game.getStorm()) && !game.isOver()) {
            if (player.getLastDamagedFromStorm().elapsed(secondsToMillis(1))) {
                player.dealDamage(new Hit(damageForState(game.getStorm().getStormState()), Hitmask.RED, CombatIcon.NONE), true);
                player.getLastDamagedFromStorm().reset();
            }
        }
    }

    public void handleStormArrow(Player player, Game game) {
        if (game.getStorm() == null || game.getStorm().getCorners() == null) {
            return;
        }
        if (!isInStorm(player.getPosition(), game.getStorm())) {
            player.getPacketSender().sendPositionalHint(game.getStorm().getStormCenter(), 2);
        } else {
            player.getPacketSender().sendPositionalHint(game.getStorm().getStormCenter(), -1);
            handleNextStormArrow(player, game);
        }
    }

    private boolean isInStorm(Position position, Storm storm, List<ImmutablePair<Integer, Integer>> corners) {
        ImmutablePair<Integer, Integer> rightTop = corners.get(1);
        ImmutablePair<Integer, Integer> leftBottom = corners.get(2);
        return position.getX() <= storm.getStormCenter().getX() + rightTop.left
                && position.getY() <= storm.getStormCenter().getY() + rightTop.right
                && position.getX() >= leftBottom.left + storm.getStormCenter().getX()
                && position.getY() >= leftBottom.right + storm.getStormCenter().getY();
    }

    public boolean isInStorm(Position position, Storm storm) {
        return isInStorm(position, storm, storm.getCorners());
    }

    private boolean isInNextStrom(Player player, Storm storm) {
        return isInStorm(player.getPosition(), storm,
                getPointsForDistanceReturnCorners(distanceForState(storm.getStormState() == -1 ? 1 : storm.getStormState() + 1)).left);
    }

    private void addStormToGame(Game game) {
        Storm storm = new Storm();
        storm.setStormCenter(game.getLocation().getRandomStormCenter());
        game.setStorm(storm);
        game.getStorm().getTransitionTimer().headStart(TRANSITION_TIME);
    }

    private ImmutablePair<List<ImmutablePair<Integer, Integer>>, ImmutablePair<Integer, Integer>[]> getPointsForDistanceReturnCorners(int distance) {
        ImmutablePair<Integer, Integer> leftTop = new ImmutablePair<>(-distance, distance);
        ImmutablePair<Integer, Integer> rightTop = new ImmutablePair<>(distance, distance);
        ImmutablePair<Integer, Integer> leftBottom = new ImmutablePair<>(-distance, -distance);
        ImmutablePair<Integer, Integer> rightBottom = new ImmutablePair<>(distance, -distance);

        List corners = List.of(leftTop, rightTop, leftBottom, rightBottom);

        int lineDistance = ((distance * 2) + 1);

        ArrayList<ImmutablePair<Integer, Integer>> points = new ArrayList<>();

        for (int i = 0; i < lineDistance; i++) {
            points.add(new ImmutablePair<>(leftTop.left + i, leftTop.right));
            points.add(new ImmutablePair<>(rightTop.left, rightTop.right - i));
            points.add(new ImmutablePair<>(rightBottom.left - i, rightBottom.right));
            points.add(new ImmutablePair<>(leftBottom.left, leftBottom.right + i));
        }

        return new ImmutablePair<>(corners, points.toArray(new ImmutablePair[points.size()]));
    }

    private void removeStormMinimapIcons(Game game) {
        game.getPlayersInRoom().forEach(player -> player.getPacketSender().clearMinimapIcons());
    }

    private void handleNextStormArrow(Player player, Game game) {
        if (game.getStorm() == null) {
            return;
        }
        if (!isInNextStrom(player, game.getStorm())) {
            player.getPacketSender().sendPositionalHint(game.getStorm().getStormCenter(), 2);
        } else {
            player.getPacketSender().sendPositionalHint(game.getStorm().getStormCenter(), -1);
        }
    }

    private void shrinkStorm(Game game) {//TODO Make sizes map dynamic.
        Storm storm = game.getStorm();
        removeStormMinimapIcons(game);
        storm.setCorners(getPointsForDistanceReturnCorners(distanceForState(storm.getStormState())).left);
        spawnStorm(game, storm, distanceForState(storm.getStormState() == -1 ? 1 : storm.getStormState() + 1), false);
        spawnStorm(game, storm, distanceForState(storm.getStormState()), true);
        storm.setStormState(storm.getStormState() + 1);
    }

    public int distanceForState(int state) {
        switch (state) {
            case 0:
                return 20;
            case 1:
                return 10;
            case 2:
                return 8;
            case 3:
                return 6;
            case 4:
                return 4;
        }
        return 4;
    }

    private int damageForState(int state) {
        /*switch (state) {
            case 0:
                return 10;
            case 1:
                return 20;
            case 2:
                return 30;
            case 3:
                return 40;
            case 4:
                return 50;
        }
        return 10;*/
        return (state + 1) * 10;
    }

    private void spawnStorm(Game game, Storm storm, int distance, boolean red) {
        ImmutablePair<List<ImmutablePair<Integer, Integer>>, ImmutablePair<Integer, Integer>[]> points = getPointsForDistanceReturnCorners(distance);

        //System.out.println(points.length + ":" + (distance * 2) + 1);

        for (int i = 0; i < points.right.length; i++) {
            int x = storm.getStormCenter().getX() + points.right[i].getLeft();
            int y = storm.getStormCenter().getY() + points.right[i].getRight();
            game.getPlayersInRoom().forEach(player -> {
                if (red) player.getPacketSender().sendRedMinimapIcon(x, y);
                else player.getPacketSender().sendGreenMinimapIcon(x, y);
            });
        }
    }
}
