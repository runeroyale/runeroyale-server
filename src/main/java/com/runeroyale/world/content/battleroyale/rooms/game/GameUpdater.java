package com.runeroyale.world.content.battleroyale.rooms.game;

import com.runeroyale.world.content.battleroyale.rooms.RoomUpdater;
import com.runeroyale.world.content.battleroyale.rooms.game.crates.CrateHandler;
import com.runeroyale.world.content.battleroyale.rooms.game.storm.StormHandler;
import com.runeroyale.world.content.battleroyale.rooms.spectatorroom.SpectatorRoomEnumerator;
import com.runeroyale.world.entity.impl.player.Player;

import java.util.function.Consumer;

public class GameUpdater implements RoomUpdater {
    private final GameEnumerator gameEnumerator = new GameEnumerator();
    private final GamePopulator gamePopulator = new GamePopulator();
    private final SpectatorRoomEnumerator spectatorRoomEnumerator = new SpectatorRoomEnumerator();
    private final StormHandler stormHandler = new StormHandler();
    private final CrateHandler crateHandler = new CrateHandler(stormHandler);

    @Override
    public void updateRoom() {
        forEachGameInWorldDoNotAllowNull(game -> {
            populateGames(game);
            determineWinners(game);
            handleStorm(game);
            handleCrateSending(game);
        });
    }

    private void handleCrateSending(Game game) {
        crateHandler.handleCrateSending(game);
    }

    private void determineWinners(Game game) {
        if (game.getPlayersInRoom().size() == game.getGameType().getWinnerCap() && !game.isOver()) {
            game.setOver(true);
            forEachPlayerInGameDoNotAllowNull(game, winner -> {
                winner.setFreezeDelay(Integer.MAX_VALUE);
                sendVictoryRoyale(winner);
                game.sendExitInventoryInterface(winner);
                winner.getPacketSender().sendRichPressenceDetails("Victory Royale!");
            });
            forEachSpectatorInGameDoNotAllowNull(game, this::sendVictoryRoyale);
        }
    }

    private void forEachGameInWorldDoNotAllowNull(Consumer<Game> action) {
        gameEnumerator.forEachGameInWorldDoNotAllowNull(action);
    }

    private void forEachPlayerInGameDoNotAllowNull(Game game, Consumer<Player> action) {
        gameEnumerator.forEachPlayerInGameDoNotAllowNull(game, action);
    }

    private void forEachSpectatorInGameDoNotAllowNull(Game game, Consumer<Player> action) {
        spectatorRoomEnumerator.forEachSpectatorInGameDoNotAllowNull(game, action);
    }

    private void handleStorm(Game game) {
        stormHandler.handleStorm(game);
        forEachPlayerInGameDoNotAllowNull(game, player -> {
            stormHandler.handleStormDamage(player, game);
            stormHandler.handleStormArrow(player, game);
        });
    }

    private void populateGames(Game game) {
        if (!game.itemsAreSpawned()) {
            gamePopulator.spawnChests(game);
            gamePopulator.spawnRegularItems(game);
            game.setItemsSpawned();
        }
    }

    private void sendVictoryRoyale(Player player) {
        player.getPacketSender().sendInterface(42037);
    }
}
