package com.runeroyale.world.content.battleroyale.rooms;

import com.runeroyale.world.content.battleroyale.rooms.game.GameUpdater;
import com.runeroyale.world.content.battleroyale.rooms.waitingroom.WaitingRoomUpdater;

public class RoomHandler {

    private static RoomUpdater waitingRoomUpdater = new WaitingRoomUpdater();
    private static RoomUpdater gameUpdater = new GameUpdater();

    public static void handleRooms() {
        waitingRoomUpdater.updateRoom();
        gameUpdater.updateRoom();
    }
}