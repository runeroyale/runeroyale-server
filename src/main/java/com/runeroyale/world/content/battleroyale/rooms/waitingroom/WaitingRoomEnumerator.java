package com.runeroyale.world.content.battleroyale.rooms.waitingroom;

import com.runeroyale.world.World;
import com.runeroyale.world.content.battleroyale.GameType;
import com.runeroyale.world.entity.impl.player.Player;
import com.runeroyale.world.entity.impl.player.PlayerState;

import java.util.function.Consumer;

public class WaitingRoomEnumerator {

    public void forEachPlayerInWaitingRoomDoNotAllowNull(WaitingRoom room, Consumer<Player> action) {
        room.getPlayersInRoom().stream().filter(player -> player != null).forEach(action);
    }

    public void forEachPlayerInWorldWithWaitingRoomPlayerStateDoNotAllowNull(Consumer<Player> action) {
        World.getPlayers().stream().filter(player -> player != null && player.getState().equals(PlayerState
                .IN_WAITING_ROOM))
                .forEach(action);
    }

    public void forEachWaitingRoomInWorldDoNotAllowNull(Consumer<WaitingRoom> action) {
        World.getWaitingRooms().stream().filter(room -> room != null).forEach(action);
    }

    public void forEachWaitingRoomInWorldWithGameTypeDoNotAllowNull(Consumer<WaitingRoom> action, GameType gameType) {
        World.getWaitingRooms().stream().filter(room -> room.getGameType().equals(gameType) && room != null)
                .forEach(action);
    }
}