package com.runeroyale.world.content.battleroyale.costumes;

import com.runeroyale.engine.task.Task;
import com.runeroyale.engine.task.TaskManager;
import com.runeroyale.model.*;
import com.runeroyale.model.container.impl.Equipment;
import com.runeroyale.util.Misc;
import com.runeroyale.util.Stopwatch;
import com.runeroyale.world.content.battleroyale.items.MagicItems;
import com.runeroyale.world.content.battleroyale.items.MeleeItems;
import com.runeroyale.world.content.battleroyale.items.RangedItems;
import com.runeroyale.world.content.combat.CombatType;
import com.runeroyale.world.entity.impl.player.Player;

public class CostumePowerUpHandler {

    private final long FIVE_SECONDS = 5000;

    private final long FIVE_MINUTES = 300000;

    private Stopwatch invisibilityTimer = new Stopwatch().headStart(FIVE_MINUTES);

    public void applyPowerUp(Player player, CostumePowerUp powerUp) {
        if (powerUp != null) {
            switch (powerUp) {
            }
        }
    }

    public void handleKillPowerUp(Player player, Player victim, CostumePowerUp powerUp) {
        if (powerUp != null) {
            switch (powerUp) {
                case GILDED:
                    if (Misc.getRandom(100) >= 50) {
                        Item weapon = player.getEquipment().get(Equipment.WEAPON_SLOT);
                        String name = weapon.getDefinition().getName().toLowerCase();
                        int id = weapon.getId();
                        Integer nextId;
                        if ((name.startsWith("dragon") && id != 4587) || name.contains("staff") || name.startsWith("magic") || id == 9185) {
                            return;
                        }
                        if (id == 4587) {
                            nextId = 4151;
                        } else {
                            nextId = Misc.nextElement(MeleeItems.REGULAR_ITEMS, id);
                            if (nextId == null) nextId = Misc.nextElement(RangedItems.REGULAR_ITEMS, id);
                            if (nextId == null) nextId = Misc.nextElement(MagicItems.REGULAR_ITEMS, id);
                        }
                        if (nextId != null) {
                            player.getEquipment().set(Equipment.WEAPON_SLOT, new Item(nextId, weapon.getAmount()));
                            player.getEquipment().refreshItems();
                            player.getUpdateFlag().flag(Flag.APPEARANCE);
                            String nextName = new Item(nextId).getDefinition().getName();
                            player.getPacketSender().sendMessage("@or2@Your costume upgrades your weapon to a " + nextName + "!");
                        }
                    }
            }
        }
    }

    public void handleCombatPowerUp(Player player, Player victim, int damage, CombatType combatType, CostumePowerUp powerUp) {
        if (powerUp != null) {
            switch (powerUp) {
                case LEECH_HP_RANGE:
                    if (combatType == CombatType.RANGED) {
                        if (Misc.getRandom(30) <= 4) {
                            new Projectile(player, victim, 2252, 44, 3, 43, 31, 0).sendProjectile();
                            victim.performGraphic(new Graphic(2253));
                            int amount = Misc.getRandom(20);
                            victim.dealDamage(new Hit(amount, Hitmask.RED, CombatIcon.NONE));
                            victim.heal(amount);
                        }
                    }
                    break;
            }
        }
    }

    public void handleGameStartPowerUp(Player player, CostumePowerUp powerUp) {
        if (powerUp != null) {
            switch (powerUp) {
                case SMUGGLE:
                    player.getInventory().add(2442, 1);
                    player.getInventory().add(15335, 1);
                    player.getPacketSender().sendMessage("@or2@You smuggle some of the \"stuff\" in-game.");
                    // todo make overload not decrease defence
                    break;
            }
        }
    }

    public boolean operatePowerUp(Player player) {
        Costume costume = player.getCostume();

        if (costume == null) {
            return false;
        }

        switch (costume.getPowerUp()) {
            case INVISIBILITY:
                handlePlayerInvisibility(player);
                break;
            // todo royale costume
        }

        return true;
    }

    private void handlePlayerInvisibility(Player player) {
        if (player.isVisible()) {
            if (invisibilityTimer.elapsed(FIVE_MINUTES)) {
                player.setVisible(false);
                player.getUpdateFlag().flag(Flag.APPEARANCE);
                invisibilityTimer.reset();
                TaskManager.submit(new Task((int) FIVE_SECONDS / 600) {

                    @Override
                    protected void execute() {
                        player.setVisible(true);
                        player.getUpdateFlag().flag(Flag.APPEARANCE);
                        stop();
                    }
                });
            } else {
                player.getPacketSender().sendMessage("You can only turn invisible every 5 minutes.");
            }
        }
    }
}
