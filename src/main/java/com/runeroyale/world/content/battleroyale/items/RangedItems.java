package com.runeroyale.world.content.battleroyale.items;

import java.util.Collections;
import java.util.List;

public class RangedItems {
    public static final List<Integer> REGULAR_ITEMS = Collections.unmodifiableList(List.of(
            853, 857,                                       // shortbows (no magic)
            851, 855, 859,                                  // longbows
            9181, 9183, 9185                                // crossbows
    ));
    public static final List<Integer> CHEST_ONLY_ITEMS = Collections.unmodifiableList(List.of(
            861,                                            // magic shortbow
            11235                                           // dark bow
    ));
    public static final List<Integer> SPECIAL_ITEMS = Collections.unmodifiableList(List.of(
            15701, 15702, 15703, 15704,                     // colored dark bows
            12926,                                          // toxic blowpipe
            15241                                           // hand cannon
    ));
}
