package com.runeroyale.world.content.battleroyale.rooms.waitingroom;

import com.runeroyale.world.World;
import com.runeroyale.world.content.battleroyale.GameType;
import com.runeroyale.world.content.battleroyale.rooms.Room;
import com.runeroyale.world.entity.impl.player.Player;
import com.runeroyale.world.entity.impl.player.PlayerState;

import java.util.ArrayList;
import java.util.List;

import static com.runeroyale.world.content.battleroyale.settings.BattleRoyaleGameSettings.MAX_TICKS_TILL_GAME_START;

public class WaitingRoom implements Room {

    private final GameType gameType;
    private final WaitingRoomTransitioner transitioner = new WaitingRoomTransitioner(this);
    private ArrayList<Player> playersInRoom = new ArrayList<>();
    private int ticks;
    private boolean inactive;

    public WaitingRoom(GameType gameType) {
        this.gameType = gameType;
    }

    @Override
    public List<Player> getPlayersInRoom() {
        return playersInRoom;
    }

    @Override
    public void enter(Player player) {
        player.setState(PlayerState.IN_WAITING_ROOM);
    }

    @Override
    public void leave(Player player) {
        playersInRoom.remove(player);
        player.setCurrentRoom(World.LOBBY);
        player.getPacketSender().sendWalkableInterface(-1);
        if (player.getDialogueIterator() != null) player.getDialogueIterator().forceCloseDialogue(player);
    }

    public GameType getGameType() {
        return gameType;
    }

    public WaitingRoomTransitioner getTransitioner() {
        return transitioner;
    }

    public int getTicks() {
        return ticks;
    }

    public void setTicks(int ticks) {
        this.ticks = ticks;
    }

    public void incrementTicks() {
        if (ticks >= MAX_TICKS_TILL_GAME_START) {
            ticks = 0;
        } else {
            ticks++;
        }
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }
}