package com.runeroyale.world.content.battleroyale.settings;

public class BattleRoyaleGameSettings {
    public static int MIN_GAME_PLAYERS = 2;
    public static int MAX_GAME_PLAYERS = 24;
    public static final int MAX_TICKS_TILL_GAME_START = 100; // 5 minutes
}