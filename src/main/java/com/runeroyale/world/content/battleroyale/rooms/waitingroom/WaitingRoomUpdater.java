package com.runeroyale.world.content.battleroyale.rooms.waitingroom;

import com.runeroyale.world.World;
import com.runeroyale.world.content.battleroyale.GameType;
import com.runeroyale.world.content.battleroyale.rooms.RoomUpdater;
import com.runeroyale.world.content.battleroyale.settings.BattleRoyaleGameSettings;
import com.runeroyale.world.content.dialogue.DialogueExecutor;
import com.runeroyale.world.content.dialogue.impl.GameDirectionSelection;
import com.runeroyale.world.entity.impl.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class WaitingRoomUpdater implements RoomUpdater {

    private WaitingRoomEnumerator waitingRoomEnumerator = new WaitingRoomEnumerator();
    private WaitingRoomValidator waitingRoomValidator = new WaitingRoomValidator();

    @Override
    public void updateRoom() {
        forEachWaitingRoomInWorldDoNotAllowNull(room -> {
            room.incrementTicks();
            removeAbsentPlayersFromRoom(room);
            sendViableRoomsToGame(room);
        });
        forEachPlayerWithWaitingRoomPlayerStateDoNotAllowNull(player -> {
            if (playerIsSolo(player)) {
                if (!playerRegisteredToAnyWaitingRoomWithGameType(player, GameType.SOLO)) {
                    registerPlayerToWaitingRoom(player, GameType.SOLO);
                }
            } else {
                if (!playerRegisteredToAnyWaitingRoomWithGameType(player, GameType.DUOS)) {
                    registerPlayerToWaitingRoom(player, GameType.DUOS);
                }
            }
        });
        removeInactiveRooms();
    }

    private void removeInactiveRooms() {
        List<WaitingRoom> inactiveRooms = new ArrayList<>();
        forEachWaitingRoomInWorldDoNotAllowNull(room -> {
            if (room.isInactive()) {
                inactiveRooms.add(room);
            }
        });
        inactiveRooms.forEach(World.getWaitingRooms()::remove);
    }

    private void addPlayerToRoom(Player player, WaitingRoom room) {
        player.setCurrentRoom(room);
        room.getPlayersInRoom().add(player);
    }

    private void addWaitingRoomToWorld(GameType gameType) {
        World.getWaitingRooms().add(new WaitingRoom(gameType));
    }

    private void forEachPlayerInRoomDoNotAllowNull(WaitingRoom room, Consumer<Player> action) {
        waitingRoomEnumerator.forEachPlayerInWaitingRoomDoNotAllowNull(room, action);
    }

    private void forEachPlayerWithWaitingRoomPlayerStateDoNotAllowNull(Consumer<Player> action) {
        waitingRoomEnumerator.forEachPlayerInWorldWithWaitingRoomPlayerStateDoNotAllowNull(action);
    }

    private void forEachWaitingRoomInWorldDoNotAllowNull(Consumer<WaitingRoom> action) {
        waitingRoomEnumerator.forEachWaitingRoomInWorldDoNotAllowNull(action);
    }

    private void forEachWaitingRoomWithGameTypeDoNotAllowNull(Consumer<WaitingRoom> action, GameType gameType) {
        waitingRoomEnumerator.forEachWaitingRoomInWorldWithGameTypeDoNotAllowNull(action, gameType);
    }

    private boolean playerIsAbsentFromRoom(Player player, WaitingRoom room) {
        return waitingRoomValidator.playerIsAbsentFromRoom(player, room);
    }

    private boolean playerIsSolo(Player player) {
        return player.getGameType().equals(GameType.SOLO);
    }

    private boolean playerRegisteredToAnyWaitingRoomWithGameType(Player player, GameType gameType) {
        return waitingRoomValidator.playerRegisteredToAnyWaitingRoomWithGameType(player, gameType);
    }

    private void registerPlayerToWaitingRoom(Player player, GameType gameType) {
        if (worldHasNoWaitingRoomsForGameType(gameType)) {
            addWaitingRoomToWorld(gameType);
        }

        forEachWaitingRoomWithGameTypeDoNotAllowNull(room -> {
            if (!roomIsViable(room)) {
                addPlayerToRoom(player, room);
            }
        }, gameType);
    }

    private void removeAbsentPlayersFromRoom(WaitingRoom room) {
        List<Player> playersToRemove = new ArrayList<>();

        forEachPlayerInRoomDoNotAllowNull(room, player -> {
            if (playerIsAbsentFromRoom(player, room)) {
                playersToRemove.add(player);
            }
        });

        room.getPlayersInRoom().removeAll(playersToRemove);
    }

    private boolean roomIsViable(WaitingRoom room) {
        return waitingRoomValidator.roomIsViable(room);
    }

    private void sendViableRoomsToGame(WaitingRoom room) {
        if (roomIsViable(room)) {
            if (!room.getTransitioner().isTransitionTimerActive()) {
                room.getTransitioner().initializeTransitioner();
                forEachPlayerInRoomDoNotAllowNull(room, player -> DialogueExecutor.startDialogue(player,
                        new GameDirectionSelection(player, room.getTransitioner())));
            } else {
                room.getTransitioner().pulse();
            }
        } else {
            if (room.getTransitioner().isTransitionTimerActive()) {
                room.setTicks(BattleRoyaleGameSettings.MAX_TICKS_TILL_GAME_START);
                room.getTransitioner().abortTransition();
            }
        }
    }

    private boolean worldHasNoWaitingRoomsForGameType(GameType gameType) {
        return waitingRoomValidator.worldHasNoWaitingRoomsForGameType(gameType);
    }
}