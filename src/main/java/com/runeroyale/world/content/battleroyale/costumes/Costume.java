package com.runeroyale.world.content.battleroyale.costumes;

import com.runeroyale.world.content.costumes.CostumeType;

public class Costume {

    private int amuletId = -1;
    private int bootsId = -1;
    private int capeId = -1;
    private String costumeName = "No Skin Costume";
    private int glovesId = -1;
    private int helmId = -1;
    private int platebodyId = -1;
    private int platelegsId = -1;
    private CostumePowerUp powerUp;
    private CostumeType type;

    public int getAmuletId() {
        return amuletId;
    }

    public void setAmuletId(int amuletId) {
        this.amuletId = amuletId;
    }

    public int getBootsId() {
        return bootsId;
    }

    public void setBootsId(int bootsId) {
        this.bootsId = bootsId;
    }

    public int getCapeId() {
        return capeId;
    }

    public void setCapeId(int capeId) {
        this.capeId = capeId;
    }

    public String getCostumeName() {
        return costumeName;
    }

    public void setCostumeName(String costumeName) {
        this.costumeName = costumeName;
    }

    public int getGlovesId() {
        return glovesId;
    }

    public void setGlovesId(int glovesId) {
        this.glovesId = glovesId;
    }

    public int getHelmId() {
        return helmId;
    }

    public void setHelmId(int helmId) {
        this.helmId = helmId;
    }

    public int getPlatebodyId() {
        return platebodyId;
    }

    public void setPlatebodyId(int platebodyId) {
        this.platebodyId = platebodyId;
    }

    public int getPlatelegsId() {
        return platelegsId;
    }

    public void setPlatelegsId(int platelegsId) {
        this.platelegsId = platelegsId;
    }

    public CostumePowerUp getPowerUp() {
        return powerUp;
    }

    public void setPowerUp(CostumePowerUp powerUp) {
        this.powerUp = powerUp;
    }

    public CostumeType getType() {
        return type;
    }

    public void setType(CostumeType type) {
        this.type = type;
    }
}
