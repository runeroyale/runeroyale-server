package com.runeroyale.world.content.battleroyale.costumes;

import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.runeroyale.KotlinFunctions.withAppropriateFilePathing;

public class CostumeDeserializer {

    public Map<String, Costume> deserializeCostumes() {
        String jsonArray = getCostumeJson();
        Map<String, Costume> costumes = new HashMap<>();

        Costume[] rawCostumes = new Gson().fromJson(jsonArray, Costume[].class);

        for (Costume costume : rawCostumes) {
            costumes.put(costume.getCostumeName(), costume);
        }

        return costumes;
    }

    private String getCostumeJson() {
        String costumeJson = null;

        try {
            File file = new File(withAppropriateFilePathing("./data/def/json/costumes.json"));

            List<String> allFileLines = Files.readAllLines(file.toPath());

            StringBuilder builder = new StringBuilder();

            for (String line : allFileLines) {
                builder.append(line);
            }

            costumeJson = builder.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(0);
        }

        return costumeJson;
    }
}
