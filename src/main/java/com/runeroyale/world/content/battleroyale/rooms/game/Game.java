package com.runeroyale.world.content.battleroyale.rooms.game;

import com.runeroyale.GameSettings;
import com.runeroyale.model.GameObject;
import com.runeroyale.model.GroundItem;
import com.runeroyale.model.Item;
import com.runeroyale.model.Position;
import com.runeroyale.model.definitions.ItemDefinition;
import com.runeroyale.util.Misc;
import com.runeroyale.world.World;
import com.runeroyale.world.content.CustomObjects;
import com.runeroyale.world.content.PlayerLogs;
import com.runeroyale.world.content.Sounds;
import com.runeroyale.world.content.battleroyale.GameType;
import com.runeroyale.world.content.battleroyale.locations.GameLocation;
import com.runeroyale.world.content.battleroyale.objects.Chest;
import com.runeroyale.world.content.battleroyale.objects.Crate;
import com.runeroyale.world.content.battleroyale.rooms.Room;
import com.runeroyale.world.content.battleroyale.rooms.game.storm.Storm;
import com.runeroyale.world.content.battleroyale.rooms.spectatorroom.SpectatorRoom;
import com.runeroyale.world.entity.impl.GroundItemManager;
import com.runeroyale.world.entity.impl.player.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static com.runeroyale.world.content.battleroyale.rooms.spectatorroom.SpectatorRoom.EXIT_TEXT_ID;
import static com.runeroyale.world.content.battleroyale.rooms.spectatorroom.SpectatorRoom.SPECTATING_INTERFACE;
import static com.runeroyale.world.entity.impl.player.PlayerState.IN_GAME;

public class Game implements Room {
    private final GameType gameType;
    private final GameLocation location;
    private ArrayList<Chest> chests = new ArrayList<>();
    private ArrayList<Crate> crates = new ArrayList<>();
    private ArrayList<GameObject> emptyChests = new ArrayList<>();
    private ArrayList<GroundItem> groundItems = new ArrayList<>();
    private boolean itemsSpawned;
    private HashMap<Player, Integer> kills = new HashMap<>();
    private boolean over;
    private List<Player> players;
    private SpectatorRoom spectatorRoom = new SpectatorRoom();
    private Storm storm;

    public Game(GameType gameType, GameLocation location, List<Player> players) {
        this.gameType = gameType;
        this.location = location;
        this.players = players;
        World.getSpectatorRooms().add(spectatorRoom);
    }

    @Override
    public void enter(Player player) {
        player.setState(IN_GAME);
        player.getPacketSender().clearMinimapIcons();
        player.getPowerUpHandler().handleGameStartPowerUp(player, player.getCostume().getPowerUp());
    }

    @Override
    public List<Player> getPlayersInRoom() {
        return players;
    }

    @Override
    public boolean handleButton(Player player, int buttonId) {
        if (buttonId == EXIT_TEXT_ID) {
            exitGame(player);
            return true;
        }
        return false;
    }

    @Override
    public void handleDeath(Player killer, Player victim) {
        if (killer != null) {
            kills.merge(killer, 1, Integer::sum);
        }
        resetAndDropItems(victim);
        players.remove(victim);
        Player tempToGetSpectators = killer;
        if (tempToGetSpectators == null) {
            tempToGetSpectators = Misc.randomElement(players);
        }
        Player finalToGetSpectators = tempToGetSpectators;
        finalToGetSpectators.getSpectators().addAll(victim.getSpectators());
        victim.getSpectators().forEach(player -> spectatorRoom.moveSpectator(player, finalToGetSpectators));
        victim.setKillerName(killer != null ? killer.getAttributes().getUserName() : "The Storm");
        finalToGetSpectators.getSpectators().add(victim);
        spectatorRoom.addSpectator(victim, finalToGetSpectators);
        //victim.getCostume().reset(victim);
        victim.setCurrentRoom(spectatorRoom);
        victim.getSpectators().clear();
    }

    @Override
    public void handleLogout(Player player) {
        exitGame(player);
    }

    @Override
    public void dropItem(Player player, Item item, int itemSlot) {
        if (item != null && item.getId() != -1 && item.getAmount() >= 1) {
            player.getInventory().setItem(itemSlot, new Item(-1, 0)).refreshItems();
            GamePopulator.spawnGroundItem(player, item, player.getPosition(), this);
            PlayerLogs.log(player.getAttributes().getUserName(), "Player dropping item: " + (ItemDefinition.forId(item.getId()) != null && ItemDefinition.forId(item.getId()).getName() != null ? ItemDefinition.forId(item.getId()).getName() : item.getId()) + ", amount: " + item.getAmount());
            Sounds.sendSound(player, Sounds.Sound.DROP_ITEM);
        }
    }

    public int getKills(Player player) {
        Integer totalKills = kills.get(player);
        if (totalKills == null) {
            return 0;
        }
        return totalKills;
    }

    public boolean itemsAreSpawned() {
        return itemsSpawned;
    }

    public void openChest(Player player, Position position) {
        Chest chest = getChest(position);
        if (chest != null) {
            chest.open(player);
        }
    }

    public void openCrate(Player player, Position position) {
        Crate crate = getCrate(position);
        if (crate != null) {
            crate.open(player);
            crates.remove(crate);
        }
    }

    public boolean containsCrate(Crate crate) {
        return crates.contains(crate);
    }

    public void registerChest(Chest chest) {
        chests.add(chest);
        CustomObjects.spawnGlobalObject(chest);
    }

    public void registerCrate(Crate crate) {
        crates.add(crate);
        CustomObjects.spawnGlobalObject(crate);
    }

    public void registerEmptyChest(Position position) {
        GameObject object = new GameObject(GameLocation.CHEST_ID + 1, position);
        emptyChests.add(object);
        CustomObjects.spawnGlobalObject(object);
    }

    public void registerGroundItem(Player player, GroundItem groundItem) {
        groundItems.add(groundItem);
        if (GroundItemManager.getGroundItem(player, groundItem.getItem(), groundItem.getPosition()) == null) {
            GroundItemManager.spawnGroundItem(player, groundItem);
        }
    }

    public void sendExitInventoryInterface(Player player) {
        player.getPacketSender().sendTabInterface(GameSettings.INVENTORY_TAB, SPECTATING_INTERFACE);
        player.getPacketSender().sendString(EXIT_TEXT_ID, "     Exit");
    }

    public void setItemsSpawned() {
        this.itemsSpawned = true;
    }

    private void cleanup() {
        Player winner = players.get(0);
        winner.getSpectators().clear();
        for (Chest chest : chests) {
            CustomObjects.deleteGlobalObject(chest);
        }
        for (GroundItem item : groundItems) {
            GroundItemManager.remove(item, true);
        }
        for (GameObject emptyChest : emptyChests) {
            CustomObjects.deleteGlobalObject(emptyChest);
        }
        for (Crate crate : crates) {
            CustomObjects.deleteGlobalObject(crate);
        }
        chests.clear();
        emptyChests.clear();
        groundItems.clear();
        kills.clear();
        players.clear();
        World.getGames().remove(this);
    }

    /**
     * exitGame determines if we should physically leave the game only or also clear the game.
     */
    private void exitGame(Player player) {
        if (players.size() == 1) {
            handleLastPlayerLeave(player);
        } else {
            player.getSpectators().forEach(spectator -> spectatorRoom.moveSpectator(spectator, Misc.randomElement(players)));
            leaveGame(player);
            players.remove(player);
        }
    }

    private Chest getChest(Position position) {
        return chests.stream().filter(chest -> chest.isAt(position)).findFirst().orElse(null);
    }

    private Crate getCrate(Position position) {
        return crates.stream().filter(crate -> crate.isAt(position)).findFirst().orElse(null);
    }

    private void handleLastPlayerLeave(Player winner) {
        leaveGame(winner);
        cleanup();
    }

    /**
     * Physically leaves the game
     */
    private void leaveGame(Player player) {
        resetAndDropItems(player);
        player.getSpectators().clear();
        spectatorRoom.moveToLobby(player);
    }

    private void resetAndDropItems(Player player) {
        final ArrayList<Item> victimsItems = new ArrayList<Item>();
        victimsItems.addAll(player.getInventory().getValidItems());
        victimsItems.addAll(player.getEquipment().getValidNonCostumeItems());
        player.getInventory().resetItems().refreshItems();
        player.getEquipment().resetEquipmentBesidesCostume().refreshItems();
        for (Item item : victimsItems) {
            getPlayersInRoom().stream().filter(Objects::nonNull).forEach(p -> GamePopulator.spawnGroundItem(p, item,
                    player.getPosition(), this));
        }
    }

    public GameType getGameType() {
        return gameType;
    }

    public GameLocation getLocation() {
        return location;
    }

    public SpectatorRoom getSpectatorRoom() {
        return spectatorRoom;
    }

    public Storm getStorm() {
        return storm;
    }

    public void setStorm(Storm storm) {
        this.storm = storm;
    }

    public boolean isOver() {
        return over;
    }

    public void setOver(boolean over) {
        this.over = over;
    }
}
