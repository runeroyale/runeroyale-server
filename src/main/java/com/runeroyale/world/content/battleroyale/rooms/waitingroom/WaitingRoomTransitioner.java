package com.runeroyale.world.content.battleroyale.rooms.waitingroom;

import com.runeroyale.util.Stopwatch;
import com.runeroyale.world.World;
import com.runeroyale.world.content.battleroyale.locations.CardinalDirection;
import com.runeroyale.world.content.battleroyale.locations.GameLocation;
import com.runeroyale.world.content.battleroyale.locations.MiscellaniaGameLocation;
import com.runeroyale.world.content.battleroyale.rooms.game.Game;
import com.runeroyale.world.entity.impl.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WaitingRoomTransitioner {
    private final WaitingRoom room;
    private final ArrayList<Transitionee> transitionees = new ArrayList<>();
    private GameLocation gameLocation;
    private Stopwatch transitionTimer;

    public static final int TRANSITION_TIME = 10000;

    private int northernPeople = 0;
    private int easternPeople = 0;
    private int southernPeople = 0;
    private int westernPeople = 0;

    public WaitingRoomTransitioner(WaitingRoom room) {
        this.room = room;
    }

    public Transitionee addTransitionee(Player player, CardinalDirection direction) {
        Transitionee transitionee = new Transitionee(player, direction);
        transitionees.add(transitionee);
        return transitionee;
    }

    public void removeTransitionee(Transitionee transitionee) {
        transitionees.remove(transitionee);
    }

    public void initializeTransitioner() {
        gameLocation = new MiscellaniaGameLocation();
        // todo in future, make this a random game location
        pulse();
    }

    public void pulse() {
        if (getTransitionTimer().elapsed(TRANSITION_TIME)) {
            transition();
        }
    }

    private void chooseForUndecidedPlayers() {
        List<Player> decidedPlayers = transitionees.stream().map(Transitionee::getPlayer).collect(Collectors.toList());
        List<Player> undecidedPlayers = room.getPlayersInRoom().stream().filter(player -> !decidedPlayers.contains
                (player)).collect(Collectors.toList());
        undecidedPlayers.forEach(player -> addTransitionee(player, CardinalDirection.getRandomDirection()));
    }

    private Stopwatch createNewTransitionTimer() {
        transitionTimer = new Stopwatch();
        return transitionTimer;
    }

    private void transition() {
        chooseForUndecidedPlayers();
        transport();
        resetTransitionTimer();
        clearTransitionees();
    }

    public void abortTransition() {
        for (Player player : room.getPlayersInRoom()) {
            player.getDialogueIterator().forceCloseDialogue(player);
        }
        clearTransitioner();
    }

    private void clearTransitioner() {
        transitionees.clear();
        transitionTimer = null;
        northernPeople = 0;
        easternPeople = 0;
        southernPeople = 0;
        westernPeople = 0;
    }

    private void clearTransitionees() {
        transitionees.clear();
    }

    private void transport() {
        List<Player> players = room.getPlayersInRoom();
        Game newGame = new Game(room.getGameType(), gameLocation, players);
        for (Transitionee transitionee : transitionees) {
            Player player = transitionee.getPlayer();
            switch (transitionee.getDirectionChosen()) {
                case NORTH:
                    player.moveTo(gameLocation.getRandomNorthLocation());
                    break;
                case EAST:
                    player.moveTo(gameLocation.getRandomEastLocation());
                    break;
                case SOUTH:
                    player.moveTo(gameLocation.getRandomSouthLocation());
                    break;
                case WEST:
                    player.moveTo(gameLocation.getRandomWestLocation());
                    break;
            }
            player.setCurrentRoom(newGame);
            player.getDialogueIterator().forceCloseDialogue(player);
        }
        clearTransitioner();
        addGameToWorld(newGame);
        markRoomInactive(room);
    }

    private void markRoomInactive(WaitingRoom room) {
        room.setInactive(true);
    }

    public void updateDialogueForEveryone() {
        for (Player player : room.getPlayersInRoom()) {
            player.getDialogueIterator().reloadDialogue();
        }
    }

    private void addGameToWorld(Game game) {
        World.getGames().add(game);
    }

    public GameLocation getGameLocation() {
        return gameLocation;
    }

    public Stopwatch getTransitionTimer() {
        return transitionTimer == null ? createNewTransitionTimer() : transitionTimer;
    }

    public boolean isTransitionTimerActive() {
        return transitionTimer != null;
    }

    private void resetTransitionTimer() {
        transitionTimer = null;
    }

    public void incrementNorthernPeople() {
        northernPeople++;
    }

    public void incrementEasternPeople() {
        easternPeople++;
    }

    public void incrementSouthernPeople() {
        southernPeople++;
    }

    public void incrementWesternPeople() {
        westernPeople++;
    }

    public void decrementNorthernPeople() {
        northernPeople--;
    }

    public void decrementEasternPeople() {
        easternPeople--;
    }

    public void decrementSouthernPeople() {
        southernPeople--;
    }

    public void decrementWesternPeople() {
        westernPeople--;
    }


    public int getNorthernPeople() {
        return northernPeople;
    }

    public int getEasternPeople() {
        return easternPeople;
    }

    public int getSouthernPeople() {
        return southernPeople;
    }

    public int getWesternPeople() {
        return westernPeople;
    }
}
