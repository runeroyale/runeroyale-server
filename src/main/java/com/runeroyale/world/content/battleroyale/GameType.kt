package com.runeroyale.world.content.battleroyale

enum class GameType(val winnerCap: Int) {
    SOLO(1), DUOS(2);

    override fun toString(): String {
        return name.toLowerCase().capitalize()
    }
}
