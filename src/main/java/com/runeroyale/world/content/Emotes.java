package com.runeroyale.world.content;

import com.runeroyale.model.Animation;
import com.runeroyale.model.Graphic;
import com.runeroyale.world.entity.impl.player.Player;

/**
 * Handles emotes
 *
 * @author Gabriel Hannason
 */
public class Emotes {

    public static boolean doEmote(final Player player, int buttonId) {
        EmoteData emoteData = EmoteData.forButton(buttonId);
        if (emoteData != null) {
            if (player.getAttributes().getUnlockedEmotes().contains(emoteData)) {
                animation(player, emoteData.animation, emoteData.graphic);
                return true;
            } else {
                player.getPacketSender().sendMessage("You haven't unlocked this emote!");
            }
        }
        return false;
    }

    public static void animation(Player player, Animation anim, Graphic graphic) {
        if (player.getCombatBuilder().isAttacking() || player.getCombatBuilder().isBeingAttacked()) {
            player.getPacketSender().sendMessage("You cannot do this right now.");
            return;
        }
        if (PlayerPunishment.Jail.isJailed(player)) {
            player.getPacketSender().sendMessage("Use your words.");
            return;
        }
        if (!player.getEmoteDelay().elapsed(3100)) {
            player.getPacketSender().sendMessage("You must wait a bit before performing another emote.");
            return;
        }
        player.getMovementQueue().reset();
        if (anim != null)
            player.performAnimation(anim);
        if (graphic != null)
            player.performGraphic(graphic);
        player.getEmoteDelay().reset();
    }

    public enum EmoteData {
        YES(168, new Animation(855), null),
        NO(169, new Animation(856), null),
        BOW(164, new Animation(858), null),
        ANGRY(165, new Animation(859), null),
        THINK(162, new Animation(857), null),
        WAVE(163, new Animation(863), null),
        SHRUG(13370, new Animation(2113), null),
        CHEER(171, new Animation(862), null),
        BECKON(167, new Animation(864), null),
        LAUGH(170, new Animation(861), null),
        JUMP_FOR_JOY(13366, new Animation(2109), null),
        YAWN(13368, new Animation(2111), null),
        DANCE(166, new Animation(866), null),
        JIG(13363, new Animation(2106), null),
        SPIN(13364, new Animation(2107), null),
        HEADBANG(13365, new Animation(2108), null),
        CRY(161, new Animation(860), null),
        KISS(11100, new Animation(1374), new Graphic(1702)),
        PANIC(13362, new Animation(2105), null),
        RASPBERRY(13367, new Animation(2110), null),
        CRAP(172, new Animation(865), null),
        SALUTE(13369, new Animation(2112), null),
        GOBLIN_BOW(13383, new Animation(2127), null),
        GOBLIN_SALUTE(13384, new Animation(2128), null),
        GLASS_BOX(667, new Animation(1131), null),
        CLIMB_ROPE(6503, new Animation(1130), null),
        LEAN(6506, new Animation(1129), null),
        GLASS_WALL(666, new Animation(1128), null),
        ZOMBIE_WALK(18464, new Animation(3544), null),
        ZOMBIE_DANCE(18465, new Animation(3543), null),
        SCARED(15166, new Animation(2836), null),
        RABBIT_HOP(18686, new Animation(6111), null);

        public Animation animation;
        public Graphic graphic;
        private int button;

        EmoteData(int button, Animation animation, Graphic graphic) {
            this.button = button;
            this.animation = animation;
            this.graphic = graphic;
        }

        private static EmoteData forButton(int button) {
            for (EmoteData data : EmoteData.values()) {
                if (data != null && data.button == button)
                    return data;
            }
            return null;
        }
    }
}
