package com.runeroyale.world.content.skill.impl.construction;

import com.runeroyale.model.Position;
import com.runeroyale.world.World;
import com.runeroyale.world.entity.impl.npc.NPC;
import com.runeroyale.world.entity.impl.player.Player;

/**
 * @author Owner Blade
 */
public class Servant extends NPC {

    private boolean fetching, greetVisitors;
    private int[] inventory;

    public Servant(int npcId, Position position) {
        super(npcId, position);
    }

    public Servant(int npcId, Position position, int inventorySize) {
        super(npcId, position);
        inventory = new int[inventorySize];
    }

    public boolean addInventoryItem(int itemId) {
        for (int i = 0; i < inventory.length; i++) {
            if (inventory[i] == 0) {
                inventory[i] = itemId;
                return true;
            }
        }
        return false;
    }

    public int freeSlots() {
        int value = 0;
        for (int i = 0; i < inventory.length; i++) {
            if (inventory[i] == 0) {
                value++;
            }
        }
        return value;
    }

    @Override
    public void appendDeath() {
        World.deregister(this);
        getRegionInstance().remove(this);
        putBackInBank(getRegionInstance().getOwner());
    }

    public void putBackInBank(Player p) {
    }

    public void giveItems(Player p) {
        for (int i = 0; i < inventory.length; i++) {
            if (p.getInventory().getFreeSlots() == 0)
                break;
            p.getInventory().add(inventory[i], 1);
            inventory[i] = 0;
        }
    }

    public int[] getInventory() {
        return inventory;
    }

    public boolean isFetching() {
        return fetching;
    }

    public void setFetching(boolean fetching) {
        this.fetching = fetching;
    }

    public boolean isGreetVisitors() {
        return greetVisitors;
    }

    public void setGreetVisitors(boolean greetVisitors) {
        this.greetVisitors = greetVisitors;
    }


}
