package com.runeroyale.world.content.skill.impl.runecrafting;

import com.runeroyale.world.entity.impl.player.Player;

public class RunecraftingPouches {

    private static final int RUNE_ESS = 1436, PURE_ESS = 7936;

    public static void fill(Player p, RunecraftingPouch pouch) {
        if (p.getInterfaceId() > 0) {
            p.getPacketSender().sendMessage("Please close the interface you have open before doing this.");
            return;
        }
        int rEss = p.getInventory().getAmount(RUNE_ESS);
        int pEss = p.getInventory().getAmount(PURE_ESS);
        if (rEss == 0 && pEss == 0) {
            p.getPacketSender().sendMessage("You do not have any essence in your inventory.");
            return;
        }
        rEss = rEss > pouch.maxRuneEss ? pouch.maxRuneEss : rEss;
        pEss = pEss > pouch.maxPureEss ? pouch.maxPureEss : pEss;
        int stored = 0;
        if (stored > 0)
            p.getPacketSender().sendMessage("You fill your pouch with " + stored + " essence..");
    }

    public static void empty(Player p, RunecraftingPouch pouch) {
        if (p.getInterfaceId() > 0) {
            p.getPacketSender().sendMessage("Please close the interface you have open before doing this.");
            return;
        }
    }

    public enum RunecraftingPouch {
        SMALL(5509, 3, 3),
        MEDIUM_POUCH(5510, 9, 9),
        LARGE_POUCH(5512, 18, 18),
        GIANT_POUCH(5514, 30, 30);

        private int id;
        private int maxRuneEss, maxPureEss;

        RunecraftingPouch(int id, int maxRuneEss, int maxPureEss) {
            this.id = id;
            this.maxRuneEss = maxRuneEss;
            this.maxPureEss = maxPureEss;
        }

        public static RunecraftingPouch forId(int id) {
            for (RunecraftingPouch pouch : RunecraftingPouch.values()) {
                if (pouch.id == id)
                    return pouch;
            }
            return null;
        }
    }
}
