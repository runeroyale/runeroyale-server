package com.runeroyale.world.content.combat.strategy.impl;

import com.runeroyale.model.definitions.NpcDefinition;
import com.runeroyale.world.content.combat.CombatContainer;
import com.runeroyale.world.content.combat.CombatType;
import com.runeroyale.world.content.combat.strategy.CombatStrategy;
import com.runeroyale.world.entity.impl.Character;

public class IceQueen implements CombatStrategy {

    public static NpcDefinition npcdef = NpcDefinition.forId(795);

    @Override
    public boolean canAttack(Character entity, Character victim) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public CombatContainer attack(Character entity, Character victim) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean customContainerAttack(Character entity, Character victim) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int attackDelay(Character entity) {
        return npcdef.getAttackSpeed();
    }

    @Override
    public int attackDistance(Character entity) {
        return npcdef.getSize();
    }

    @Override
    public CombatType getCombatType() {
        return CombatType.MIXED;
    }

}
