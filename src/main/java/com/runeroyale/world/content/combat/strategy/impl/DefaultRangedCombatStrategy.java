package com.runeroyale.world.content.combat.strategy.impl;

import com.runeroyale.engine.task.Task;
import com.runeroyale.engine.task.TaskManager;
import com.runeroyale.model.*;
import com.runeroyale.model.RegionInstance.RegionInstanceType;
import com.runeroyale.model.container.impl.Equipment;
import com.runeroyale.model.definitions.WeaponAnimations;
import com.runeroyale.util.Misc;
import com.runeroyale.world.content.combat.CombatContainer;
import com.runeroyale.world.content.combat.CombatContainer.ContainerHit;
import com.runeroyale.world.content.combat.CombatFactory;
import com.runeroyale.world.content.combat.CombatType;
import com.runeroyale.world.content.combat.effect.CombatPoisonEffect.PoisonType;
import com.runeroyale.world.content.combat.range.CombatRangedAmmo.AmmunitionData;
import com.runeroyale.world.content.combat.range.CombatRangedAmmo.RangedWeaponData;
import com.runeroyale.world.content.combat.strategy.CombatStrategy;
import com.runeroyale.world.content.combat.weapon.FightStyle;
import com.runeroyale.world.entity.impl.Character;
import com.runeroyale.world.entity.impl.npc.NPC;
import com.runeroyale.world.entity.impl.player.Player;

/**
 * The default combat strategy assigned to an {@link Character} during a ranged
 * based combat session.
 *
 * @author lare96
 */
public class DefaultRangedCombatStrategy implements CombatStrategy {

    public static void fireProjectile(Character e, Character victim, final AmmunitionData ammo, boolean dBow) {
        TaskManager.submit(new Task(1, e.getCombatBuilder(), false) { //TODO FIX THIS PROJECTILE, SHOULDNT BE A TASK
            @Override
            protected void execute() {
                new Projectile(e, victim, ammo.getProjectileId(), ammo.getProjectileDelay() + 16, ammo.getProjectileSpeed(), ammo.getStartHeight(), ammo.getEndHeight(), 0).sendProjectile();
                if (dBow) {
                    new Projectile(e, victim, ammo.getProjectileId(), ammo.getProjectileDelay() + 32, ammo.getProjectileSpeed(), ammo.getStartHeight() - 2, ammo.getEndHeight(), 0).sendProjectile();
                }
                stop();
            }
        });
    }

    private static final int getModifiedDamage(Player player, Character target, CombatContainer container) {
        if (container == null || container.getHits().length < 1)
            return 0;
        ContainerHit hit = container.getHits()[0];
        if (!hit.isAccurate())
            return 0;
        int damage = container.getHits()[0].getHit().getDamage();
        int ammo = player.getFireAmmo();
        if (ammo == -1) {
            return damage;
        }
        double multiplier = 1;
        Player pTarget = target.isPlayer() ? ((Player) target) : null;
        switch (ammo) {
            case 9236: // Lucky Lightning
                target.performGraphic(new Graphic(749));
                multiplier = 1.3;
                break;
            case 9237: // Earth's Fury
                target.performGraphic(new Graphic(755));
                multiplier = 1.05;
                break;
            case 9238: // Sea Curse
                target.performGraphic(new Graphic(750));
                multiplier = 1.1;
                break;
            case 9239: // Down To Earth
                target.performGraphic(new Graphic(757));
                if (pTarget != null) {
                    pTarget.getSkillManager().setCurrentLevel(Skill.MAGIC, pTarget.getSkillManager().getCurrentLevel(Skill.MAGIC) - 3);
                    pTarget.getPacketSender().sendMessage("Your Magic level has been reduced.");
                }
                break;
            case 9240: // Clear Mind
                target.performGraphic(new Graphic(751));
                if (pTarget != null) {
                    pTarget.getSkillManager().setCurrentLevel(Skill.PRAYER, pTarget.getSkillManager().getCurrentLevel(Skill.PRAYER) - 40);
                    if (pTarget.getSkillManager().getCurrentLevel(Skill.PRAYER) < 0) {
                        pTarget.getSkillManager().setCurrentLevel(Skill.PRAYER, 0);
                    }
                    pTarget.getPacketSender().sendMessage("Your Prayer level has been leeched.");
                    player.getSkillManager().setCurrentLevel(Skill.PRAYER, pTarget.getSkillManager().getCurrentLevel(Skill.PRAYER) + 40);
                    if (player.getSkillManager().getCurrentLevel(Skill.PRAYER) > player.getSkillManager().getMaxLevel(Skill.PRAYER)) {
                        player.getSkillManager().setCurrentLevel(Skill.PRAYER, player.getSkillManager().getMaxLevel(Skill.PRAYER));
                    } else {
                        player.getPacketSender().sendMessage("Your enchanced bolts leech some Prayer points from your opponent..");
                    }
                }
                break;
            case 9241: // Magical Posion
                target.performGraphic(new Graphic(752));
                CombatFactory.poisonEntity(target, PoisonType.MILD);
                break;
            case 9242:
                if (player.getSkillManager().getCurrentLevel(Skill.CONSTITUTION) - player.getSkillManager().getCurrentLevel(Skill.CONSTITUTION) / 200 < 10) {
                    break;
                }
                int priceDamage = (int) (player.getSkillManager().getCurrentLevel(Skill.CONSTITUTION) * 0.08);
                if (priceDamage < 0) {
                    return damage;
                }
                int dmg2 = (int) ((int) ((int) target.getConstitution() * 0.065) > 1000 ? 650 + Misc.getRandom(50) : ((int) target.getConstitution() * 0.065));
                if (dmg2 <= 0) {
                    return damage;
                }
                target.performGraphic(new Graphic(754));
                player.dealDamage(new Hit(priceDamage, Hitmask.getRedOrBlue(player), CombatIcon.RANGED));
                return dmg2;
            case 9243:
                target.performGraphic(new Graphic(758, GraphicHeight.MIDDLE));
                multiplier = 1.15;
                break;
            case 9244:
                target.performGraphic(new Graphic(756));
                if (pTarget != null && (pTarget.getEquipment().getItems()[Equipment.SHIELD_SLOT].getId() == 1540 || pTarget.getEquipment().getItems()[Equipment.SHIELD_SLOT].getId() == 13655 || pTarget.getEquipment().getItems()[Equipment.SHIELD_SLOT].getId() == 11283 || pTarget.getFireImmunity() > 0)) {
                    return damage;
                }
                if (damage < 300 && Misc.getRandom(3) <= 1) {
                    damage = 300 + Misc.getRandom(150);
                }
                multiplier = 1.25;
                break;
            case 9245:
                target.performGraphic(new Graphic(753));
                multiplier = 1.26;
                int heal = (int) (damage * 0.25) + 10;
                player.heal(heal);
                if (damage < 250 && Misc.getRandom(3) <= 1) {
                    damage += 150 + Misc.getRandom(80);
                }
                //player.getPacketSender().sendMessage("[DEBUG]: Onyx bolts healed: "+heal);
                break;
        }
        return (int) (damage * multiplier);
    }

    @Override
    public boolean canAttack(Character entity, Character victim) {
        return true;
    }

    @Override
    public CombatContainer attack(Character entity, Character victim) {
        if (entity.isNpc()) {
            NPC npc = (NPC) entity;
            AmmunitionData ammo = AmmunitionData.ADAMANT_ARROW;
            switch (npc.getId()) {
                case 688:
                    ammo = AmmunitionData.BRONZE_ARROW;
                    break;
                case 27:
                    ammo = AmmunitionData.STEEL_ARROW;
                    break;
                case 2028:
                    ammo = AmmunitionData.BOLT_RACK;
                    break;
                case 6220:
                case 6256:
                case 6276:
                    ammo = AmmunitionData.RUNE_ARROW;
                    break;
                case 6225:
                    ammo = AmmunitionData.STEEL_JAVELIN;
                    break;
                case 6252:
                    ammo = AmmunitionData.RUNE_ARROW;
                    break;
            }

            entity.performAnimation(new Animation(npc.getDefinition().getAttackAnimation()));

            entity.performGraphic(new Graphic(ammo.getStartGfxId(), ammo.getStartHeight() >= 43 ? GraphicHeight.HIGH : GraphicHeight.MIDDLE));

            fireProjectile(npc, victim, ammo, false);

            return new CombatContainer(entity, victim, 1, CombatType.RANGED, true);
        }

        Player player = (Player) entity;
        final boolean dBow = CombatFactory.darkBow(player);

        player.setFireAmmo(0);

        startAnimation(player);

        AmmunitionData ammo = RangedWeaponData.getAmmunitionData(player);


        if (!player.isSpecialActivated()) {
            player.performGraphic(new Graphic(ammo.getStartGfxId(), ammo.getStartGfxId() == 2138 ? GraphicHeight.LOW : ammo.getStartHeight() >= 43 ? GraphicHeight.HIGH : GraphicHeight.MIDDLE));
            fireProjectile(player, victim, ammo, dBow);
        }

        CombatContainer container = new CombatContainer(entity, victim, dBow ? 2 : 1, CombatType.RANGED, true);

        /** CROSSBOW BOLTS EFFECT **/
        if (player.getEquipment().get(Equipment.WEAPON_SLOT).getDefinition() != null && player.getEquipment().get(Equipment.WEAPON_SLOT).getDefinition().getName().toLowerCase().contains("crossbow")) {
            if (Misc.getRandom(12) >= 10) {
                container.setModifiedDamage(getModifiedDamage(player, victim, container));
            }
        }

        return container;
    }

    @Override
    public int attackDelay(Character entity) {
        return entity.getAttackSpeed();
    }

    @Override
    public int attackDistance(Character entity) {

        // The default distance for all npcs using ranged is 6.
        if (entity.isNpc()) {
            return 6;
        }

        // Create the player instance.

        Player player = (Player) entity;

        int distance = 5;
        if (player.getRangedWeaponData() != null)
            distance = player.getRangedWeaponData().getType().getDistanceRequired();

        if (player.getRegionInstance() != null && (player.getRegionInstance().equals(RegionInstanceType.KRAKEN) || player.getRegionInstance().equals(RegionInstanceType.ZULRAH))) {
            distance += 3;
        }

        return distance + (player.getFightType().getStyle() == FightStyle.DEFENSIVE ? 2 : 0);
    }

    /**
     * Starts the performAnimation for the argued {@link Player} in the current combat
     * hook.
     *
     * @param player the player to start the performAnimation for.
     */
    private void startAnimation(Player player) {
        if (player.getEquipment().get(Equipment.WEAPON_SLOT).getDefinition().getName().startsWith("Karils")) {
            player.performAnimation(new Animation(2075));
        } else {
            player.performAnimation(new Animation(WeaponAnimations.getAttackAnimation(player)));
        }
    }

    /**
     * Checks the ammo to make sure the argued {@link Player} has the right type
     * and amount before attacking.
     *
     * @param player the player's ammo to check.
     * @return <code>true</code> if the player has the right ammo,
     * <code>false</code> otherwise.
     */
    private boolean checkAmmo(Player player) {
        return true;
    }

    @Override
    public boolean customContainerAttack(Character entity, Character victim) {
        return false;
    }

    @Override
    public CombatType getCombatType() {
        return CombatType.RANGED;
    }
}
