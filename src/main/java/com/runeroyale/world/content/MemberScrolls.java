package com.runeroyale.world.content;

import com.runeroyale.model.PlayerRights;
import com.runeroyale.model.definitions.ItemDefinition;
import com.runeroyale.util.Misc;
import com.runeroyale.world.entity.impl.player.Player;

public class MemberScrolls {

    public static void checkForRankUpdate(Player player) {
        if (player.getAttributes().getPlayerRights().isStaff()) {
            return;
        }
        if (player.getAttributes().getPlayerRights().isMember()) {
            return;
        }
        PlayerRights rights = null;
        if (rights != null && rights != player.getAttributes().getPlayerRights()) {
            player.getPacketSender().sendMessage("You've become a " + Misc.formatText(rights.toString().toLowerCase()) + "! Congratulations!");
            player.getAttributes().setPlayerRights(rights);
            player.getPacketSender().sendRights();
        }
    }

    /*

     */

    public static boolean handleScroll(Player player, int item) {
        switch (item) {
            case 6769:
            case 10942:
            case 10934:
            case 10935:
            case 10943:
                PlayerLogs.log(player.getAttributes().getUserName(), "Has just redeemed a " + ItemDefinition.forId(item).getName() + " successfully!");
                int funds = item == 6769 ? 5 : item == 10942 ? 10 : item == 10934 ? 25 : item == 10935 ? 50 : item == 10943 ? 100 : -1;
                player.getInventory().delete(item, 1);
                player.getPointsHandler().setMemberPoints(funds, true);
                player.getPacketSender().sendMessage("Your account has gained funds worth $" + funds + ". Your total is now at $");
                checkForRankUpdate(player);
                PlayerPanel.refreshPanel(player);
                break;
            case 10944:
                if (player.getAttributes().getPlayerRights().isStaff()) {
                    player.getPacketSender().sendMessage("As a staff member, you already have member rights.");
                    PlayerLogs.log(player.getAttributes().getUserName(), "Attempted to redeem a " + ItemDefinition.forId(item).getName() + ", but was a staff member! (Unsuccessful)");
                    break;
                }
                if (player.getAttributes().getPlayerRights().isMember()) {
                    player.getPacketSender().sendMessage("You are a member, that would be wasteful!");
                    PlayerLogs.log(player.getAttributes().getUserName(), "Attempted to redeem a " + ItemDefinition.forId(item).getName() + ", but was already a member! (Unsuccessful)");
                    break;
                } else {
                    PlayerLogs.log(player.getAttributes().getUserName(), "Has just redeemed a " + ItemDefinition.forId(item).getName() + " successfully!");
                    player.getPacketSender().sendMessage("Sending redemption request...");
                    player.getInventory().delete(10944, 1);
                    player.getAttributes().setPlayerRights(PlayerRights.MEMBER);
                    player.getPacketSender().sendMessage("Congratulations! You've upgraded to a member account!");
                    player.getPacketSender().sendRights();
                    PlayerPanel.refreshPanel(player);
                }
                break;
            case 7630:
                PlayerLogs.log(player.getAttributes().getUserName(), "Has just redeemed a " + ItemDefinition.forId(item).getName() + " successfully!");
                if (player.getInventory().getFreeSlots() > 0) {
                    player.getInventory().delete(7630, 1);
                    player.getInventory().add(12934, 1000);
                    player.getPacketSender().sendMessage("You unpack the crate, and find 1000 of Zulrah's Scales inside.");
                } else {
                    player.getPacketSender().sendMessage("You need at least 1 free inventory space to unpack this.");
                }
                break;
        }
        return false;
    }
}
