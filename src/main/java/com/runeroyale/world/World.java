package com.runeroyale.world;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.runeroyale.GameSettings;
import com.runeroyale.model.MessageType;
import com.runeroyale.model.PlayerRights;
import com.runeroyale.util.Misc;
import com.runeroyale.world.content.PlayerLogs;
import com.runeroyale.world.content.battleroyale.rooms.RoomHandler;
import com.runeroyale.world.content.battleroyale.rooms.game.Game;
import com.runeroyale.world.content.battleroyale.rooms.lobbyroom.LobbyRoom;
import com.runeroyale.world.content.battleroyale.rooms.spectatorroom.SpectatorRoom;
import com.runeroyale.world.content.battleroyale.rooms.waitingroom.WaitingRoom;
import com.runeroyale.world.entity.Entity;
import com.runeroyale.world.entity.EntityHandler;
import com.runeroyale.world.entity.impl.CharacterList;
import com.runeroyale.world.entity.impl.GlobalItemSpawner;
import com.runeroyale.world.entity.impl.npc.NPC;
import com.runeroyale.world.entity.impl.player.Player;
import com.runeroyale.world.entity.impl.player.PlayerHandler;
import com.runeroyale.world.entity.updating.NpcUpdateSequence;
import com.runeroyale.world.entity.updating.PlayerUpdateSequence;
import com.runeroyale.world.entity.updating.UpdateSequence;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Phaser;

/**
 * @author Gabriel Hannason
 * Thanks to lare96 for help with parallel updating system
 */
public class World {

    /**
     * The queue of {@link Player}s waiting to be logged in.
     **/
    private static Queue<Player> logins = new ConcurrentLinkedQueue<>();
    /**
     * The queue of {@link Player}s waiting to be logged out.
     **/
    private static Queue<Player> logouts = new ConcurrentLinkedQueue<>();
    /**
     * All of the registered NPCs.
     */
    private static CharacterList<NPC> npcs = new CharacterList<>(GameSettings.npcCharacterListCapacity);
    /**
     * All of the registered players.
     */
    private static CharacterList<Player> players = new CharacterList<>(GameSettings.playerCharacterListCapacity);
    /**
     * Used to block the game thread until updating has completed.
     */
    private static Phaser synchronizer = new Phaser(1);
    /**
     * A thread pool that will update players in parallel.
     */
    private static ExecutorService updateExecutor = Executors.newFixedThreadPool(Runtime.getRuntime()
            .availableProcessors(), new ThreadFactoryBuilder().setNameFormat("UpdateThread").setPriority(Thread
            .MAX_PRIORITY).build());
    /**
     * All of the waiting rooms currently active in-game.
     */
    private static final List<WaitingRoom> WAITING_ROOMS = new ArrayList<>();
    /**
     * All of the GAMES currently active in-game.
     */
    private static final List<Game> GAMES = new ArrayList<>();
    private static final List<SpectatorRoom> SPECTATOR_ROOMS = new ArrayList<>();
    /**
     * The LOBBY in the game.
     */
    public static final LobbyRoom LOBBY = new LobbyRoom();

    public static void deregister(Entity entity) {
        EntityHandler.deregister(entity);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static Player getPlayerByName(String username) {
        Optional<Player> op = players.search(p -> p != null && p.getAttributes().getUserName().equals(Misc.formatText(username)));
        return op.isPresent() ? op.get() : null;
    }

    public static void register(Entity entity) {
        EntityHandler.register(entity);
    }

    public static void savePlayers() {
        players.forEach(p -> p.save());
    }

    public static void sendFilteredMessage(String message) {
        players.stream().filter(p -> p != null && (p.toggledGlobalMessages() == false)).forEach(p -> p
                .getPacketSender().sendMessage(message));
    }

    public static void sendGlobalGroundItems() {
        players.stream().filter(p -> p != null).forEach(p -> GlobalItemSpawner.spawnGlobalGroundItems(p)); //p
        // .getPacketSender().createGroundItem(952, new Position(3571, 3312), 1));
    }

    public static void sendMessage(String message) {
        players.forEach(p -> p.getPacketSender().sendMessage(message));
    }

    public static void sendMessage(MessageType type, String message) {
        players.forEach(p -> p.getPacketSender().sendMessage(type, message));
    }

    public static void sendOwnerDevMessage(String message) {
        players.stream().filter(p -> p != null && (p.getAttributes().getPlayerRights() == PlayerRights.OWNER || p.getAttributes().getPlayerRights() ==
                PlayerRights.DEVELOPER)).forEach(p -> p.getPacketSender().sendMessage(message));
        //DiscordMessager.sendDebugMessage("[Owner/Developer]\n" + message);
    }

    public static void sendStaffMessage(String message) {
        players.stream().filter(p -> p != null && (p.getAttributes().getPlayerRights().isStaff())).forEach(p -> p.getPacketSender()
                .sendMessage(message)); // == PlayerRights.OWNER || p.getAttributes().getPlayerRights() == PlayerRights.DEVELOPER || p
        // .getAttributes().getPlayerRights() == PlayerRights.ADMINISTRATOR || p.getAttributes().getPlayerRights() == PlayerRights.MODERATOR || p.getAttributes().getPlayerRights() ==
        // PlayerRights.SUPPORT)).forEach(p -> p.getPacketSender().sendMessage(message));
        //DiscordMessager.sendStaffMessage(message);
    }

    public static void sequence() {

        // Handle queued logins.
        for (int amount = 0; amount < GameSettings.LOGIN_THRESHOLD; amount++) {
            Player player = logins.poll();
            if (player == null)
                break;
            if (World.getPlayerByName(player.getAttributes().getUserName()) != null) {
                System.out.println("STOPPED MULTI LOG by " + player.getAttributes().getUserName());
                PlayerLogs.log(player.getAttributes().getUserName(), "Had a multilog attempt.");
                break;
            }
            PlayerHandler.handleLogin(player);
        }

        // Handle queued logouts.
        int amount = 0;
        Iterator<Player> $it = logouts.iterator();
        while ($it.hasNext()) {
            Player player = $it.next();
            if (player == null || amount >= GameSettings.LOGOUT_THRESHOLD)
                break;
            if (PlayerHandler.handleLogout(player, false)) {
                $it.remove();
                amount++;
            }
        }

        // Handle queued vote rewards

        // First we construct the update sequences.
        UpdateSequence<Player> playerUpdate = new PlayerUpdateSequence(synchronizer, updateExecutor);
        UpdateSequence<NPC> npcUpdate = new NpcUpdateSequence();
        // Then we execute pre-updating code.
        players.forEach(playerUpdate::executePreUpdate);
        npcs.forEach(npcUpdate::executePreUpdate);
        // Then we execute parallelized updating code.
        synchronizer.bulkRegister(players.size());
        players.forEach(playerUpdate::executeUpdate);
        synchronizer.arriveAndAwaitAdvance();
        // Then we execute post-updating code.
        players.forEach(playerUpdate::executePostUpdate);
        npcs.forEach(npcUpdate::executePostUpdate);

        RoomHandler.handleRooms();
    }

    public static void updatePlayersOnline() {
        players.forEach(p -> p.getPacketSender().sendString(39160, "@or2@Players online:   @or2@[ @yel@" + (int)
                (players.size()) + "@or2@ ]"));
        players.forEach(p -> p.getPacketSender().sendString(57003, "Players:  @gre@" + (int) (World.getPlayers().size
                ())));
    }

    public static void updateServerTime() {
        //players.forEach(p -> p.getPacketSender().sendString(39161, "@or2@Server time: @or2@[ @yel@"+Misc
        // .getCurrentServerTime()+"@or2@ ]"));
    }

    public static Queue<Player> getLoginQueue() {
        return logins;
    }

    public static Queue<Player> getLogoutQueue() {
        return logouts;
    }

    public static CharacterList<NPC> getNpcs() {
        return npcs;
    }

    public static CharacterList<Player> getPlayers() {
        return players;
    }

    public static List<WaitingRoom> getWaitingRooms() {
        return WAITING_ROOMS;
    }

    public static List<Game> getGames() {
        return GAMES;
    }

    public static List<SpectatorRoom> getSpectatorRooms() {
        return SPECTATOR_ROOMS;
    }
}