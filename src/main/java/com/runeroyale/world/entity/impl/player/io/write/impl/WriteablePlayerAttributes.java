package com.runeroyale.world.entity.impl.player.io.write.impl;

/**
 * Contains the players serialized attributes and user name for easy access.
 *
 * @author Tyb97
 */
public class WriteablePlayerAttributes {

    /**
     * The player's attributes as a json blob.
     */
    private String json;

    /**
     * The players user name.
     */
    private String userName;

    /**
     * @param userName The player's user name.
     * @param json     The json blob to be saved.
     */
    public WriteablePlayerAttributes(String userName, String json) {
        this.userName = userName;
        this.json = json;
    }

    /**
     * Returns the player's attributes as a json blob.
     *
     * @return The player's attributes as a json blob.
     */
    public String getJson() {
        return json;
    }

    /**
     * Returns the player's user name.
     *
     * @return The player's user name.
     */
    public String getUserName() {
        return userName;
    }
}
