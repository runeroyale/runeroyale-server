package com.runeroyale.world.entity.impl.player.io.write;

/**
 * Saves the writable object to a file.
 *
 * @param <T> The writeable object.
 * @author Tyb97
 */
public interface EntityFileWriter<T> {
    /**
     * Saves the writable object to a file.
     *
     * @param writeable The writeable object.
     */
    void writeFile(T writeable);
}
