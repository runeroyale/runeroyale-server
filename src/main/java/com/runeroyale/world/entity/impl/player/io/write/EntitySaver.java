package com.runeroyale.world.entity.impl.player.io.write;

/**
 * The entry point for the entire saving process.
 *
 * @param <T> The object needing to be saved.
 * @author Tyb97
 */
public interface EntitySaver<T> {
    /**
     * The entry method for the entire saving process.
     *
     * @param entity The object needing to be saved.
     */
    void saveEntity(T entity);
}
