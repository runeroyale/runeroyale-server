package com.runeroyale.world.entity.impl.player.io.write.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.runeroyale.world.entity.impl.player.Player;
import com.runeroyale.world.entity.impl.player.io.write.EntitySerializer;

/**
 * The implementation of the EntitySerializer responsible for serializing the player.
 *
 * @author Tyb97
 */
public class PlayerSerializer implements EntitySerializer<Player, WriteablePlayerAttributes> {

    @Override
    public WriteablePlayerAttributes serializeEntity(Player player) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        return new WriteablePlayerAttributes(player.getAttributes().getUserName(), gson.toJson(player.getAttributes()));
    }
}
