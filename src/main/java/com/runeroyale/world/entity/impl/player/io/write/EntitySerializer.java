package com.runeroyale.world.entity.impl.player.io.write;

/**
 * Serializes an object into the specified format.
 *
 * @param <TEntity> The object to serialize.
 * @param <TOutput> The type of output object.
 * @Author Tyb97
 */
public interface EntitySerializer<TEntity, TOutput> {
    /**
     * Marshals the object into the specified format.
     *
     * @param entity The object to serialize.
     * @return The serialized object.
     */
    TOutput serializeEntity(TEntity entity);
}
