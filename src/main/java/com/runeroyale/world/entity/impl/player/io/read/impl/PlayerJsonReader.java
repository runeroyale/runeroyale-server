package com.runeroyale.world.entity.impl.player.io.read.impl;

import com.google.gson.Gson;
import com.runeroyale.GameServer;
import com.runeroyale.net.login.LoginResponses;
import com.runeroyale.world.entity.impl.player.PlayerAttributes;
import com.runeroyale.world.entity.impl.player.io.read.EntityJsonReader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Level;

import static com.runeroyale.KotlinFunctions.withAppropriateFilePathing;

public class PlayerJsonReader implements EntityJsonReader<PlayerAttributes> {

    @Override
    public PlayerAttributes getFromJson(String userName) {
        String json = getFileContents(userName);

        if (json == null)
            return null;

        return new Gson().fromJson(json, PlayerAttributes.class);
    }

    public String getFileContents(String userName) {
        File file = new File(withAppropriateFilePathing("./data/saves/characters/" + userName.toLowerCase() + ".json"));

        if (!fileExists(file)) {
            return null;
        }

        return fileContentsToString(file);
    }

    private String fileContentsToString(File file) {
        String result;

        try {
            List<String> allFileLines = Files.readAllLines(file.toPath());

            StringBuilder builder = new StringBuilder();

            for (String line : allFileLines) {
                builder.append(line);
            }

            result = builder.toString();
        } catch (IOException io) {
            logIOException(io);
            result = Integer.toString(LoginResponses.LOGIN_COULD_NOT_COMPLETE);
        }

        return result;
    }

    private boolean fileExists(File file) {
        return file.exists();
    }

    /**
     * Logs an IO exception with a meaningful message.
     *
     * @param io The exception to log.
     */
    private void logIOException(IOException io) {
        GameServer.getLogger().log(Level.WARNING,
                "An error has occurred while loading a character file!", io);
    }
}
