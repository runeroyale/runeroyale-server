package com.runeroyale.world.entity.impl.player;

import com.runeroyale.GameServer;
import com.runeroyale.GameSettings;
import com.runeroyale.KotlinFunctions;
import com.runeroyale.config.ConfigurationKt;
import com.runeroyale.database.DatabaseSource;
import com.runeroyale.database.DatabaseUtils;
import com.runeroyale.discord.linkage.DiscordLinkage;
import com.runeroyale.engine.task.TaskManager;
import com.runeroyale.engine.task.impl.*;
import com.runeroyale.model.Flag;
import com.runeroyale.model.Locations;
import com.runeroyale.model.Skill;
import com.runeroyale.model.container.impl.Equipment;
import com.runeroyale.model.definitions.WeaponAnimations;
import com.runeroyale.model.definitions.WeaponInterfaces;
import com.runeroyale.net.PlayerSession;
import com.runeroyale.net.SessionState;
import com.runeroyale.net.security.ConnectionHandler;
import com.runeroyale.world.World;
import com.runeroyale.world.content.*;
import com.runeroyale.world.content.battleroyale.costumes.CostumeTab;
import com.runeroyale.world.content.clan.ClanChatManager;
import com.runeroyale.world.content.combat.effect.CombatPoisonEffect;
import com.runeroyale.world.content.combat.effect.CombatTeleblockEffect;
import com.runeroyale.world.content.combat.magic.Autocasting;
import com.runeroyale.world.content.combat.prayer.CurseHandler;
import com.runeroyale.world.content.combat.prayer.PrayerHandler;
import com.runeroyale.world.content.combat.range.DwarfMultiCannon;
import com.runeroyale.world.content.combat.weapon.CombatSpecial;
import com.runeroyale.world.content.skill.impl.hunter.Hunter;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import static com.runeroyale.KotlinFunctions.asMD5;

public class PlayerHandler {

    public static void handleLogin(Player player) {
        //Register the player
        System.out.println("[World] Registering player - [username, host] : [" + player.getAttributes().getUserName()
                + ", " + player.getAttributes().getHostAddress() + "]");
        ConnectionHandler.add(player.getAttributes().getHostAddress());
        World.getPlayers().add(player);
        World.updatePlayersOnline();
        PlayersOnlineInterface.add(player);
        player.getSession().setState(SessionState.LOGGED_IN);

        //Packets
        player.getPacketSender().sendMapRegion().sendDetails();

        player.getRecordedLogin().reset();

        //Tabs
        player.getPacketSender().sendTabs();

        player.getInventory().refreshItems();
        player.getEquipment().refreshItems();

        //Weapons and equipment..
        WeaponAnimations.update(player);
        WeaponInterfaces.assign(player, player.getEquipment().get(Equipment.WEAPON_SLOT));
        CombatSpecial.updateBar(player);
        BonusManager.update(player);

        //Skills
        player.getSummoning().login();
        for (Skill skill : Skill.values()) {
            player.getSkillManager().updateSkill(skill);
        }
        player.getSkillManager().setMaxStats();

        //Relations
        player.getRelations().setPrivateMessageId(1).onLogin(player).updateLists(true);

        //Client configurations
        player.getPacketSender().sendConfig(172, player.isAutoRetaliate() ? 1 : 0)
                .sendTotalXp(player.getSkillManager().getTotalGainedExp())
                .sendConfig(player.getFightType().getParentId(), player.getFightType().getChildId())
                .sendRunStatus().sendRunEnergy(100).sendRights()
                .sendString(8135, "" + player.getMoneyInPouch())
                .sendInteractionOption("Follow", 3, false)
                .sendInteractionOption("Trade With", 4, false);
        //.sendInterfaceRemoval().sendString(39161, "@or2@Server time: @or2@[ @yel@"+Misc.getCurrentServerTime()
        // +"@or2@ ]");

        player.getPacketSender().sendString(54045, " " + player.getSkillManager().getCurrentLevel(Skill.SUMMONING) +
                "/" + player.getSkillManager().getMaxLevel(Skill.SUMMONING));

        if (player.newPlayer()) {
            player.getAttributes().getUnlockedEmotes().add(Emotes.EmoteData.DANCE);
            if (ConfigurationKt.getConfig().getSqlEnabled()) createForumsUser(player);
        }

        Autocasting.onLogin(player);
        PrayerHandler.deactivateAll(player);
        CurseHandler.deactivateAll(player);
        BonusManager.sendCurseBonuses(player);

        //Tasks
        TaskManager.submit(new PlayerSkillsTask(player));
        TaskManager.submit(new PlayerRegenConstitutionTask(player));
        TaskManager.submit(new SummoningRegenPlayerConstitutionTask(player));
        if (player.isPoisoned()) {
            TaskManager.submit(new CombatPoisonEffect(player));
        }
        if (player.getPrayerRenewalPotionTimer() > 0) {
            TaskManager.submit(new PrayerRenewalPotionTask(player));
        }
        if (player.getOverloadPotionTimer() > 0) {
            TaskManager.submit(new OverloadPotionTask(player));
        }
        if (player.getTeleblockTimer() > 0) {
            TaskManager.submit(new CombatTeleblockEffect(player));
        }
        if (player.getSkullTimer() > 0) {
            player.setSkullIcon(1);
            TaskManager.submit(new CombatSkullEffect(player));
        }
        if (player.getFireImmunity() > 0) {
            FireImmunityTask.makeImmune(player, player.getFireImmunity(), player.getFireDamageModifier());
        }
        if (player.getSpecialPercentage() < 100) {
            TaskManager.submit(new PlayerSpecialAmountTask(player));
        }
        if (player.hasStaffOfLightEffect()) {
            TaskManager.submit(new StaffOfLightSpecialAttackTask(player));
        }
        if (player.getMinutesBonusExp() >= 0) {
            TaskManager.submit(new BonusExperienceTask(player));
        }

        //Update appearance


        //Others
        Locations.login(player);
        player.getPacketSender().sendMessage("@bla@Welcome to " + GameSettings.RSPS_NAME + "!");

        //synchronization
        if (player.getAttributes().getForumsUID() == -1) {
            player.getPacketSender().sendMessage("Your account doesn't seem to be linked with your forums account. " +
                    "Fix that by");
            player.getPacketSender().sendMessage("doing ::link forums YOUR_UID");
        }
        if (player.getAttributes().getDiscordId() == null) {
            player.getPacketSender().sendMessage("Your account doesn't seem to be linked with your Discord. " +
                    "Fix that by going ");
            player.getPacketSender().sendMessage("to Discord and typing ::link in #spam!");
        }

        if (!player.getAttributes().getPlayerRights().OwnerDeveloperOnly() && player.getSkillManager().getExperience
                (Skill.CONSTRUCTION) > 1) {
            player.getSkillManager().setExperience(Skill.CONSTRUCTION, 0);
            player.getSkillManager().setMaxLevel(Skill.CONSTRUCTION, 1);
            player.getSkillManager().setCurrentLevel(Skill.CONSTRUCTION, 1, true);
        }

        PlayerPanel.refreshPanel(player);

        ClanChatManager.handleLogin(player);

        player.setCurrentRoom(World.LOBBY);

        sendAlphaTestingStuff(player);

        CostumeTab.writeToCostumeTab(player);

        player.getUpdateFlag().flag(Flag.APPEARANCE);

        PlayerLogs.log(player.getAttributes().getUserName(), "Login. ip: " + player.getAttributes().getHostAddress()
                + ", mac: " + player.getAttributes().getMac() + ", uuid: " + player.getAttributes().getUuid());

        player.getPacketSender().sendString(57003, "Players:  @gre@" + (World.getPlayers().size()));

    }

    private static void sendAlphaTestingStuff(Player player) {
        player.getAttributes().getObtainedCostumes().clear();
        player.getAttributes().getObtainedCostumes().addAll(List.of(
                "Noob Costume", "Zerker Costume", "Zamorakian Prince Costume", "\"Black And Yellow\" Costume",
                "Chicken Costume", "Royale Costume", "Religious Drug Dealer Costume", "Rich Thief Costume",
                "Gilded Age Costume", "Dharok's Costume", "Fally Guard Costume", "No Dragon Slayer Noob Costume",
                "Rich F2P Kid Costume"
        ));

    }

    public static boolean handleLogout(Player player, Boolean forced) {
        try {

            PlayerSession session = player.getSession();

            if (session.getChannel().isOpen()) {
                session.getChannel().close();
            }

            if (!player.isRegistered()) {
                return true;
            }

            boolean exception = forced || GameServer.isUpdating() || World.getLogoutQueue().contains(player) &&
                    player.getLogoutTimer().elapsed(90000);
            if (player.logout() || exception) {
                //new Thread(new HighscoresHandler(player)).start();
                System.out.println("[World] Deregistering player - [username, host] : [" + player.getAttributes()
                        .getUserName() + ", " + player.getAttributes().getHostAddress() + "]");
                player.getSession().setState(SessionState.LOGGING_OUT);
                ConnectionHandler.remove(player.getAttributes().getHostAddress());
                player.setTotalPlayTime(player.getTotalPlayTime() + player.getRecordedLogin().elapsed());
                player.getPacketSender().sendInterfaceRemoval();
                if (player.getCannon() != null) {
                    DwarfMultiCannon.pickupCannon(player, player.getCannon(), true);
                }
                if (exception && player.getResetPosition() != null) {
                    player.moveTo(player.getResetPosition());
                    player.setResetPosition(null);
                }
                if (player.getRegionInstance() != null) {
                    player.getRegionInstance().destruct();
                }
                Hunter.handleLogout(player);
                Locations.logout(player);
                player.getSummoning().unsummon(false, false);
                player.getFarming().save();
                ClanChatManager.leave(player, false);
                player.getRelations().updateLists(false);
                PlayersOnlineInterface.remove(player);
                TaskManager.cancelTasks(player.getCombatBuilder());
                TaskManager.cancelTasks(player);
                if (player.getCurrentRoom() != null) {
                    player.getCurrentRoom().handleLogout(player);
                }
                player.save();
                World.getPlayers().remove(player);
                session.setState(SessionState.LOGGED_OUT);
                World.updatePlayersOnline();
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private static void createForumsUser(Player player) {
        try (Connection forumsConnection = DatabaseSource.getForumsConnection()) {
            String salt = DiscordLinkage.generateVerificationCode().substring(0, 8); // lmfao
            String saltPassword = asMD5(asMD5(salt) + asMD5(player.getAttributes().getPassword()));
            DatabaseUtils.performDatabaseFunction(forumsConnection, "INSERT INTO mybb_users " +
                    "(uid, username, password, salt, usergroup, regdate, regip, signature, buddylist, ignorelist, pmfolders, notepad, usernotes, email) VALUES " +
                    "(NULL, \"" + player.getAttributes().getUserName() + "\", \"" + saltPassword + "\", \"" + salt + "\", " +
                    "2, " + (System.currentTimeMillis() / 1000) + ", \"" + player.getAttributes().getHostAddress() + "\", " +
                    "\"\", \"\", \"\", \"\", \"\", \"\", \"abc123@abc.com\");");
        } catch (SQLException e) {
            if (!(e instanceof SQLIntegrityConstraintViolationException)) {
                e.printStackTrace();
            }
        }
        KotlinFunctions.executeURL("https://runeroyale.com/forums/update_stats_script.php");
        KotlinFunctions.executeURL("https://runeroyale.com/forums/get_member_uid_script.php?username=\"" + player.getAttributes().getUserName().replace(" ", "%20") + "\"",
                output -> {
                    player.getAttributes().setForumsUID(Integer.parseInt(output));
                    return null;
                });
    }

    private static void setForumsUID(Player player, int forumsUID) {
        player.getAttributes().setForumsUID(forumsUID);
    }
}
