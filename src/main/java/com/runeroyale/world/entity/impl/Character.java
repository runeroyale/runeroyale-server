package com.runeroyale.world.entity.impl;

import com.runeroyale.engine.task.Task;
import com.runeroyale.engine.task.TaskManager;
import com.runeroyale.model.*;
import com.runeroyale.model.Locations.Location;
import com.runeroyale.model.movement.MovementQueue;
import com.runeroyale.util.Stopwatch;
import com.runeroyale.world.content.combat.CombatBuilder;
import com.runeroyale.world.content.combat.CombatType;
import com.runeroyale.world.content.combat.magic.CombatSpell;
import com.runeroyale.world.content.combat.strategy.CombatStrategy;
import com.runeroyale.world.entity.Entity;

/**
 * A player or NPC
 *
 * @author Gabriel Hannason
 */

public abstract class Character extends Entity {

    public Position singlePlayerPositionFacing;

    private transient Animation animation;

    /*
     * Fields
     */
    private transient CombatBuilder combatBuilder = new CombatBuilder(this);

    /*** LONGS **/
    private transient CombatSpell currentlyCasting;
    /*** INSTANCES ***/
    private transient Direction direction, primaryDirection = Direction.NONE, secondaryDirection = Direction.NONE,
            lastDirection = Direction.NONE;
    /*** STRINGS ***/
    private transient String forcedChat;
    private transient int freezeDelay;
    private transient Graphic graphic;
    private transient Entity interactingEntity;
    private transient Stopwatch lastCombat = new Stopwatch();
    private transient Location location;
    private transient MovementQueue movementQueue = new MovementQueue(this);
    private transient boolean moving;
    private transient boolean needsPlacement;
    /*** INTS ***/
    private transient int npcTransformationId;
    private transient int poisonDamage;
    private transient Position positionToFace;
    /*** BOOLEANS ***/
    private transient boolean[] prayerActive = new boolean[30], curseActive = new boolean[20];
    private transient Hit primaryHit;
    private transient RegionInstance regionInstance;
    private transient boolean registered;
    private transient boolean resetMovementQueue;
    private transient Hit secondaryHit;
    private transient int stunDelay;
    private transient boolean teleporting;
    private transient UpdateFlag updateFlag = new UpdateFlag();
    private transient boolean visible = true;

    public Character(Position position) {
        super(position);
        location = Location.getLocation(this);
    }

    @Override
    public void performAnimation(Animation animation) {
        if (animation == null)
            return;
        setAnimation(animation);
    }

    @Override
    public void performGraphic(Graphic graphic) {
        if (graphic == null)
            return;
        setGraphic(graphic);
    }

    public abstract void appendDeath();

    /**
     * Deals one damage to this entity.
     *
     * @param hit the damage to be dealt.
     * @param directDamage if the damage should attack hp directly and ignore defence
     */
    public void dealDamage(Hit hit, boolean directDamage) {
        if (getUpdateFlag().flagged(Flag.SINGLE_HIT)) {
            dealSecondaryDamage(hit);
            return;
        }
        if (getConstitution() <= 0)
            return;
        primaryHit = decrementHealth(hit, directDamage);
        getUpdateFlag().flag(Flag.SINGLE_HIT);
    }

    public void dealDamage(Hit hit) {
        dealDamage(hit, false);
    }

    /**
     * Deals two damage splats to this entity.
     *
     * @param hit       the first hit.
     * @param secondHit the second hit.
     */
    public void dealDoubleDamage(Hit hit, Hit secondHit) {
        dealDamage(hit);
        dealSecondaryDamage(secondHit);
    }

    /**
     * Deals four damage splats to this entity.
     *
     * @param hit       the first hit.
     * @param secondHit the second hit.
     * @param thirdHit  the third hit.
     * @param fourthHit the fourth hit.
     */
    public void dealQuadrupleDamage(Hit hit, Hit secondHit, final Hit thirdHit,
                                    final Hit fourthHit) {
        dealDoubleDamage(hit, secondHit);

        TaskManager.submit(new Task(1, this, false) {
            @Override
            public void execute() {
                if (!registered) {
                    this.stop();
                    return;
                }
                dealDoubleDamage(thirdHit, fourthHit);
                this.stop();
            }
        });
    }

    /**
     * Deals three damage splats to this entity.
     *
     * @param hit       the first hit.
     * @param secondHit the second hit.
     * @param thirdHit  the third hit.
     */
    public void dealTripleDamage(Hit hit, Hit secondHit, final Hit thirdHit) {
        dealDoubleDamage(hit, secondHit);

        TaskManager.submit(new Task(1, this, false) {
            @Override
            public void execute() {
                if (!registered) {
                    this.stop();
                    return;
                }
                dealDamage(thirdHit);
                this.stop();
            }
        });
    }

    public int decrementAndGetFreezeDelay() {
        return this.freezeDelay--;
    }

    public int decrementAndGetStunDelay() {
        return this.stunDelay--;
    }

    public Hit decrementHealth(Hit hit, boolean overrideDefence) {
        if (getConstitution() <= 0)
            return hit;
        if (!overrideDefence) {
            if (getDefence() > 0 && hit.getHitmask() != Hitmask.RED) {
                int hpRemainder;
                int defenceDamageInflicted = hit.getDamage();
                if (hit.getDamage() > getDefence()) {
                    hpRemainder = hit.getDamage() - getDefence();
                    defenceDamageInflicted = getDefence();
                    hit.setHitmask(Hitmask.RED);
                    hit.setDamage(hpRemainder);
                }
                int defenceOutcome = getDefence() - defenceDamageInflicted;
                setDefence(defenceOutcome);
                if (hit.getHitmask() == Hitmask.BLUE) {
                    return hit;
                }
            }
        }
        if (hit.getDamage() > getConstitution())
            hit.setDamage(getConstitution());
        if (hit.getDamage() < 0)
            hit.setDamage(0);
        int outcome = getConstitution() - hit.getDamage();
        if (outcome < 0)
            outcome = 0;
        setConstitution(outcome);
        return hit;
    }

    /*
     * Getters and setters
     * Also contains methods.
     */

    public void delayedMoveTo(final Position teleportTarget, final int delay) {
        if (moving)
            return;
        moving = true;
        TaskManager.submit(new Task(delay, this, false) {
            @Override
            public void stop() {
                setEventRunning(false);
                moving = false;
            }

            @Override
            protected void execute() {
                moveTo(teleportTarget);
                stop();
            }
        });
    }

    public abstract CombatStrategy determineStrategy();

    public Character forceChat(String message) {
        setForcedChat(message);
        getUpdateFlag().flag(Flag.FORCED_CHAT);
        return this;
    }

    public abstract int getBaseAttack(CombatType type);

    public abstract int getBaseDefence(CombatType type);

    public abstract void heal(int damage);

    public Character moveTo(Position teleportTarget) {
        getMovementQueue().reset();
        super.setPosition(teleportTarget.copy());
        setNeedsPlacement(true);
        setResetMovementQueue(true);
        setTeleporting(true);
        if (isPlayer()) {
            getMovementQueue().handleRegionChange();
        }
        return this;
    }

    public abstract void poisonVictim(Character victim, CombatType type);

    /**
     * Prepares to cast the argued spell on the argued victim.
     *
     * @param spell  the spell to cast.
     * @param victim the victim to cast the spell on.
     */
    public void prepareSpell(CombatSpell spell, Character victim) {
        currentlyCasting = spell;
        currentlyCasting.startCast(this, victim);
    }

    public Character setCurseActive(int id, boolean curseActive) {
        this.curseActive[id] = curseActive;
        return this;
    }

    public Character setEntityInteraction(Entity entity) {
        this.interactingEntity = entity;
        getUpdateFlag().flag(Flag.ENTITY_INTERACTION);
        return this;
    }

    public Character setPrayerActive(int id, boolean prayerActive) {
        this.prayerActive[id] = prayerActive;
        return this;
    }

    /**
     * Deal secondary damage to this entity.
     *
     * @param hit the damage to be dealt.
     */
    private void dealSecondaryDamage(Hit hit) {
        secondaryHit = decrementHealth(hit, false);
        getUpdateFlag().flag(Flag.DOUBLE_HIT);
    }

    public int getAndDecrementPoisonDamage() {
        return poisonDamage -= 15;
    }

    public Animation getAnimation() {
        return animation;
    }

    public Character setAnimation(Animation animation) {
        this.animation = animation;
        getUpdateFlag().flag(Flag.ANIMATION);
        return this;
    }

    public abstract int getAttackSpeed();

    /**
     * Gets the combat session.
     *
     * @return the combat session.
     */
    public CombatBuilder getCombatBuilder() {
        return combatBuilder;
    }

    public abstract int getConstitution();

    public abstract Character setConstitution(int constitution);

    public CombatSpell getCurrentlyCasting() {
        return currentlyCasting;
    }

    public void setCurrentlyCasting(CombatSpell currentlyCasting) {
        this.currentlyCasting = currentlyCasting;
    }

    public boolean[] getCurseActive() {
        return curseActive;
    }

    public Character setCurseActive(boolean[] curseActive) {
        this.curseActive = curseActive;
        return this;
    }

    public abstract int getDefence();

    /*** ABSTRACT METHODS ***/
    public abstract Character setDefence(int defence);

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
        int[] directionDeltas = direction.getDirectionDelta();
        setPositionToFace(getPosition().copy().add(directionDeltas[0], directionDeltas[1]));
    }

    public String getForcedChat() {
        return forcedChat;
    }

    public Character setForcedChat(String forcedChat) {
        this.forcedChat = forcedChat;
        return this;
    }

    public int getFreezeDelay() {
        return freezeDelay;
    }

    public void setFreezeDelay(int freezeDelay) {
        this.freezeDelay = freezeDelay;
    }

    public Graphic getGraphic() {
        return graphic;
    }

    public Character setGraphic(Graphic graphic) {
        this.graphic = graphic;
        getUpdateFlag().flag(Flag.GRAPHIC);
        return this;
    }

    public Entity getInteractingEntity() {
        return interactingEntity;
    }

    /**
     * @return the lastCombat
     */
    public Stopwatch getLastCombat() {
        return lastCombat;
    }

    /**
     * Gets the last direction this character was facing.
     *
     * @return the last direction.
     */
    public final Direction getLastDirection() {
        return lastDirection;
    }

    public final void setLastDirection(Direction lastDirection) {
        this.lastDirection = lastDirection;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public MovementQueue getMovementQueue() {
        return movementQueue;
    }

    public Character setMovementQueue(MovementQueue movementQueue) {
        this.movementQueue = movementQueue;
        return this;
    }

    public int getNpcTransformationId() {
        return npcTransformationId;
    }

    public Character setNpcTransformationId(int npcTransformationId) {
        this.npcTransformationId = npcTransformationId;
        return this;
    }

    public int getPoisonDamage() {
        return poisonDamage;
    }

    public void setPoisonDamage(int poisonDamage) {
        this.poisonDamage = poisonDamage;
    }

    public Position getPositionToFace() {
        return positionToFace;
    }

    public Character setPositionToFace(Position positionToFace) {
        this.positionToFace = positionToFace;
        getUpdateFlag().flag(Flag.FACE_POSITION);
        return this;
    }

    public boolean[] getPrayerActive() {
        return prayerActive;
    }

    public Character setPrayerActive(boolean[] prayerActive) {
        this.prayerActive = prayerActive;
        return this;
    }

    public Direction getPrimaryDirection() {
        return primaryDirection;
    }

    public void setPrimaryDirection(Direction primaryDirection) {
        this.primaryDirection = primaryDirection;
    }

    /**
     * Get the primary hit for this entity.
     *
     * @return the primaryHit.
     */
    public Hit getPrimaryHit() {
        return primaryHit;
    }

    /*
     * Movement queue
     */

    public RegionInstance getRegionInstance() {
        return regionInstance;
    }

    public void setRegionInstance(RegionInstance regionInstance) {
        this.regionInstance = regionInstance;
    }

    public Direction getSecondaryDirection() {
        return secondaryDirection;
    }

    public final void setSecondaryDirection(Direction secondaryDirection) {
        this.secondaryDirection = secondaryDirection;
    }

    /**
     * Get the secondary hit for this entity.
     *
     * @return the secondaryHit.
     */
    public Hit getSecondaryHit() {
        return secondaryHit;
    }

    public int getStunDelay() {
        return freezeDelay;
    }

    public void setStunDelay(int stunDelay) {
        this.stunDelay = stunDelay;
    }

    public UpdateFlag getUpdateFlag() {
        return updateFlag;
    }

    public boolean isFrozen() {
        return freezeDelay > 0;
    }

    public boolean isNeedsPlacement() {
        return needsPlacement;
    }

    public void setNeedsPlacement(boolean needsPlacement) {
        this.needsPlacement = needsPlacement;
    }

    public boolean isPoisoned() {
        if (poisonDamage < 0)
            poisonDamage = 0;
        return poisonDamage != 0;
    }

    /**
     * Gets if this entity is registered.
     *
     * @return the unregistered.
     */
    public boolean isRegistered() {
        return registered;
    }

    /**
     * Sets if this entity is registered,
     *
     * @param registered the unregistered to set.
     */
    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    /**
     * Determines if this character needs to reset their movement queue.
     *
     * @return {@code true} if this character needs to reset their movement
     * queue, {@code false} otherwise.
     */
    public final boolean isResetMovementQueue() {
        return resetMovementQueue;
    }

    public final void setResetMovementQueue(boolean resetMovementQueue) {
        this.resetMovementQueue = resetMovementQueue;
    }

    public boolean isStunned() {
        return stunDelay > 0;
    }

    public boolean isTeleporting() {
        return this.teleporting;
    }

    public Character setTeleporting(boolean teleporting) {
        this.teleporting = teleporting;
        return this;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}