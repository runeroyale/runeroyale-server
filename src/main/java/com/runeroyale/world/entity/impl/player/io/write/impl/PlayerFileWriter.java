package com.runeroyale.world.entity.impl.player.io.write.impl;

import com.runeroyale.GameServer;
import com.runeroyale.world.entity.impl.player.io.write.EntityFileWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;

import static com.runeroyale.KotlinFunctions.withAppropriateFilePathing;

/**
 * Writes the serialized player to a file.
 *
 * @author Tyb97
 */
public class PlayerFileWriter implements EntityFileWriter<WriteablePlayerAttributes> {

    @Override
    public void writeFile(WriteablePlayerAttributes writeablePlayer) {
        String userName = writeablePlayer.getUserName().toLowerCase();

        File file = new File(withAppropriateFilePathing("./data/saves/characters/" + userName.toLowerCase() + ".json"));

        if (!fileExists(file)) {
            createNewFile(file);
        }

        try (FileWriter writer = new FileWriter(file)) {
            writer.write(writeablePlayer.getJson());
        } catch (IOException io) {
            logIOException(io);
        }
    }

    /**
     * Creates a new player save file.
     *
     * @param file The file to create.
     */
    private void createNewFile(File file) {
        try {
            file.createNewFile();
        } catch (IOException io) {
            logIOException(io);
        }
    }

    /**
     * Checks if a file exists.
     *
     * @param file The file to check.
     * @return If the file exists.
     */
    private boolean fileExists(File file) {
        return file.exists();
    }

    /**
     * Logs an IO exception with a meaningful message.
     *
     * @param io The exception to log.
     */
    private void logIOException(IOException io) {
        GameServer.getLogger().log(Level.WARNING,
                "An error has occurred while saving a character file!", io);
    }
}
