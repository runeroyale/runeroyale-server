package com.runeroyale.world.entity.impl.player

enum class PlayerState {
    IN_LOBBY, IN_WAITING_ROOM, IN_GAME, DEAD;
}