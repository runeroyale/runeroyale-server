package com.runeroyale.world.entity.impl.player;

import com.runeroyale.model.RegionInstance.RegionInstanceType;
import com.runeroyale.world.entity.impl.GroundItemManager;

public class PlayerProcess {

    /*
     * The player (owner) of this instance
     */
    private Player player;

    /*
     * The timer tick, once this reaches 2, the player's
     * total play time will be updated.
     * 2 equals 1.2 seconds.
     */
    private int timerTick;

    /*
     * Makes sure ground items are spawned on height change
     */
    private int previousHeight;

    public PlayerProcess(Player player) {
        this.player = player;
        this.previousHeight = player.getPosition().getZ();
    }

    public void sequence() {

        /** COMBAT **/
        player.getCombatBuilder().process();

        /** SKILLS **/
        if (player.shouldProcessFarming()) {
            player.getFarming().sequence();
        }

        /** MISC **/

        if (previousHeight != player.getPosition().getZ()) {
            GroundItemManager.handleRegionChange(player);
            previousHeight = player.getPosition().getZ();
        }
        timerTick++;

        PlayerStateHandler.sequence(player);

        if (player.getRegionInstance() != null && (player.getRegionInstance().getType() == RegionInstanceType.CONSTRUCTION_HOUSE || player.getRegionInstance().getType() == RegionInstanceType.CONSTRUCTION_DUNGEON)) {
            //((House) player.getRegionInstance()).process();
        }
    }
}
