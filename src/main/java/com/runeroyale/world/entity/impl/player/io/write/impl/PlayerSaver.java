package com.runeroyale.world.entity.impl.player.io.write.impl;

import com.runeroyale.world.entity.impl.player.Player;
import com.runeroyale.world.entity.impl.player.io.write.EntityFileWriter;
import com.runeroyale.world.entity.impl.player.io.write.EntitySaver;
import com.runeroyale.world.entity.impl.player.io.write.EntitySerializer;

/**
 * The entry point for saving the player's data to a file.
 *
 * @author Tyb97
 */
public class PlayerSaver implements EntitySaver<Player> {

    /**
     * The serializer object.
     */
    private final EntitySerializer<Player, WriteablePlayerAttributes> serializer;

    /**
     * The file writer object.
     */
    private final EntityFileWriter<WriteablePlayerAttributes> writer;

    /**
     * @param serializer The serializer object.
     * @param writer     The file writer object.
     */
    public PlayerSaver(EntitySerializer<Player, WriteablePlayerAttributes> serializer, EntityFileWriter<WriteablePlayerAttributes> writer) {
        this.serializer = serializer;
        this.writer = writer;
    }

    @Override
    public void saveEntity(Player player) {
        WriteablePlayerAttributes playerJson = serializePlayer(player);

        writeToFile(playerJson);
    }

    /**
     * For calling the serializer to get the serialized player.
     *
     * @param player The player to be serialized.
     * @return The serialized player.
     */
    private WriteablePlayerAttributes serializePlayer(Player player) {
        return serializer.serializeEntity(player);
    }

    /**
     * For calling the file writer to save the serialized player to a file.
     *
     * @param writeablePlayer The serialized player.
     */
    private void writeToFile(WriteablePlayerAttributes writeablePlayer) {
        writer.writeFile(writeablePlayer);
    }
}
