package com.runeroyale.world.entity.impl.player.io.read.impl;

import com.runeroyale.net.login.LoginResponses;
import com.runeroyale.world.entity.impl.player.PlayerAttributes;
import com.runeroyale.world.entity.impl.player.io.read.EntityValidator;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.mindrot.jbcrypt.BCrypt;

/**
 * Validates that the Player attributes are valid and returns a login code based on the validation results.
 *
 * @author Tyb97
 */
public class PlayerValidator implements EntityValidator<Integer, ImmutablePair<PlayerAttributes, PlayerAttributes>> {

    @Override
    public Integer validateEntity(ImmutablePair<PlayerAttributes, PlayerAttributes> playerAttributes) {
        PlayerAttributes deserializedAttributes = playerAttributes.getLeft();
        PlayerAttributes attributesToCompare = playerAttributes.getRight();

        if (playerIsNew(deserializedAttributes)) {
            return LoginResponses.NEW_ACCOUNT;
        }

        if (!playerHasCorrectPassword(deserializedAttributes, attributesToCompare)) {
            return LoginResponses.LOGIN_INVALID_CREDENTIALS;
        }

        return LoginResponses.LOGIN_SUCCESSFUL;
    }

    private boolean playerHasCorrectPassword(PlayerAttributes deserializedAttributes, PlayerAttributes
            attributesToCompare) {
        String hash = deserializedAttributes.getHash();
        attributesToCompare.setSalt(hash.substring(0, 29));
        return BCrypt.checkpw(attributesToCompare.getPassword(), hash);
    }

    private boolean playerIsNew(PlayerAttributes deserializedAttributes) {
        return deserializedAttributes == null;
    }
}
