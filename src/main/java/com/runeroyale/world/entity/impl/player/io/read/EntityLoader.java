package com.runeroyale.world.entity.impl.player.io.read;

/**
 * The entry point for the entity loading process.
 *
 * @param <TResult> The type of response after loading an entity.
 * @param <TEntity> The type of entity to load into.
 * @author Tyb97
 */
public interface EntityLoader<TResult, TEntity> {
    /**
     * Obtains the result after loading an entity.
     *
     * @param entity The entity to load into.
     * @return The result after loading.
     */
    TResult getLoadedEntityResult(TEntity entity);
}
