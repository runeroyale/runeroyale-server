package com.runeroyale.world.entity.impl.player.io.read.impl;

import com.runeroyale.net.login.LoginResponses;
import com.runeroyale.world.entity.impl.player.Player;
import com.runeroyale.world.entity.impl.player.PlayerAttributes;
import com.runeroyale.world.entity.impl.player.io.read.EntityJsonReader;
import com.runeroyale.world.entity.impl.player.io.read.EntityLoader;
import com.runeroyale.world.entity.impl.player.io.read.EntityValidator;
import org.apache.commons.lang3.tuple.ImmutablePair;

public class PlayerLoader implements EntityLoader<Integer, Player> {

    private final EntityJsonReader<PlayerAttributes> reader;

    private final EntityValidator<Integer, ImmutablePair<PlayerAttributes, PlayerAttributes>> validator;

    public PlayerLoader(EntityJsonReader<PlayerAttributes> reader, EntityValidator<Integer, ImmutablePair<PlayerAttributes,
            PlayerAttributes>>
            validator) {
        this.reader = reader;
        this.validator = validator;
    }

    @Override
    public Integer getLoadedEntityResult(Player player) {
        PlayerAttributes playerAttributes = getPlayerAttributesFromJson(player.getAttributes().getUserName());

        int response = validatePlayer(new ImmutablePair<>(playerAttributes, player.getAttributes()));

        if (responseIsSuccessful(response)) {
            player.setAttributes(playerAttributes);
        }

        return response;
    }

    public PlayerAttributes getPlayerAttributesFromJson(String userName) {
        return reader.getFromJson(userName);
    }

    private boolean responseIsSuccessful(int response) {
        return response == LoginResponses.LOGIN_SUCCESSFUL;
    }

    private Integer validatePlayer(ImmutablePair<PlayerAttributes, PlayerAttributes> players) {
        return validator.validateEntity(players);
    }
}
