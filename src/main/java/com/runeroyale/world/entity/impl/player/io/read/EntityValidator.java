package com.runeroyale.world.entity.impl.player.io.read;

/**
 * Validates that the {@link TInput} meets the acceptance criteria.
 *
 * @param <TOutput> The response type after validating the input.
 * @param <TInput>  The type of object needing validation.
 * @author Tyb97
 */
public interface EntityValidator<TOutput, TInput> {
    /**
     * Validates the entity.
     *
     * @param input The entity to validate.
     * @return The response after validating.
     */
    TOutput validateEntity(TInput input);
}
