package com.runeroyale.world.entity.impl.player.io.read;

/**
 * Reads JSON and converts into the specified format.
 *
 * @param <T> Type of object to convert JSON into.
 * @author Tyb97
 */
public interface EntityJsonReader<T> {
    /**
     * Reads JSON and returns an object of specified type.
     *
     * @param fileName The file name of the JSON file to read from.
     * @return The object containing the data read from the JSON.
     */
    T getFromJson(String fileName);
}
