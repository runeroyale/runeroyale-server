package com.runeroyale.world.entity.impl.player;

import com.runeroyale.engine.task.Task;
import com.runeroyale.engine.task.TaskManager;
import com.runeroyale.engine.task.impl.PlayerDeathTask;
import com.runeroyale.engine.task.impl.WalkToTask;
import com.runeroyale.model.*;
import com.runeroyale.model.container.impl.Equipment;
import com.runeroyale.model.container.impl.Inventory;
import com.runeroyale.model.definitions.WeaponAnimations;
import com.runeroyale.model.definitions.WeaponInterfaces;
import com.runeroyale.model.definitions.WeaponInterfaces.WeaponInterface;
import com.runeroyale.model.input.Input;
import com.runeroyale.net.PlayerSession;
import com.runeroyale.net.SessionState;
import com.runeroyale.net.packet.PacketSender;
import com.runeroyale.util.FrameUpdater;
import com.runeroyale.util.Stopwatch;
import com.runeroyale.world.content.BonusManager;
import com.runeroyale.world.content.DropLog.DropLogEntry;
import com.runeroyale.world.content.KillsTracker.KillsEntry;
import com.runeroyale.world.content.PointsHandler;
import com.runeroyale.world.content.battleroyale.GameType;
import com.runeroyale.world.content.battleroyale.costumes.Costume;
import com.runeroyale.world.content.battleroyale.costumes.CostumePowerUpHandler;
import com.runeroyale.world.content.battleroyale.locations.RoomLocations;
import com.runeroyale.world.content.battleroyale.rooms.Room;
import com.runeroyale.world.content.clan.ClanChat;
import com.runeroyale.world.content.combat.CombatFactory;
import com.runeroyale.world.content.combat.CombatType;
import com.runeroyale.world.content.combat.effect.CombatPoisonEffect.CombatPoisonData;
import com.runeroyale.world.content.combat.magic.CombatSpell;
import com.runeroyale.world.content.combat.magic.CombatSpells;
import com.runeroyale.world.content.combat.prayer.CurseHandler;
import com.runeroyale.world.content.combat.prayer.PrayerHandler;
import com.runeroyale.world.content.combat.pvp.PlayerKillingAttributes;
import com.runeroyale.world.content.combat.range.CombatRangedAmmo.RangedWeaponData;
import com.runeroyale.world.content.combat.strategy.CombatStrategies;
import com.runeroyale.world.content.combat.strategy.CombatStrategy;
import com.runeroyale.world.content.combat.weapon.CombatSpecial;
import com.runeroyale.world.content.combat.weapon.FightType;
import com.runeroyale.world.content.dialogue.DialogueIterator;
import com.runeroyale.world.content.skill.SkillManager;
import com.runeroyale.world.content.skill.impl.farming.Farming;
import com.runeroyale.world.content.skill.impl.slayer.Slayer;
import com.runeroyale.world.content.skill.impl.summoning.Pouch;
import com.runeroyale.world.content.skill.impl.summoning.Summoning;
import com.runeroyale.world.entity.impl.Character;
import com.runeroyale.world.entity.impl.npc.NPC;
import com.runeroyale.world.entity.impl.player.io.write.impl.PlayerFileWriter;
import com.runeroyale.world.entity.impl.player.io.write.impl.PlayerSaver;
import com.runeroyale.world.entity.impl.player.io.write.impl.PlayerSerializer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


public class Player extends Character {


    private PlayerAttributes attributes = new PlayerAttributes();

    private final Appearance appearance = new Appearance(this);
    private final BonusManager bonusManager = new BonusManager();
    private final Stopwatch clickDelay = new Stopwatch();
    private final CopyOnWriteArrayList<DropLogEntry> dropLog = new CopyOnWriteArrayList<DropLogEntry>();
    private final Stopwatch emoteDelay = new Stopwatch();
    private final Stopwatch foodTimer = new Stopwatch();
    private final FrameUpdater frameUpdater = new FrameUpdater();
    private final CopyOnWriteArrayList<KillsEntry> killsTracker = new CopyOnWriteArrayList<KillsEntry>();
    private final Stopwatch lastDfsTimer = new Stopwatch();
    private final Stopwatch lastItemPickup = new Stopwatch();
    private final Stopwatch lastSummon = new Stopwatch();
    private final Stopwatch lastVengeance = new Stopwatch();
    private final Stopwatch lastChickenUsage = new Stopwatch();
    private final Stopwatch lastDamagedFromStorm = new Stopwatch();
    private final List<NPC> localNpcs = new LinkedList<NPC>();
    private final List<Player> localPlayers = new LinkedList<Player>();
    private final Stopwatch lougoutTimer = new Stopwatch();
    private final CopyOnWriteArrayList<NPC> npc_faces_updated = new CopyOnWriteArrayList<NPC>();
    private final PacketSender packetSender = new PacketSender(this);
    private final PlayerKillingAttributes playerKillingAttributes = new PlayerKillingAttributes(this);
    private final PointsHandler pointsHandler = new PointsHandler(this);
    private final Stopwatch potionTimer = new Stopwatch();
    private final PlayerProcess process = new PlayerProcess(this);
    private final Stopwatch recordedLogin = new Stopwatch();
    private final Stopwatch specialRestoreTimer = new Stopwatch();
    private final Stopwatch tolerance = new Stopwatch();
    private String killerName = "";
    private boolean acceptingAid;
    private int[] ancientArmourCharges = new int[15];
    private boolean autoRetaliate;
    private boolean autocast;
    private CombatSpell autocastSpell, castSpell, previousCastSpell;
    private int blowpipeCharges;
    private boolean bonecrushEffect = true;
    private boolean[] bossPets = new boolean[100];
    private int[] brawlerCharges = new int[11];
    private DwarfCannon cannon;
    private CharacterAnimations characterAnimations = new CharacterAnimations();
    private ChatMessage chatMessages = new ChatMessage();
    private String clanChatName;
    private boolean clickToTeleport;
    private CombatSpecial combatSpecial;
    private boolean[] crossedObstacles = new boolean[7];
    private boolean crossingObstacle;
    private int currentBankTab;
    private int currentBookPage;
    private ClanChat currentClanChat;
    private Room currentRoom;
    private Task currentTask;
    private int dfsCharges;
    private DialogueIterator dialogueIterator;
    private boolean drainingPrayer;
    private int effigy;
    private Equipment equipment = new Equipment(this);
    private boolean experienceLocked;
    private Farming farming = new Farming(this);
    private FightType fightType = FightType.UNARMED_PUNCH;
    private int fireAmmo = 11212;
    private int fireImmunity, fireDamageModifier;
    private int[] forceMovement = new int[7];
    private int forgingCharges;
    private GameType gameType = GameType.SOLO;
    private boolean ghostWalking;
    private boolean hasVengeance;
    private boolean inActive;
    private Input inputHandling;
    private Item interactingItem;
    private GameObject interactingObject;
    private int interfaceId, walkableInterfaceId, multiIcon;
    private Inventory inventory = new Inventory(this);
    private boolean isBanking, noteWithdrawal, swapMode;
    private boolean isCoughing;
    private boolean isDying;
    private boolean isRunning = true, isResting;
    private boolean killsTrackerOpen;
    private CombatType lastCombatType = CombatType.MELEE;
    private int lastTomed;
    private int[] leechedBonuses = new int[7];
    private List<Integer> lootList;
    private int minutesBonusExp = -1;
    private long moneyInPouch;
    private boolean newPlayer;
    private boolean openBank;
    private int[] ores = new int[2];
    private int overloadPotionTimer, prayerRenewalPotionTimer;
    private PlayerInteractingOption playerInteractingOption = PlayerInteractingOption.NONE;
    private boolean playerLocked;
    private int playerViewingIndex;
    private int poisonImmunity;
    private Prayerbook prayerbook = Prayerbook.NORMAL;
    private int[] previousTeleports = new int[]{0, 0, 0, 0};
    private boolean processFarming;
    private RangedWeaponData rangedWeaponData;
    private int recoilCharges;
    private boolean recoveringSpecialAttack;
    private boolean reffered;
    private boolean regionChange, allowRegionChangePacket;
    private PlayerRelations relations = new PlayerRelations(this);
    private Position resetPosition;
    private PlayerSaver saver = new PlayerSaver(new PlayerSerializer(), new PlayerFileWriter());
    private Pouch selectedPouch;
    private int selectedSkillingItem;
    private PlayerSession session;
    private boolean settingUpCannon;
    private int shadowState;
    private int skillAnimation;
    private SkillManager skillManager = new SkillManager(this);
    private int skullIcon = -1, skullTimer;
    private Slayer slayer = new Slayer(this);
    private boolean soundsActive, musicActive;
    private boolean specialActivated;
    private int specialPercentage = 100;
    private MagicSpellbook spellbook = MagicSpellbook.ANCIENT;
    private boolean spiritdebug;
    private int staffOfLightEffect;
    private PlayerState state = PlayerState.IN_LOBBY;
    private Summoning summoning = new Summoning(this);
    private boolean targeted;
    private int teleblockTimer;
    private boolean toggledglobalmessages;
    private long totalPlayTime;
    private int trapsLaid;
    private Item untradeableDropItem;
    private Object[] usableObject;
    private WalkToTask walkToTask;
    private WeaponInterface weapon;
    private Costume costume = new Costume();
    private CostumePowerUpHandler powerUpHandler = new CostumePowerUpHandler();
    private List<Player> spectators = new ArrayList<>();

    public Player(PlayerSession playerIO) {
        super(RoomLocations.LOBBY.copy());
        this.session = playerIO;
    }

    @Override
    public void appendDeath() {
        if (!isDying) {
            isDying = true;
            TaskManager.submit(new PlayerDeathTask(this));
        }
    }

    @Override
    public CombatStrategy determineStrategy() {
        if (specialActivated && castSpell == null) {

            if (combatSpecial.getCombatType() == CombatType.MELEE) {
                return CombatStrategies.getDefaultMeleeStrategy();
            } else if (combatSpecial.getCombatType() == CombatType.RANGED) {
                setRangedWeaponData(RangedWeaponData.getData(this));
                return CombatStrategies.getDefaultRangedStrategy();
            } else if (combatSpecial.getCombatType() == CombatType.MAGIC) {
                return CombatStrategies.getDefaultMagicStrategy();
            }
        }

        if (castSpell != null || autocastSpell != null) {
            return CombatStrategies.getDefaultMagicStrategy();
        }

        RangedWeaponData data = RangedWeaponData.getData(this);
        if (data != null) {
            setRangedWeaponData(data);
            return CombatStrategies.getDefaultRangedStrategy();
        }

        return CombatStrategies.getDefaultMeleeStrategy();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Player)) {
            return false;
        }

        Player p = (Player) o;
        String userName = getAttributes().getUserName();
        return p.getIndex() == getIndex() || p.getAttributes().getUserName().equals(userName);
    }

    @Override
    public int getAttackSpeed() {
        int speed = weapon.getSpeed();
        String weapon = equipment.get(Equipment.WEAPON_SLOT).getDefinition().getName();
        if (getCurrentlyCasting() != null) {
            if (getCurrentlyCasting() == CombatSpells.BLOOD_BLITZ.getSpell() || getCurrentlyCasting() == CombatSpells
                    .SHADOW_BLITZ.getSpell() || getCurrentlyCasting() == CombatSpells.SMOKE_BLITZ.getSpell() ||
                    getCurrentlyCasting() == CombatSpells.ICE_BLITZ.getSpell()) {
                return 5;
            } else {
                return 6;
            }
        }
        int weaponId = equipment.get(Equipment.WEAPON_SLOT).getId();

        if (fightType == FightType.CROSSBOW_RAPID || fightType == FightType.LONGBOW_RAPID || weaponId == 6522 &&
                fightType == FightType.KNIFE_RAPID || weapon.contains("rapier")) {
            if (weaponId != 11235) {
                speed--;
            }
        } else if (weaponId != 6522 && weaponId != 15241 && (fightType == FightType.SHORTBOW_RAPID || fightType ==
                FightType.DART_RAPID || fightType == FightType.KNIFE_RAPID || fightType == FightType.THROWNAXE_RAPID
                || fightType == FightType.JAVELIN_RAPID) || weaponId == 11730) {
            speed -= 2;
        }
        return speed;
    }

    @Override
    public int getBaseAttack(CombatType type) {
        if (type == CombatType.RANGED)
            return skillManager.getCurrentLevel(Skill.RANGED);
        else if (type == CombatType.MAGIC)
            return skillManager.getCurrentLevel(Skill.MAGIC);
        return skillManager.getCurrentLevel(Skill.ATTACK);
    }

    @Override
    public int getBaseDefence(CombatType type) {
        if (type == CombatType.MAGIC)
            return skillManager.getCurrentLevel(Skill.MAGIC);
        return 99;
    }

    @Override
    public int getConstitution() {
        return getSkillManager().getCurrentLevel(Skill.CONSTITUTION);
    }

    public int getDefence() {
        return getSkillManager().getCurrentLevel(Skill.DEFENCE);
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public void heal(int amount) {
        boolean nexEffect = getEquipment().wearingNexAmours();
        int level = skillManager.getMaxLevel(Skill.CONSTITUTION);
        int nexHp = level + 400;
        int currentlevel = skillManager.getCurrentLevel(Skill.CONSTITUTION);

        if (currentlevel >= level && !nexEffect) {
            return;
        }
        if (currentlevel >= nexHp && nexEffect) {
            return;
        }

        if ((currentlevel + amount) >= (nexEffect ? nexHp : level)) {
            setConstitution(nexEffect ? nexHp : level);
        } else if ((currentlevel + amount) < (nexEffect ? nexHp : level)) {
            setConstitution(currentlevel + amount);
        }

        getSkillManager().updateSkill(Skill.CONSTITUTION);
    }

    @Override
    public boolean isPlayer() {
        return true;
    }

    @Override
    public void poisonVictim(Character victim, CombatType type) {
        if (type == CombatType.MELEE || weapon == WeaponInterface.DART || weapon == WeaponInterface.KNIFE || weapon
                == WeaponInterface.THROWNAXE || weapon == WeaponInterface.JAVELIN) {
            CombatFactory.poisonEntity(victim, CombatPoisonData.getPoisonType(equipment.get(Equipment.WEAPON_SLOT)));
        } else if (type == CombatType.RANGED) {
            CombatFactory.poisonEntity(victim, CombatPoisonData.getPoisonType(equipment.get(Equipment
                    .AMMUNITION_SLOT)));
        }
    }

    @Override
    public Character setConstitution(int constitution) {
        if (isDying) {
            return this;
        }
        skillManager.setCurrentLevel(Skill.CONSTITUTION, constitution);
        packetSender.sendSkill(Skill.CONSTITUTION);
        if (getConstitution() <= 0 && !isDying)
            appendDeath();
        return this;
    }

    public Character setDefence(int defence) {
        skillManager.setCurrentLevel(Skill.DEFENCE, defence);
        packetSender.sendSkill(Skill.DEFENCE);
        return this;
    }

    public boolean busy() {
        return interfaceId > 0 || isBanking || isResting;
    }

    public boolean checkItem(int slot, int id) {
        if (this.getEquipment().getItems()[slot].getId() == id) {
            return true;
        } else {
            return false;
        }
    }

    public boolean couldHeal() {
        boolean nexEffect = getEquipment().wearingNexAmours();
        int level = skillManager.getMaxLevel(Skill.CONSTITUTION);
        int nexHp = level + 400;
        int currentlevel = skillManager.getCurrentLevel(Skill.CONSTITUTION);

        if (currentlevel >= level && !nexEffect) {
            return false;
        }
        if (currentlevel >= nexHp && nexEffect) {
            return false;
        }

        return true;
    }

    public void decrementSkullTimer() {
        skullTimer -= 50;
    }

    public void decrementSpecialPercentage(int drainAmount) {
        this.specialPercentage -= drainAmount;

        if (specialPercentage < 0) {
            specialPercentage = 0;
        }
    }

    public void decrementStaffOfLightEffect() {
        this.staffOfLightEffect--;
    }

    public void decrementTeleblockTimer() {
        teleblockTimer--;
    }

    public void dispose() {
        packetSender.sendLogout();
    }

    public boolean experienceLocked() {
        return experienceLocked;
    }



    /*
     * Getters & Setters
     */

    public boolean getBossPet(int i) {
        return bossPets[i];
    }

    public CombatStrategy getStrategy(int npc) {
        return CombatStrategies.getStrategy(npc);
    }

    public boolean hasStaffOfLightEffect() {
        return staffOfLightEffect > 0;
    }

    public boolean hasVengeance() {
        return hasVengeance;
    }

    public void incrementDfsCharges(int amount) {
        this.dfsCharges += amount;
    }

    public void incrementSpecialPercentage(int gainAmount) {
        this.specialPercentage += gainAmount;

        if (specialPercentage > 100) {
            specialPercentage = 100;
        }
    }

    public boolean logout() {
        if (getCombatBuilder().isBeingAttacked()) {
            getPacketSender().sendMessage("You must wait a few seconds after being out of combat before doing this.");
            return false;
        }
        if (getConstitution() <= 0 || isDying || settingUpCannon || crossingObstacle) {
            getPacketSender().sendMessage("You cannot log out at the moment.");
            return false;
        }
        return true;
    }

    public boolean musicActive() {
        return musicActive;
    }

    public boolean newPlayer() {
        return newPlayer;
    }

    public void newStance() {
        WeaponAnimations.update(this);
        this.getUpdateFlag().flag(Flag.APPEARANCE);
        return;
    }

    public void wearCostume(Costume costume) {
        powerUpHandler.applyPowerUp(this, costume.getPowerUp());
        equipment.resetItems();
        equipment.set(Equipment.HEAD_SLOT, getItem(costume.getHelmId()));
        equipment.set(Equipment.CAPE_SLOT, getItem(costume.getCapeId()));
        equipment.set(Equipment.AMULET_SLOT, getItem(costume.getAmuletId()));
        equipment.set(Equipment.BODY_SLOT, getItem(costume.getPlatebodyId()));
        equipment.set(Equipment.LEG_SLOT, getItem(costume.getPlatelegsId()));
        equipment.set(Equipment.FEET_SLOT, getItem(costume.getBootsId()));
        equipment.set(Equipment.HANDS_SLOT, getItem(costume.getGlovesId()));
        getUpdateFlag().flag(Flag.APPEARANCE);
        equipment.refreshItems();
        setCostume(costume);
    }

    private Item getItem(int helmId) {
        return new Item(helmId);
    }

    public void process() {
        process.sequence();
    }

    public void restart() {
        setStunDelay(0);
        setFreezeDelay(0);
        setOverloadPotionTimer(0);
        setPrayerRenewalPotionTimer(0);
        setSpecialPercentage(100);
        setSpecialActivated(false);
        CombatSpecial.updateBar(this);
        setHasVengeance(false);
        setSkullTimer(0);
        setSkullIcon(0);
        setTeleblockTimer(0);
        setPoisonDamage(0);
        setStaffOfLightEffect(0);
        performAnimation(new Animation(65535));
        WeaponInterfaces.assign(this, getEquipment().get(Equipment.WEAPON_SLOT));
        WeaponAnimations.update(this);
        PrayerHandler.deactivateAll(this);
        CurseHandler.deactivateAll(this);
        getEquipment().refreshItems();
        getInventory().refreshItems();
        getSkillManager().setMaxStats();
        setDying(false);
        getMovementQueue().setLockMovement(false).reset();
        getUpdateFlag().flag(Flag.APPEARANCE);
    }

    public void save() {
        if (session.getState() != SessionState.LOGGED_IN && session.getState() != SessionState.LOGGING_OUT) {
            return;
        }
        saver.saveEntity(this);
        //PlayerSaving.save(this);
    }

    public int setBlowpipeCharges(int blowpipeCharges) {
        return this.blowpipeCharges = blowpipeCharges;
    }

    public Player setBossPet(int i, boolean completed) {
        bossPets[i] = completed;
        return this;
    }

    public Player setCrossedObstacle(int i, boolean completed) {
        crossedObstacles[i] = completed;
        return this;
    }

    public void setExperienceLocked(boolean experienceLocked) {
        this.experienceLocked = experienceLocked;
    }

    public int setForgingCharges(int forgingCharges) {
        return this.forgingCharges = forgingCharges;
    }

    public void setHasVengeance(boolean hasVengeance) {
        this.hasVengeance = hasVengeance;
    }

    public void setInactive(boolean inActive) {
        this.inActive = inActive;
    }

    public void setMinutesBonusExp(int minutesBonusExp, boolean add) {
        this.minutesBonusExp = (add ? this.minutesBonusExp + minutesBonusExp : minutesBonusExp);
    }

    public void setMusicActive(boolean musicActive) {
        this.musicActive = musicActive;
    }

    public void setNewPlayer(boolean newPlayer) {
        this.newPlayer = newPlayer;
    }

    public void setNoteWithdrawal(boolean noteWithdrawal) {
        this.noteWithdrawal = noteWithdrawal;
    }

    public void setPreviousCastSpell(CombatSpell previousCastSpell) {
        this.previousCastSpell = previousCastSpell;
    }

    public void setProcessFarming(boolean processFarming) {
        this.processFarming = processFarming;
    }

    public int setRecoilCharges(int recoilCharges) {
        return this.recoilCharges = recoilCharges;
    }

    public void setReffered(boolean reffered) {
        this.reffered = reffered;
    }

    public Player setRegionChange(boolean regionChange) {
        this.regionChange = regionChange;
        return this;
    }

    public void setSoundsActive(boolean soundsActive) {
        this.soundsActive = soundsActive;
    }

    public void setSwapMode(boolean swapMode) {
        this.swapMode = swapMode;
    }

    public Player setUsableObject(Object[] usableObject) {
        this.usableObject = usableObject;
        return this;
    }

    public Player setUsableObject(int index, Object usableObject) {
        this.usableObject[index] = usableObject;
        return this;
    }

    public boolean shouldProcessFarming() {
        return processFarming;
    }

    public boolean soundsActive() {
        return soundsActive;
    }

    public boolean swapMode() {
        return swapMode;
    }

    public boolean toggledGlobalMessages() {
        return toggledglobalmessages;
    }

    public boolean withdrawAsNote() {
        return noteWithdrawal;
    }

    public int[] getAncientArmourCharges() {
        return this.ancientArmourCharges;
    }

    public void setAncientArmourCharges(int[] ancientArmourCharges) {
        this.ancientArmourCharges = ancientArmourCharges;
    }

    public Appearance getAppearance() {
        return appearance;
    }

    public PlayerAttributes getAttributes() {
        return attributes;
    }

    public void setAttributes(PlayerAttributes attributes) {
        this.attributes = attributes;
    }

    /**
     * @return the autocastSpell
     */
    public CombatSpell getAutocastSpell() {
        return autocastSpell;
    }

    /**
     * @param autocastSpell the autocastSpell to set
     */
    public void setAutocastSpell(CombatSpell autocastSpell) {
        this.autocastSpell = autocastSpell;
    }

    public int getBlowpipeCharges() {
        return this.blowpipeCharges;
    }

    public boolean getBonecrushEffect() {
        return bonecrushEffect;
    }

    public void setBonecrushEffect(boolean bonecrushEffect) {
        this.bonecrushEffect = bonecrushEffect;
    }

    public BonusManager getBonusManager() {
        return bonusManager;
    }

    public boolean[] getBossPetsAll() {
        return bossPets;
    }

    public void setBossPetsAll(boolean[] boolAray) {
        this.bossPets = boolAray;
    }

    public int[] getBrawlerChargers() {
        return this.brawlerCharges;
    }

    public DwarfCannon getCannon() {
        return cannon;
    }

    public Player setCannon(DwarfCannon cannon) {
        this.cannon = cannon;
        return this;
    }

    /**
     * @return the castSpell
     */
    public CombatSpell getCastSpell() {
        return castSpell;
    }

    /**
     * @param castSpell the castSpell to set
     */
    public void setCastSpell(CombatSpell castSpell) {
        this.castSpell = castSpell;
    }

    /**
     * @return the equipmentAnimation
     */
    public CharacterAnimations getCharacterAnimations() {
        return characterAnimations;
    }

    /**
     * @return the equipmentAnimation
     */
    public void setCharacterAnimations(CharacterAnimations equipmentAnimation) {
        this.characterAnimations = equipmentAnimation.clone();
    }

    public ChatMessage getChatMessages() {
        return chatMessages;
    }

    public String getClanChatName() {
        return clanChatName;
    }

    public Player setClanChatName(String clanChatName) {
        this.clanChatName = clanChatName;
        return this;
    }

    public Stopwatch getClickDelay() {
        return clickDelay;
    }

    /**
     * @return the combatSpecial
     */
    public CombatSpecial getCombatSpecial() {
        return combatSpecial;
    }

    /**
     * @param combatSpecial the combatSpecial to set
     */
    public void setCombatSpecial(CombatSpecial combatSpecial) {
        this.combatSpecial = combatSpecial;
    }

    public boolean[] getCrossedObstacles() {
        return crossedObstacles;
    }

    public void setCrossedObstacles(boolean[] crossedObstacles) {
        this.crossedObstacles = crossedObstacles;
    }

    public int getCurrentBookPage() {
        return currentBookPage;
    }

    public void setCurrentBookPage(int currentBookPage) {
        this.currentBookPage = currentBookPage;
    }

    public ClanChat getCurrentClanChat() {
        return currentClanChat;
    }

    public Player setCurrentClanChat(ClanChat clanChat) {
        this.currentClanChat = clanChat;
        return this;
    }

    public Room getCurrentRoom() {
        return currentRoom;
    }

    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
        currentRoom.enter(this);
    }

    public Task getCurrentTask() {
        return currentTask;
    }

    public void setCurrentTask(Task currentTask) {
        this.currentTask = currentTask;
    }

    public int getDfsCharges() {
        return dfsCharges;
    }

    public void setDfsCharges(int amount) {
        this.dfsCharges = amount;
    }

    public DialogueIterator getDialogueIterator() {
        return dialogueIterator;
    }

    public void setDialogueIterator(DialogueIterator dialogueIterator) {
        this.dialogueIterator = dialogueIterator;
    }

    public CopyOnWriteArrayList<DropLogEntry> getDropLog() {
        return dropLog;
    }

    public int getEffigy() {
        return this.effigy;
    }

    public void setEffigy(int effigy) {
        this.effigy = effigy;
    }

    public Stopwatch getEmoteDelay() {
        return emoteDelay;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public Farming getFarming() {
        return farming;
    }

    /**
     * @return the fightType
     */
    public FightType getFightType() {
        return fightType;
    }

    /**
     * @param fightType the fightType to set
     */
    public void setFightType(FightType fightType) {
        this.fightType = fightType;
    }

    /**
     * @return the fireAmmo
     */
    public int getFireAmmo() {
        return fireAmmo;
    }

    /**
     * @param fireAmmo the fireAmmo to set
     */
    public void setFireAmmo(int fireAmmo) {
        this.fireAmmo = fireAmmo;
    }

    public int getFireDamageModifier() {
        return fireDamageModifier;
    }

    public Player setFireDamageModifier(int fireDamageModifier) {
        this.fireDamageModifier = fireDamageModifier;
        return this;
    }

    public int getFireImmunity() {
        return fireImmunity;
    }

    public Player setFireImmunity(int fireImmunity) {
        this.fireImmunity = fireImmunity;
        return this;
    }

    public Stopwatch getFoodTimer() {
        return foodTimer;
    }

    public int[] getForceMovement() {
        return forceMovement;
    }

    public Player setForceMovement(int[] forceMovement) {
        this.forceMovement = forceMovement;
        return this;
    }

    public int getForgingCharges() {
        return forgingCharges;
    }

    public FrameUpdater getFrameUpdater() {
        return this.frameUpdater;
    }

    public GameType getGameType() {
        return gameType;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }

    public Input getInputHandling() {
        return inputHandling;
    }

    public void setInputHandling(Input inputHandling) {
        this.inputHandling = inputHandling;
    }

    public Item getInteractingItem() {
        return interactingItem;
    }

    public void setInteractingItem(Item interactingItem) {
        this.interactingItem = interactingItem;
    }

    public GameObject getInteractingObject() {
        return interactingObject;
    }

    public Player setInteractingObject(GameObject interactingObject) {
        this.interactingObject = interactingObject;
        return this;
    }

    public int getInterfaceId() {
        return this.interfaceId;
    }

    public Player setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
        return this;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public CopyOnWriteArrayList<KillsEntry> getKillsTracker() {
        return killsTracker;
    }

    public CombatType getLastCombatType() {
        return lastCombatType;
    }

    public void setLastCombatType(CombatType lastCombatType) {
        this.lastCombatType = lastCombatType;
    }

    public Stopwatch getLastDfsTimer() {
        return lastDfsTimer;
    }

    public Stopwatch getLastItemPickup() {
        return lastItemPickup;
    }

    public Stopwatch getLastSummon() {
        return lastSummon;
    }

    public int getLastTomed() {
        return this.lastTomed;
    }

    public void setLastTomed(int lastTomed) {
        this.lastTomed = lastTomed;
    }

    public Stopwatch getLastVengeance() {
        return lastVengeance;
    }

    public int[] getLeechedBonuses() {
        return leechedBonuses;
    }

    /**
     * The player's local npcs list getter
     */
    public List<NPC> getLocalNpcs() {
        return localNpcs;
    }

    /**
     * The player's local players list.
     */
    public List<Player> getLocalPlayers() {
        return localPlayers;
    }

    public Stopwatch getLogoutTimer() {
        return lougoutTimer;
    }

    public List<Integer> getLootList() {
        return lootList;
    }

    public void setLootList(List<Integer> lootList) {
        this.lootList = lootList;
    }

    public int getMinutesBonusExp() {
        return minutesBonusExp;
    }

    public long getMoneyInPouch() {
        return moneyInPouch;
    }

    public void setMoneyInPouch(long moneyInPouch) {
        this.moneyInPouch = moneyInPouch;
    }

    public int getMoneyInPouchAsInt() {
        return moneyInPouch > Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) moneyInPouch;
    }

    public int getMultiIcon() {
        return multiIcon;
    }

    public Player setMultiIcon(int multiIcon) {
        this.multiIcon = multiIcon;
        return this;
    }

    public CopyOnWriteArrayList<NPC> getNpcFacesUpdated() {
        return npc_faces_updated;
    }

    public int[] getOres() {
        return ores;
    }

    public void setOres(int[] ores) {
        this.ores = ores;
    }

    public int getOverloadPotionTimer() {
        return overloadPotionTimer;
    }

    public void setOverloadPotionTimer(int overloadPotionTimer) {
        this.overloadPotionTimer = overloadPotionTimer;
    }

    public PacketSender getPacketSender() {
        return packetSender;
    }

    public PlayerInteractingOption getPlayerInteractingOption() {
        return playerInteractingOption;
    }

    public Player setPlayerInteractingOption(PlayerInteractingOption playerInteractingOption) {
        this.playerInteractingOption = playerInteractingOption;
        return this;
    }

    public PlayerKillingAttributes getPlayerKillingAttributes() {
        return playerKillingAttributes;
    }

    public int getPlayerViewingIndex() {
        return playerViewingIndex;
    }

    public void setPlayerViewingIndex(int playerViewingIndex) {
        this.playerViewingIndex = playerViewingIndex;
    }

    public PointsHandler getPointsHandler() {
        return pointsHandler;
    }

    public int getPoisonImmunity() {
        return poisonImmunity;
    }

    public void setPoisonImmunity(int poisonImmunity) {
        this.poisonImmunity = poisonImmunity;
    }

    public Stopwatch getPotionTimer() {
        return potionTimer;
    }

    public int getPrayerRenewalPotionTimer() {
        return prayerRenewalPotionTimer;
    }

    public void setPrayerRenewalPotionTimer(int prayerRenewalPotionTimer) {
        this.prayerRenewalPotionTimer = prayerRenewalPotionTimer;
    }

    public Prayerbook getPrayerbook() {
        return prayerbook;
    }

    public Player setPrayerbook(Prayerbook prayerbook) {
        this.prayerbook = prayerbook;
        return this;
    }

    public int[] getPreviousTeleports() {
        return previousTeleports;
    }

    public void setPreviousTeleports(int[] previousTeleports) {
        this.previousTeleports = previousTeleports;
    }

    /**
     * @return the rangedAmmo
     */
    public RangedWeaponData getRangedWeaponData() {
        return rangedWeaponData;
    }

    public void setRangedWeaponData(RangedWeaponData rangedWeaponData) {
        this.rangedWeaponData = rangedWeaponData;
    }

    public int getRecoilCharges() {
        return this.recoilCharges;
    }

    public Stopwatch getRecordedLogin() {
        return recordedLogin;
    }

    public PlayerRelations getRelations() {
        return relations;
    }

    public Position getResetPosition() {
        return resetPosition;
    }

    public void setResetPosition(Position resetPosition) {
        this.resetPosition = resetPosition;
    }

    public Pouch getSelectedPouch() {
        return selectedPouch;
    }

    public void setSelectedPouch(Pouch selectedPouch) {
        this.selectedPouch = selectedPouch;
    }

    public int getSelectedSkillingItem() {
        return selectedSkillingItem;
    }

    public void setSelectedSkillingItem(int selectedItem) {
        this.selectedSkillingItem = selectedItem;
    }

    public PlayerSession getSession() {
        return session;
    }

    public int getShadowState() {
        return shadowState;
    }

    public void setShadowState(int shadow) {
        this.shadowState = shadow;
    }

    public int getSkillAnimation() {
        return skillAnimation;
    }

    public Player setSkillAnimation(int animation) {
        this.skillAnimation = animation;
        return this;
    }

    public SkillManager getSkillManager() {
        return skillManager;
    }

    /**
     * @return the skullIcon
     */
    public int getSkullIcon() {
        return skullIcon;
    }

    /**
     * @param skullIcon the skullIcon to set
     */
    public void setSkullIcon(int skullIcon) {
        this.skullIcon = skullIcon;
    }

    /**
     * @return the skullTimer
     */
    public int getSkullTimer() {
        return skullTimer;
    }

    /**
     * @param skullTimer the skullTimer to set
     */
    public void setSkullTimer(int skullTimer) {
        this.skullTimer = skullTimer;
    }

    public Slayer getSlayer() {
        return slayer;
    }

    /**
     * @return the specialPercentage
     */
    public int getSpecialPercentage() {
        return specialPercentage;
    }

    /**
     * @param specialPercentage the specialPercentage to set
     */
    public void setSpecialPercentage(int specialPercentage) {
        this.specialPercentage = specialPercentage;
    }

    public Stopwatch getSpecialRestoreTimer() {
        return specialRestoreTimer;
    }

    public MagicSpellbook getSpellbook() {
        return spellbook;
    }

    public Player setSpellbook(MagicSpellbook spellbook) {
        this.spellbook = spellbook;
        return this;
    }

    public int getStaffOfLightEffect() {
        return staffOfLightEffect;
    }

    public void setStaffOfLightEffect(int staffOfLightEffect) {
        this.staffOfLightEffect = staffOfLightEffect;
    }

    public PlayerState getState() {
        return state;
    }

    public void setState(PlayerState state) {
        this.state = state;
    }

    public Summoning getSummoning() {
        return summoning;
    }

    /**
     * @return the teleblockTimer
     */
    public int getTeleblockTimer() {
        return teleblockTimer;
    }

    /**
     * @param teleblockTimer the teleblockTimer to set
     */
    public void setTeleblockTimer(int teleblockTimer) {
        this.teleblockTimer = teleblockTimer;
    }

    public Stopwatch getTolerance() {
        return tolerance;
    }

    public long getTotalPlayTime() {
        return totalPlayTime;
    }

    public void setTotalPlayTime(long amount) {
        this.totalPlayTime = amount;
    }

    public int getTrapsLaid() {
        return trapsLaid;
    }

    public void setTrapsLaid(int trapsLaid) {
        this.trapsLaid = trapsLaid;
    }

    public Item getUntradeableDropItem() {
        return untradeableDropItem;
    }

    public void setUntradeableDropItem(Item untradeableDropItem) {
        this.untradeableDropItem = untradeableDropItem;
    }

    public WalkToTask getWalkToTask() {
        return walkToTask;
    }

    public void setWalkToTask(WalkToTask walkToTask) {
        this.walkToTask = walkToTask;
    }

    public int getWalkableInterfaceId() {
        return walkableInterfaceId;
    }

    public void setWalkableInterfaceId(int interfaceId2) {
        this.walkableInterfaceId = interfaceId2;
    }

    /**
     * @return the weapon.
     */
    public WeaponInterface getWeapon() {
        return weapon;
    }

    /**
     * @param weapon the weapon to set.
     */
    public void setWeapon(WeaponInterface weapon) {
        this.weapon = weapon;
    }

    public boolean isAcceptAid() {
        return acceptingAid;
    }

    public void setAcceptAid(boolean acceptingAid) {
        this.acceptingAid = acceptingAid;
    }

    public boolean isAllowRegionChangePacket() {
        return allowRegionChangePacket;
    }

    public void setAllowRegionChangePacket(boolean allowRegionChangePacket) {
        this.allowRegionChangePacket = allowRegionChangePacket;
    }

    public boolean isAutoRetaliate() {
        return autoRetaliate;
    }

    public void setAutoRetaliate(boolean autoRetaliate) {
        this.autoRetaliate = autoRetaliate;
    }

    /**
     * @return the autocast
     */
    public boolean isAutocast() {
        return autocast;
    }

    /**
     * @param autocast the autocast to set
     */
    public void setAutocast(boolean autocast) {
        this.autocast = autocast;
    }

    public boolean isChangingRegion() {
        return this.regionChange;
    }

    public boolean isClickToTeleport() {
        return clickToTeleport;
    }

    public void setClickToTeleport(boolean clickToTeleport) {
        this.clickToTeleport = clickToTeleport;
    }

    public boolean isCoughing() {
        return isCoughing;
    }

    public void setCoughing(boolean isCoughing) {
        this.isCoughing = isCoughing;
    }

    public boolean isCrossingObstacle() {
        return crossingObstacle;
    }

    public Player setCrossingObstacle(boolean crossingObstacle) {
        this.crossingObstacle = crossingObstacle;
        return this;
    }

    public boolean isDrainingPrayer() {
        return drainingPrayer;
    }

    public void setDrainingPrayer(boolean drainingPrayer) {
        this.drainingPrayer = drainingPrayer;
    }

    public boolean isDying() {
        return isDying;
    }

    public void setDying(boolean isDying) {
        this.isDying = isDying;
    }

    public boolean isGhostWalking() {
        return ghostWalking;
    }

    public void setGhostWalking(boolean ghostWalking) {
        this.ghostWalking = ghostWalking;
    }

    public boolean isInActive() {
        return inActive;
    }

    public boolean isKillsTrackerOpen() {
        return killsTrackerOpen;
    }

    public void setKillsTrackerOpen(boolean killsTrackerOpen) {
        this.killsTrackerOpen = killsTrackerOpen;
    }

    public boolean isPlayerLocked() {
        return playerLocked;
    }

    public Player setPlayerLocked(boolean playerLocked) {
        this.playerLocked = playerLocked;
        return this;
    }

    public boolean isRecoveringSpecialAttack() {
        return recoveringSpecialAttack;
    }

    public void setRecoveringSpecialAttack(boolean recoveringSpecialAttack) {
        this.recoveringSpecialAttack = recoveringSpecialAttack;
    }

    public boolean isResting() {
        return isResting;
    }

    public Player setResting(boolean isResting) {
        this.isResting = isResting;
        return this;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public Player setRunning(boolean isRunning) {
        this.isRunning = isRunning;
        return this;
    }

    public boolean isSettingUpCannon() {
        return settingUpCannon;
    }

    public void setSettingUpCannon(boolean settingUpCannon) {
        this.settingUpCannon = settingUpCannon;
    }

    /**
     * @return the specialActivated
     */
    public boolean isSpecialActivated() {
        return specialActivated;
    }

    /**
     * @param specialActivated the specialActivated to set
     */
    public void setSpecialActivated(boolean specialActivated) {
        this.specialActivated = specialActivated;
    }

    public boolean isSpiritDebug() {
        return spiritdebug;
    }

    public void setSpiritDebug(boolean spiritdebug) {
        this.spiritdebug = spiritdebug;
    }

    public boolean isTargeted() {
        return targeted;
    }

    public void setTargeted(boolean targeted) {
        this.targeted = targeted;
    }

    public List<Player> getSpectators() {
        return spectators;
    }

    public Stopwatch getLastChickenUsage() {
        return lastChickenUsage;
    }

    public Stopwatch getLastDamagedFromStorm() {
        return lastDamagedFromStorm;
    }

    public String getKillerName() {
        return killerName;
    }

    public void setKillerName(String killerName) {
        this.killerName = killerName;
    }

    public CostumePowerUpHandler getPowerUpHandler() {
        return powerUpHandler;
    }

    public Costume getCostume() {
        return costume;
    }

    public void setCostume(Costume costume) {
        this.costume = costume;
    }
}