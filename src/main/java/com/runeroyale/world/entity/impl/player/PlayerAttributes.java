package com.runeroyale.world.entity.impl.player;

import com.runeroyale.model.PlayerRights;
import com.runeroyale.world.content.Emotes;

import java.util.ArrayList;
import java.util.List;

public class PlayerAttributes {

    private String hash;
    private List<String> obtainedCostumes = new ArrayList<>();
    private PlayerRights playerRights = PlayerRights.PLAYER;
    private String userName;
    private int forumsUID = -1;
    private String discordId = null;
    private List<Long> friendList = new ArrayList<>(200);
    private List<Long> ignoreList = new ArrayList<>(100);
    private ArrayList<Emotes.EmoteData> unlockedEmotes = new ArrayList<>();


    //TODO move these transient fields back out to simplify the LoginDecoding process
    private transient String hostAddress;
    private transient String mac;
    private transient String password;
    private transient String salt;
    private transient long userNameAsLong;
    private transient String uuid;

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public List<String> getObtainedCostumes() {
        return obtainedCostumes;
    }

    public PlayerRights getPlayerRights() {
        return playerRights;
    }

    public void setPlayerRights(PlayerRights playerRights) {
        this.playerRights = playerRights;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getForumsUID() {
        return forumsUID;
    }

    public void setForumsUID(int forumsUID) {
        this.forumsUID = forumsUID;
    }

    public String getDiscordId() {
        return discordId;
    }

    public void setDiscordId(String discordId) {
        this.discordId = discordId;
    }

    public List<Long> getFriendList() {
        return friendList;
    }

    public List<Long> getIgnoreList() {
        return ignoreList;
    }

    public ArrayList<Emotes.EmoteData> getUnlockedEmotes() {
        return unlockedEmotes;
    }

    public String getHostAddress() {
        return hostAddress;
    }

    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public long getUserNameAsLong() {
        return userNameAsLong;
    }

    public void setUserNameAsLong(long userNameAsLong) {
        this.userNameAsLong = userNameAsLong;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
