package com.runeroyale.world.entity.impl.player;

import com.runeroyale.util.Misc;
import com.runeroyale.world.content.battleroyale.GameType;
import com.runeroyale.world.content.battleroyale.rooms.Room;
import com.runeroyale.world.content.battleroyale.rooms.game.Game;
import com.runeroyale.world.content.battleroyale.rooms.game.storm.StormHandler;
import com.runeroyale.world.content.battleroyale.rooms.spectatorroom.SpectatorRoom;
import com.runeroyale.world.content.battleroyale.rooms.waitingroom.WaitingRoom;
import com.runeroyale.world.content.battleroyale.settings.BattleRoyaleGameSettings;

import static com.runeroyale.world.content.battleroyale.rooms.waitingroom.WaitingRoomTransitioner.TRANSITION_TIME;
import static com.runeroyale.world.content.battleroyale.settings.BattleRoyaleGameSettings.MAX_TICKS_TILL_GAME_START;
import static com.runeroyale.world.entity.impl.player.PlayerState.IN_GAME;

public class PlayerStateHandler {
    private static final int GAME_INTERFACE_ID = 42020;
    private static final int GAME_PLAYERS_KILLED = 42023;
    private static final int GAME_PLAYERS_LEFT_ID = 42022;
    private static final int GAME_NEXT_STORM_ID = 42024;
    private static final int WAITING_ROOM_INTERFACE_ID = 42028;
    private static final int WAITING_ROOM_MODE_INTERFACE = 42030;
    private static final int WAITING_ROOM_NOT_ENOUGH_PLAYERS_INTERFACE = 42032;
    private static final String WAITING_ROOM_NOT_ENOUGH_PLAYERS_MESSAGE = "Not Enough Players!";
    private static final int WAITING_ROOM_PLAYERS_JOINED_INTERFACE = 42031;
    private static final int WAITING_ROOM_TIME_LEFT_INTERFACE = 42036;

    public static void sequence(Player player) {
        switch (player.getState()) {
            case IN_LOBBY:
                sendLobbyRP(player);
                break;
            case IN_WAITING_ROOM:
                updateWaitingRoomInterface(player);
                sendWaitingRoomRP(player);
                break;
            case IN_GAME:
                updateGameInterface(player);
                sendAttackOption(player);
                sendGameRP(player);
                break;
            case DEAD:
                sendSpectatingRP(player);
                break;
        }
    }

    private static void sendLobbyRP(Player player) {
        player.getPacketSender().sendRichPressenceState("In Lobby");
        player.getPacketSender().sendRichPressenceDetails("");
    }

    private static void sendSpectatingRP(Player player) {
        SpectatorRoom spectatorRoom = (SpectatorRoom) player.getCurrentRoom();
        player.getPacketSender().sendRichPressenceState("Dead - Killed By " + player.getKillerName());
        player.getPacketSender().sendRichPressenceDetails("Spectating " + spectatorRoom.getSpectating(player).getAttributes().getUserName());
    }

    private static void sendGameRP(Player player) {
        Game game = (Game) player.getCurrentRoom();
        if (!game.isOver()) {
            int kills = game.getKills(player);
            player.getPacketSender().sendRichPressenceState("In Game - " + game.getPlayersInRoom().size() + " Remaining");
            player.getPacketSender().sendRichPressenceDetails("Killed " + kills + " " + (kills == 1 ? "person" : "people"));
        }
    }

    private static void sendWaitingRoomRP(Player player) {
        if (player.getCurrentRoom() instanceof WaitingRoom) {
            WaitingRoom room = (WaitingRoom) player.getCurrentRoom();
            player.getPacketSender().sendRichPressenceState("In Waiting Room");
            player.getPacketSender().sendRichPressenceDetails("Playing " + room.getGameType().toString());
        }
    }

    private static void sendAttackOption(Player player) {
        player.getPacketSender().sendInteractionOption("Attack", 2, true);
    }

    private static String getTotalPlayersInRoom(Player player) {
        Room playersRoom = player.getCurrentRoom();
        if (playersRoom != null) {
            int playersInRoom = playersRoom.getPlayersInRoom().size();
            return String.valueOf(playersInRoom);
        }
        return "0";
    }

    private static void sendDuoWaitingRoomInterface(Player player) {
        player.getPacketSender().sendString(WAITING_ROOM_MODE_INTERFACE, "DUOS");
        sendPlayersInRoomInterface(player);
        sendTimeLeftInterface(player);
    }

    private static void sendPlayersInRoomInterface(Player player) {
        player.getPacketSender().sendString(WAITING_ROOM_PLAYERS_JOINED_INTERFACE, getTotalPlayersInRoom(player));
    }

    private static void sendSoloWaitingRoomInterface(Player player) {
        player.getPacketSender().sendString(WAITING_ROOM_MODE_INTERFACE, "SOLO");
        sendPlayersInRoomInterface(player);
        sendTimeLeftInterface(player);
    }

    private static void sendTimeLeftInterface(Player player) {
        if (player.getCurrentRoom() instanceof WaitingRoom) {
            WaitingRoom waitingRoom = (WaitingRoom) player.getCurrentRoom();
            if (waitingRoom.getTransitioner().isTransitionTimerActive()) {
                String time = Misc.getMinutesSecondsForMilliseconds(TRANSITION_TIME - waitingRoom.getTransitioner()
                        .getTransitionTimer().elapsed());
                sendTimeLeftInterfaceHelper(player, "", time);
            } else {
                if (waitingRoom.getPlayersInRoom().size() < BattleRoyaleGameSettings.MIN_GAME_PLAYERS && waitingRoom.getTicks() >= MAX_TICKS_TILL_GAME_START) {
                    sendTimeLeftInterfaceHelper(player, WAITING_ROOM_NOT_ENOUGH_PLAYERS_MESSAGE, "");
                } else {
                    String time = Misc.getMinutesSecondsForMilliseconds((MAX_TICKS_TILL_GAME_START - waitingRoom.getTicks()) * 600);
                    sendTimeLeftInterfaceHelper(player, "", time);
                }
            }
        }
    }

    private static void sendTimeLeftInterfaceHelper(Player player, String notEnoughPlayersText, String timeLeftText) {
        player.getPacketSender().sendString(WAITING_ROOM_NOT_ENOUGH_PLAYERS_INTERFACE, notEnoughPlayersText);
        player.getPacketSender().sendString(WAITING_ROOM_TIME_LEFT_INTERFACE, timeLeftText);
    }

    private static void updateGameInterface(Player player) {
        //player.getPacketSender().sendInterfaceRemoval();
        Game game = (Game) player.getCurrentRoom();
        String time = Misc.getMinutesSecondsForMilliseconds(StormHandler.TRANSITION_TIME - game.getStorm()
                .getTransitionTimer().elapsed());
        player.getPacketSender().sendWalkableInterface(GAME_INTERFACE_ID);
        player.getPacketSender().sendString(GAME_PLAYERS_LEFT_ID, game.getPlayersInRoom().size() + "");
        player.getPacketSender().sendString(GAME_PLAYERS_KILLED, game.getKills(player) + "");
        player.getPacketSender().sendString(GAME_NEXT_STORM_ID, time);
    }

    private static void updateWaitingRoomInterface(Player player) {
        player.getPacketSender().sendWalkableInterface(WAITING_ROOM_INTERFACE_ID);
        if (player.getGameType().equals(GameType.SOLO)) {
            sendSoloWaitingRoomInterface(player);
        } else {
            sendDuoWaitingRoomInterface(player);
        }
    }

    public static boolean canAttack(PlayerState playerState, PlayerState otherPlayerState) {
        return playerState == IN_GAME && playerState == otherPlayerState;
    }
}