package com.runeroyale.model;

public enum GraphicHeight {

    LOW,

    MIDDLE,

    HIGH;
}
