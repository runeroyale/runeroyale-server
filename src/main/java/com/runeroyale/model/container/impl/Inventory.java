package com.runeroyale.model.container.impl;

import com.runeroyale.model.Item;
import com.runeroyale.model.container.ItemContainer;
import com.runeroyale.model.container.StackType;
import com.runeroyale.model.definitions.ItemDefinition;
import com.runeroyale.world.entity.impl.player.Player;

import java.util.Optional;

/**
 * Represents a player's inventory item container.
 *
 * @author relex lawl
 */

public class Inventory extends ItemContainer {

    public static final int INTERFACE_ID = 3214;

    /**
     * The Inventory constructor.
     *
     * @param player The player who's inventory is being represented.
     */
    public Inventory(Player player) {
        super(player);
    }

    @Override
    public Inventory switchItem(ItemContainer to, Item item, int slot, boolean sort, boolean refresh) {
        if (getItems()[slot].getId() != item.getId())
            return this;
        if (to.getFreeSlots() <= 0 && !(to.contains(item.getId()) && item.getDefinition().isStackable())) {
            to.full();
            return this;
        }
        if (to instanceof BeastOfBurden || to instanceof PriceChecker) {
            if (to instanceof PriceChecker && !item.sellable()) {
                getPlayer().getPacketSender().sendMessage("You cannot do that with this item because it cannot be sold.");
                return this;
            }
            if (item.getAmount() > to.getFreeSlots()) {
                if (!item.getDefinition().isStackable()) {
                    item.setAmount(to.getFreeSlots());
                }
            }
        }
        delete(item, slot, refresh, to);
        to.add(item);
        if (sort && getAmount(item.getId()) <= 0)
            sortItems();
        if (refresh) {
            refreshItems();
            to.refreshItems();
        }
        return this;
    }

    @Override
    public int capacity() {
        return 28;
    }

    @Override
    public StackType stackType() {
        return StackType.DEFAULT;
    }

    @Override
    public Inventory refreshItems() {
        getPlayer().getPacketSender().sendItemContainer(this, INTERFACE_ID);
        return this;
    }

    @Override
    public Inventory full() {
        getPlayer().getPacketSender().sendMessage("Not enough space in your inventory.");
        return this;
    }

    public boolean isEmpty() {
        return getFreeSlots() == capacity();
    }

    /**
     * Adds a set of items into the inventory.
     *
     * @param item the set of items to add.
     */
    public void addItemSet(Item[] item) {
        for (Item addItem : item) {
            if (addItem == null) {
                continue;
            }
            add(addItem);
        }
    }

    /**
     * Deletes a set of items from the inventory.
     *
     * @param optional the set of items to delete.
     */
    public void deleteItemSet(Optional<Item[]> optional) {
        if (optional.isPresent()) {
            for (Item deleteItem : optional.get()) {
                if (deleteItem == null) {
                    continue;
                }

                delete(deleteItem);
            }
        }
    }
}
