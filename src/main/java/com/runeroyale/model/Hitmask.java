package com.runeroyale.model;

import com.runeroyale.world.entity.impl.Character;

/**
 * Represents a Hit mask type/color.
 *
 * @author Gabriel Hannason
 */

public enum Hitmask {

    /*
     * A normal red hitmask.
     */
    RED,

    /*
     * An orange-like hitmask, representing a high-hit.
     */
    CRITICAL,

    /*
     * A green hitmask representing a poison hit
     */
    RED2,

    /*
     * A light-yellow hitmask
     */
    LIGHT_YELLOW,

    /*
     * A dark purple hitmask
     */
    DARK_PURPLE,

    /*
     * A dark red hitmask
     */
    DARK_RED,

    /*
     * A dark crit hitmask
     */
    DARK_CRIT,

    /*
     * A green hitmask representing poison
     */
    DARK_GREEN,

    /*
     * No hitmask
     */
    NONE,

    /*
     * A blue hitmask representing defence
     */
    BLUE;

    public static Hitmask getRedOrBlue(Character character) {
        return character.getDefence() > 0 ? BLUE : RED;
    }
}