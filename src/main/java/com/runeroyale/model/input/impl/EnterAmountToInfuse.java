package com.runeroyale.model.input.impl;

import com.runeroyale.model.input.EnterAmount;
import com.runeroyale.world.content.skill.impl.summoning.PouchMaking;
import com.runeroyale.world.entity.impl.player.Player;

public class EnterAmountToInfuse extends EnterAmount {

    @Override
    public void handleAmount(Player player, int amount) {
        if (player.getInterfaceId() != 63471) {
            player.getPacketSender().sendInterfaceRemoval();
            return;
        }
        PouchMaking.infusePouches(player, amount);
    }

}
