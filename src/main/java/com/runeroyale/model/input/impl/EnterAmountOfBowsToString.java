package com.runeroyale.model.input.impl;

import com.runeroyale.model.input.EnterAmount;
import com.runeroyale.world.content.skill.impl.fletching.Fletching;
import com.runeroyale.world.entity.impl.player.Player;

public class EnterAmountOfBowsToString extends EnterAmount {

    @Override
    public void handleAmount(Player player, int amount) {
        Fletching.stringBow(player, amount);
    }

}
