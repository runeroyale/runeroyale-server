package com.runeroyale.model.input.impl;

import com.runeroyale.model.input.EnterAmount;
import com.runeroyale.world.content.skill.impl.prayer.BonesOnAltar;
import com.runeroyale.world.entity.impl.player.Player;

public class EnterAmountOfBonesToSacrifice extends EnterAmount {

    @Override
    public void handleAmount(Player player, int amount) {
        BonesOnAltar.offerBones(player, amount);
    }

}
