package com.runeroyale.model.input.impl;

import com.runeroyale.model.input.Input;
import com.runeroyale.world.entity.impl.player.Player;

public class EnterFriendsHouse extends Input {

    @Override
    public void handleSyntax(Player player, String syntax) {
        player.getPacketSender().sendInterfaceRemoval();
        if (syntax.length() < 3 || player.getAttributes().getUserName().equalsIgnoreCase(syntax))
            return;
    }
}
