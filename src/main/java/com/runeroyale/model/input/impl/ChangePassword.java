package com.runeroyale.model.input.impl;

import com.runeroyale.GameSettings;
import com.runeroyale.model.input.Input;
import com.runeroyale.util.NameUtils;
import com.runeroyale.world.entity.impl.player.Player;
import org.mindrot.jbcrypt.BCrypt;

public class ChangePassword extends Input {

    @Override
    public void handleSyntax(Player player, String syntax) {
        player.getPacketSender().sendInterfaceRemoval();

        if (syntax == null || syntax.length() <= 2 || syntax.length() > 15 || !NameUtils.isValidName(syntax)) {
            player.getPacketSender().sendMessage("That password is invalid. Please try another password.");
            return;
        }
        if (syntax.contains("_")) {
            player.getPacketSender().sendMessage("Your password can not contain underscores.");
            return;
        }

        if (true) {
            player.getAttributes().setPassword(syntax);
            if (GameSettings.BCRYPT_HASH_PASSWORDS) {
                player.getAttributes().setSalt(BCrypt.gensalt(GameSettings.BCRYPT_ROUNDS)); //discussion: https://crypto.stackexchange.com/questions/18963/should-you-change-salt-when-changing-password
            }
            player.getPacketSender().sendMessage("Your password has been updated.");
        }

    }

}
