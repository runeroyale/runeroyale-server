package com.runeroyale.model.input.impl;

import com.runeroyale.model.input.EnterAmount;
import com.runeroyale.world.content.skill.impl.crafting.LeatherMaking;
import com.runeroyale.world.content.skill.impl.crafting.leatherData;
import com.runeroyale.world.entity.impl.player.Player;

public class EnterAmountOfLeatherToCraft extends EnterAmount {

    @Override
    public void handleAmount(Player player, int amount) {
        for (final leatherData l : leatherData.values()) {
            if (player.getSelectedSkillingItem() == l.getLeather()) {
                LeatherMaking.craftLeather(player, l, amount);
                break;
            }
        }
    }
}
