package com.runeroyale.model.input.impl;

import com.runeroyale.model.input.Input;
import com.runeroyale.world.content.clan.ClanChatManager;
import com.runeroyale.world.entity.impl.player.Player;

public class EnterClanChatToJoin extends Input {

    @Override
    public void handleSyntax(Player player, String syntax) {
        if (syntax.length() <= 1) {
            player.getPacketSender().sendMessage("Invalid syntax entered.");
            return;
        }
        ClanChatManager.join(player, syntax);
    }
}
