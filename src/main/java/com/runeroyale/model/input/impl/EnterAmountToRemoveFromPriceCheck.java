package com.runeroyale.model.input.impl;

import com.runeroyale.model.Item;
import com.runeroyale.model.container.impl.PriceChecker;
import com.runeroyale.model.input.EnterAmount;
import com.runeroyale.world.entity.impl.player.Player;

public class EnterAmountToRemoveFromPriceCheck extends EnterAmount {


    public EnterAmountToRemoveFromPriceCheck(int item, int slot) {
        super(item, slot);
    }

    @Override
    public void handleAmount(Player player, int amount) {
    }
}
