package com.runeroyale.model.input.impl;

import com.runeroyale.model.input.EnterAmount;
import com.runeroyale.world.entity.impl.player.Player;

public class EnterAmountToPriceCheck extends EnterAmount {

    public EnterAmountToPriceCheck(int item, int slot) {
        super(item, slot);
    }

    @Override
    public void handleAmount(Player player, int amount) {
    }
}
