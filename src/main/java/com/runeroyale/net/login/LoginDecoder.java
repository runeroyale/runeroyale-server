package com.runeroyale.net.login;

import com.runeroyale.GameSettings;
import com.runeroyale.net.PlayerSession;
import com.runeroyale.net.packet.PacketBuilder;
import com.runeroyale.net.packet.codec.PacketDecoder;
import com.runeroyale.net.packet.codec.PacketEncoder;
import com.runeroyale.net.security.IsaacRandom;
import com.runeroyale.util.Misc;
import com.runeroyale.util.NameUtils;
import com.runeroyale.world.World;
import com.runeroyale.world.entity.impl.player.Player;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;
import org.mindrot.jbcrypt.BCrypt;

import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.security.SecureRandom;

/**
 * * @author Gabriel Hannason
 */
public final class LoginDecoder extends FrameDecoder {

    private static final int CONNECTED = 0;
    private static final int LOGGING_IN = 1;
    private long seed;
    private int state = CONNECTED;

    public Player login(Channel channel, LoginDetailsMessage msg) {

        PlayerSession session = new PlayerSession(channel);

        Player player = new Player(session);

        setPlayerLoginCredentials(player, msg);

        int response = LoginResponses.getResponse(player, msg);

        if (playerPasswordNeedsHashing(player)) {
            hashPlayerPassword(player);
        }

        setPlayerDetails(player, msg);

        session.setPlayer(player);

        final boolean newAccount = response == LoginResponses.NEW_ACCOUNT;
        if (newAccount) {
            player.setNewPlayer(true);
            response = LoginResponses.LOGIN_SUCCESSFUL;
        }

        if (response == LoginResponses.LOGIN_SUCCESSFUL) {
            channel.write(new PacketBuilder().put((byte) 2).put((byte) player.getAttributes().getPlayerRights
                    ().ordinal()).put((byte) 0).toPacket());

            if (!World.getLoginQueue().contains(player)) {
                World.getLoginQueue().add(player);
            }

            return player;
        } else {
            sendReturnCode(channel, response);
            return null;
        }
    }

    public static void sendReturnCode(final Channel channel, final int code) {
        channel.write(new PacketBuilder().put((byte) code).toPacket()).addListener(arg0 -> arg0.getChannel().close());
    }

    private void hashPlayerPassword(Player player) {
        if (Misc.needsNewSalt(player.getAttributes().getSalt())) {
            player.getAttributes().setSalt(BCrypt.gensalt(GameSettings.BCRYPT_ROUNDS));
            System.out.println(player.getAttributes().getUserName() + " needs a new salt. Generated one, " +
                    "rounds (" + GameSettings.BCRYPT_ROUNDS + ")");
        }

        player.getAttributes().setHash(BCrypt.hashpw(player.getAttributes().getPassword(),
                player.getAttributes().getSalt()));
    }

    private boolean playerPasswordNeedsHashing(Player player) {
        return player.getAttributes().getHash() == null;
    }

    private void setPlayerDetails(Player player, LoginDetailsMessage msg) {
        player.getAttributes().setUserNameAsLong(NameUtils.stringToLong(msg.getUsername()));
        player.getAttributes().setMac(msg.getMac());
        player.getAttributes().setUuid(msg.getUuid());
        player.getAttributes().setHostAddress(msg.getHost());
    }

    private void setPlayerLoginCredentials(Player player, LoginDetailsMessage msg) {
        player.getAttributes().setUserName(msg.getUsername());
        player.getAttributes().setPassword(msg.getPassword());
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer) throws Exception {
        if (!channel.isConnected()) {
            return null;
        }
        switch (state) {
            case CONNECTED:
                if (buffer.readableBytes() < 1) {
                    return null;
                }
                int request = buffer.readUnsignedByte();
                if (request != 14) {
                    System.out.println("Invalid login request: " + request);
                    channel.close();
                    return null;
                }
                seed = new SecureRandom().nextLong();
                channel.write(new PacketBuilder().put((byte) 0).putLong(seed).toPacket());
                state = LOGGING_IN;
                return null;
            case LOGGING_IN:
                if (buffer.readableBytes() < 2) {
                    System.out.println("no readable bytes");
                    return null;
                }
                int loginType = buffer.readByte();
                if (loginType != 16 && loginType != 18) {
                    System.out.println("Invalid login type: " + loginType);
                    channel.close();
                    return null;
                }
                int blockLength = buffer.readByte() & 0xff;
                if (buffer.readableBytes() < blockLength) {
                    channel.close();
                    return null;
                }
                int magicId = buffer.readUnsignedByte();
                if (magicId != 0xFF) {
                    System.out.println("Invalid magic id! magicId: " + magicId);
                    channel.close();
                    return null;
                }
                int clientVersion = buffer.readShort();
                int memory = buffer.readByte();
                if (memory != 0 && memory != 1) {
                    System.out.println("Unhandled memory byte value");
                    channel.close();
                    return null;
                }

                int length = buffer.readUnsignedByte();
                /**
                 * Our RSA components.
                 */
                ChannelBuffer rsaBuffer = buffer.readBytes(length);
                BigInteger bigInteger = new BigInteger(rsaBuffer.array());
                bigInteger = bigInteger.modPow(GameSettings.RSA_EXPONENT, GameSettings.RSA_MODULUS);
                rsaBuffer = ChannelBuffers.wrappedBuffer(bigInteger.toByteArray());

                int securityId = rsaBuffer.readByte();
                if (securityId != 10) {
                    System.out.println("securityId id is not 10. It is " + securityId);
                    channel.close();
                    return null;
                }
                long clientSeed = rsaBuffer.readLong();
                long seedReceived = rsaBuffer.readLong();
                if (seedReceived != seed) {
                    System.out.println("Unhandled seed read: [seed, seedReceived] : [" + seed + ", " + seedReceived +
                            "");
                    channel.close();
                    return null;
                }
                int[] seed = new int[4];
                seed[0] = (int) (clientSeed >> 32);
                seed[1] = (int) clientSeed;
                seed[2] = (int) (this.seed >> 32);
                seed[3] = (int) this.seed;
                IsaacRandom decodingRandom = new IsaacRandom(seed);
                for (int i = 0; i < seed.length; i++) {
                    seed[i] += 50;
                }
                int uid = rsaBuffer.readInt();
                String username = Misc.readString(rsaBuffer);
                String password = Misc.readString(rsaBuffer);
                String mac = Misc.readString(rsaBuffer);
                String uuid = Misc.readString(rsaBuffer);
                //String serial = Misc.readString(rsaBuffer);
                if (username.length() > 12 || password.length() > 20) {
                    System.out.println("Username or password length too long");
                    return null;
                }
                username = Misc.formatText(username.toLowerCase());
                channel.getPipeline().replace("encoder", "encoder", new PacketEncoder(new IsaacRandom(seed)));
                channel.getPipeline().replace("decoder", "decoder", new PacketDecoder(decodingRandom));
                return login(channel, new LoginDetailsMessage(username, password, ((InetSocketAddress) channel
                        .getRemoteAddress()).getAddress().getHostAddress(), mac, uuid,
                        clientVersion, uid));
        }
        return null;
    }

}
