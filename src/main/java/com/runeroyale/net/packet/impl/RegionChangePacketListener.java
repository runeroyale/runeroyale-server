package com.runeroyale.net.packet.impl;

import com.runeroyale.model.RegionInstance.RegionInstanceType;
import com.runeroyale.net.packet.Packet;
import com.runeroyale.net.packet.PacketListener;
import com.runeroyale.world.clip.region.RegionClipping;
import com.runeroyale.world.content.CustomObjects;
import com.runeroyale.world.content.Sounds;
import com.runeroyale.world.content.skill.impl.hunter.Hunter;
import com.runeroyale.world.entity.impl.GroundItemManager;
import com.runeroyale.world.entity.impl.player.Player;


public class RegionChangePacketListener implements PacketListener {

    @Override
    public void handleMessage(Player player, Packet packet) {
        if (player.isAllowRegionChangePacket()) {
            RegionClipping.loadRegion(player.getPosition().getX(), player.getPosition().getY());
            player.getPacketSender().sendMapRegion();
            CustomObjects.handleRegionChange(player);
            GroundItemManager.handleRegionChange(player);
            Sounds.handleRegionChange(player);
            player.getTolerance().reset();
            Hunter.handleRegionChange(player);
            if (player.getRegionInstance() != null && player.getPosition().getX() != 1 && player.getPosition().getY() != 1) {
                if (player.getRegionInstance().equals(RegionInstanceType.BARROWS) || player.getRegionInstance().equals(RegionInstanceType.WARRIORS_GUILD))
                    player.getRegionInstance().destruct();
            }
            player.getNpcFacesUpdated().clear();
            player.setRegionChange(false).setAllowRegionChangePacket(false);
        }
    }
}
