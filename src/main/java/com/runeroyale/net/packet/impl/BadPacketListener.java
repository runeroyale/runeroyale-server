package com.runeroyale.net.packet.impl;

import com.runeroyale.net.packet.Packet;
import com.runeroyale.net.packet.PacketListener;
import com.runeroyale.world.World;
import com.runeroyale.world.content.PlayerLogs;
import com.runeroyale.world.content.PlayerPunishment.Jail;
import com.runeroyale.world.entity.impl.player.Player;

public class BadPacketListener implements PacketListener {

    @Override
    public void handleMessage(Player player, Packet packet) {
        player.getSkillManager().stopSkilling();
        World.sendMessage(player.getAttributes().getUserName() + " sent packet 109! Please report this to a Developer.");
        //DiscordMessager.sendStaffMessage("PACKET 109 was sent by " + player.getAttributes().getUserName() + ", badlistener jailed & kicked.");
        PlayerLogs.log(player.getAttributes().getUserName(), "Sent PACKET 109 and was jailed/kicked by badlistener.");
        if (!Jail.isJailed(player)) {
            Jail.jailPlayer(player);
        }
        World.deregister(player);
        return;
    }

}
