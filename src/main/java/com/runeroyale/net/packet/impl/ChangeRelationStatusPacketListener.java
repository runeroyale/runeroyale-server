package com.runeroyale.net.packet.impl;

import com.runeroyale.model.PlayerRelations.PrivateChatStatus;
import com.runeroyale.net.packet.Packet;
import com.runeroyale.net.packet.PacketListener;
import com.runeroyale.world.entity.impl.player.Player;

public class ChangeRelationStatusPacketListener implements PacketListener {

    @Override
    public void handleMessage(Player player, Packet packet) {
        int actionId = packet.readInt();
        player.getRelations().setStatus(PrivateChatStatus.forActionId(actionId), true);
    }

}
