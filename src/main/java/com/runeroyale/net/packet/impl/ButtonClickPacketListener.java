package com.runeroyale.net.packet.impl;

import com.runeroyale.GameSettings;
import com.runeroyale.model.definitions.WeaponInterfaces;
import com.runeroyale.model.input.impl.EnterClanChatToJoin;
import com.runeroyale.net.packet.Packet;
import com.runeroyale.net.packet.PacketListener;
import com.runeroyale.world.World;
import com.runeroyale.world.content.*;
import com.runeroyale.world.content.Sounds.Sound;
import com.runeroyale.world.content.battleroyale.costumes.CostumeTab;
import com.runeroyale.world.content.clan.ClanChat;
import com.runeroyale.world.content.clan.ClanChatManager;
import com.runeroyale.world.content.clan.Guild;
import com.runeroyale.world.content.combat.magic.Autocasting;
import com.runeroyale.world.content.combat.magic.MagicSpells;
import com.runeroyale.world.content.combat.prayer.CurseHandler;
import com.runeroyale.world.content.combat.prayer.PrayerHandler;
import com.runeroyale.world.content.skill.ChatboxInterfaceSkillAction;
import com.runeroyale.world.content.skill.impl.crafting.LeatherMaking;
import com.runeroyale.world.content.skill.impl.crafting.Tanning;
import com.runeroyale.world.content.skill.impl.fletching.Fletching;
import com.runeroyale.world.content.skill.impl.herblore.ingredientsBook;
import com.runeroyale.world.content.skill.impl.slayer.Slayer;
import com.runeroyale.world.content.skill.impl.smithing.SmithingData;
import com.runeroyale.world.content.skill.impl.summoning.PouchMaking;
import com.runeroyale.world.content.skill.impl.summoning.SummoningTab;
import com.runeroyale.world.entity.impl.player.Player;

import static com.runeroyale.util.FunctionsUtil.cases;
import static com.runeroyale.util.FunctionsUtil.runCase;

/**
 * This packet listener manages a button that the player has clicked upon.
 *
 * @author Gabriel Hannason
 */

public class ButtonClickPacketListener implements PacketListener {

    public static final int OPCODE = 185;

    @Override
    public void handleMessage(Player player, Packet packet) {

        int bankid = 0;
        int id = packet.readShort();

        //if (player.getAttributes().getPlayerRights().isStaff()) {
        //    player.getPacketSender().sendMessage("Clicked button: " + id);
        //}

        if (player.getCurrentRoom().handleButton(player, id)) {
            return;
        }
        if (checkHandlers(player, id)) {
            return;
        }

        runCase("interface removal", cases(-27454, -27534, -16534, 36002, 26003, 5384),
                player.getPacketSender()::sendInterfaceRemoval, id);
        runCase("chatbox interface", cases(2799, 2798, 1747, 1748, 8890, 8886, 8875, 8871, 8894),
                () -> ChatboxInterfaceSkillAction.handleChatboxInterfaceButtons(player, id), id);
        if (player.getDialogueIterator() != null) {
            runCase("dialogue interface", cases(2494, 2495, 2496, 2497, 2498, 2471, 2472, 2473, 2461, 2462, 2482, 2483, 2484, 2485),
                    () -> player.getDialogueIterator().handleOptionClick(id), id);
        }
        runCase("autoretaliate", cases(22845, 24115, 24010, 24041, 150),
                () -> player.setAutoRetaliate(!player.isAutoRetaliate()), id);

        switch (id) {
            case -26376:
                PlayersOnlineInterface.showInterface(player);
                break;
            case 14176:
                player.setUntradeableDropItem(null);
                player.getPacketSender().sendInterfaceRemoval();
                break;
            case 14175:
                player.getPacketSender().sendInterfaceRemoval();
                if (player.getUntradeableDropItem() != null && player.getInventory().contains(player.getUntradeableDropItem().getId())) {
                    player.getInventory().delete(player.getUntradeableDropItem());
                    PlayerLogs.log(player.getAttributes().getUserName(), "Player destroying item: " + player.getUntradeableDropItem().getId() + ", amount: " + player.getUntradeableDropItem().getAmount());
                    player.getPacketSender().sendMessage("Your item vanishes as it hits the floor.");
                    Sounds.sendSound(player, Sound.DROP_ITEM);
                }
                player.setUntradeableDropItem(null);
                break;
            case 1013:
                player.getSkillManager().setTotalGainedExp(0);
                break;
            case -26348:
                DropLog.open(player);
                break;
            case 350:
                player.getPacketSender().sendMessage("To autocast a spell, please right-click it and choose the autocast option.").sendTab(GameSettings.MAGIC_TAB).sendConfig(108, player.getAutocastSpell() == null ? 3 : 1);
                break;
            case 29335:
                if (player.getInterfaceId() > 0) {
                    player.getPacketSender().sendMessage("Please close the interface you have open before opening another one.");
                    return;
                }
                break;
            case 29455:
                if (player.getInterfaceId() > 0) {
                    player.getPacketSender().sendMessage("Please close the interface you have open before opening another one.");
                    return;
                }
                ClanChatManager.toggleLootShare(player);
                break;
            case 10001:
                if (player.getInterfaceId() == -1) {
                    Consumables.handleHealAction(player);
                } else {
                    player.getPacketSender().sendMessage("You cannot heal yourself right now.");
                }
                break;
            case 18025:
                if (PrayerHandler.isActivated(player, PrayerHandler.AUGURY)) {
                    PrayerHandler.deactivatePrayer(player, PrayerHandler.AUGURY);
                } else {
                    PrayerHandler.activatePrayer(player, PrayerHandler.AUGURY);
                }
                break;
            case 18018:
                if (PrayerHandler.isActivated(player, PrayerHandler.RIGOUR)) {
                    PrayerHandler.deactivatePrayer(player, PrayerHandler.RIGOUR);
                } else {
                    PrayerHandler.activatePrayer(player, PrayerHandler.RIGOUR);
                }
                break;
            case 10000:
            case 950:
                if (player.getInterfaceId() < 0)
                    player.getPacketSender().sendInterface(40030);
                else
                    player.getPacketSender().sendMessage("Please close the interface you have open before doing this.");
                break;
            case 10162:
            case -18269:
                player.getPacketSender().sendInterfaceRemoval();
                break;
            case 841:
                ingredientsBook.readBook(player, player.getCurrentBookPage() + 2, true);
                break;
            case 839:
                ingredientsBook.readBook(player, player.getCurrentBookPage() - 2, true);
                break;
            case 14922:
                player.getPacketSender().sendClientRightClickRemoval().sendInterfaceRemoval();
                break;
            case 14921:
                player.getPacketSender().sendMessage("Please visit the forums and ask for help in the support section.");
                break;
            case 5294:
                player.getPacketSender().sendClientRightClickRemoval().sendInterfaceRemoval();
                break;
            case 15002:
                if (!player.busy() && !player.getCombatBuilder().isBeingAttacked()) {
                    player.getSkillManager().stopSkilling();
                } else {
                    player.getPacketSender().sendMessage("You cannot open this right now.");
                }
                break;
            case 1095:
                player.setExperienceLocked(!player.experienceLocked());
                if (player.experienceLocked()) {
                    player.getPacketSender().sendMessage("Your EXP is now locked, revert this lock to get EXP again.");
                } else {
                    player.getPacketSender().sendMessage("Your EXP is unlocked, and you will gain EXP as normal.");
                }
                break;
            case 11004:
                player.getPacketSender().sendTab(GameSettings.SKILLS_TAB);
                break;
            case 29332:
                ClanChat clan = player.getCurrentClanChat();
                if (clan == null) {
                    player.getPacketSender().sendMessage("You are not in a clanchat channel.");
                    return;
                }
                ClanChatManager.leave(player, false);
                player.setClanChatName(null);
                break;
            case 29329:
                if (player.getInterfaceId() > 0) {
                    player.getPacketSender().sendMessage("Please close the interface you have open before opening another one.");
                    return;
                }
                player.setInputHandling(new EnterClanChatToJoin());
                player.getPacketSender().sendEnterInputPrompt("Enter the name of the clanchat channel you wish to join:");
                break;
            case 19158:
            case 152:
                player.setRunning(true);
                player.getPacketSender().sendRunStatus();
                break;
            case 15004:
                player.setExperienceLocked(!player.experienceLocked());
                String type = player.experienceLocked() ? "locked" : "unlocked";
                player.getPacketSender().sendMessage("Your experience is now " + type + ".");
                PlayerPanel.refreshPanel(player);
                break;
            case 27651:
            case 15001:
                if (player.getInterfaceId() == -1) {
                    player.getSkillManager().stopSkilling();
                    BonusManager.update(player);
                    player.getPacketSender().sendInterface(21172);
                } else
                    player.getPacketSender().sendMessage("Please close the interface you have open before doing this.");
                break;
            case 2458: //Logout
                if (player.logout()) {
                    World.getPlayers().remove(player);
                }
                break;
            //	case 10003:
        }
    }

    private boolean checkHandlers(Player player, int id) {
        if (DropsInterface.handleButton(id)) {
            DropsInterface.handleButtonClick(player, id);
            return true;
        }
        if (CostumeTab.clickCostumeTab(player, id)) {
            return true;
        }
        if (player.isPlayerLocked() && id != 2458) {
            return true;
        }
        if (SummoningTab.handleButtons(player, id)) {
            return true;
        }
        if (WeaponInterfaces.handleButtons(player, id)) {
            return true;
        }
        if (Sounds.handleButton(player, id)) {
            return true;
        }
        if (PrayerHandler.isButton(id)) {
            PrayerHandler.togglePrayerWithActionButton(player, id);
            return true;
        }
        if (CurseHandler.isButton(player, id)) {
            return true;
        }
        if (Autocasting.handleAutocast(player, id)) {
            return true;
        }
        if (SmithingData.handleButtons(player, id)) {
            return true;
        }
        if (PouchMaking.pouchInterface(player, id)) {
            return true;
        }
        if (Fletching.fletchingButton(player, id)) {
            return true;
        }
        if (LeatherMaking.handleButton(player, id) || Tanning.handleButton(player, id)) {
            return true;
        }
        if (Emotes.doEmote(player, id)) {
            return true;
        }
        if (KillsTracker.handleButtons(player, id)) {
            return true;
        }
        if (Slayer.handleRewardsInterface(player, id)) {
            return true;
        }
        if (PlayersOnlineInterface.handleButton(player, id)) {
            return true;
        }
        if (MagicSpells.handleMagicSpells(player, id)) {
            return true;
        }
        if (ClanChatManager.handleClanChatSetupButton(player, id)) {
            return true;
        }
        if (Guild.handleClanButtons(player, id)) {
            return true;
        }
        if (pageRedirectButtons(player, id)) {
            return true;
        }
        return false;
    }

    private boolean pageRedirectButtons(Player player, int id) {
        switch (id) {
            case -26337:
                player.getPacketSender().sendString(1, GameSettings.StoreUrl);
                player.getPacketSender().sendMessage("Attempting to open the forums");
                return true;
            case -26338:
                player.getPacketSender().sendString(1, GameSettings.RuleUrl);
                player.getPacketSender().sendMessage("Attempting to open the rules");
                return true;
            case -26339:
                player.getPacketSender().sendString(1, GameSettings.ForumUrl);
                player.getPacketSender().sendMessage("Attempting to open the store");
                return true;
            case -26336:
                player.getPacketSender().sendString(1, GameSettings.VoteUrl);
                player.getPacketSender().sendMessage("Attempting to open the Vote page");
                return true;
            case -26335:
                player.getPacketSender().sendString(1, GameSettings.HiscoreUrl);
                player.getPacketSender().sendMessage("Attempting to open the Hiscore page");
                return true;
            case -26334:
                player.getPacketSender().sendString(1, GameSettings.ReportUrl);
                player.getPacketSender().sendMessage("Attempting to open the report page");
                return true;
        }
        return false;
    }
}
