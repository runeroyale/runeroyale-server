package com.runeroyale.net.packet.impl;

import com.runeroyale.engine.task.Task;
import com.runeroyale.engine.task.TaskManager;
import com.runeroyale.engine.task.impl.WalkToTask;
import com.runeroyale.engine.task.impl.WalkToTask.FinalizedMovementTask;
import com.runeroyale.model.*;
import com.runeroyale.model.Locations.Location;
import com.runeroyale.model.definitions.GameObjectDefinition;
import com.runeroyale.model.definitions.ItemDefinition;
import com.runeroyale.model.definitions.NpcDefinition;
import com.runeroyale.net.packet.Packet;
import com.runeroyale.net.packet.PacketListener;
import com.runeroyale.util.Misc;
import com.runeroyale.world.World;
import com.runeroyale.world.clip.region.RegionClipping;
import com.runeroyale.world.content.CrystalChest;
import com.runeroyale.world.content.ItemForging;
import com.runeroyale.world.content.PlayerLogs;
import com.runeroyale.world.content.combat.range.ToxicBlowpipe;
import com.runeroyale.world.content.skill.impl.cooking.Cooking;
import com.runeroyale.world.content.skill.impl.cooking.CookingData;
import com.runeroyale.world.content.skill.impl.crafting.Flax;
import com.runeroyale.world.content.skill.impl.crafting.Gems;
import com.runeroyale.world.content.skill.impl.crafting.Jewelry;
import com.runeroyale.world.content.skill.impl.crafting.LeatherMaking;
import com.runeroyale.world.content.skill.impl.firemaking.Firelighter;
import com.runeroyale.world.content.skill.impl.firemaking.Firemaking;
import com.runeroyale.world.content.skill.impl.firemaking.Logdata.logData;
import com.runeroyale.world.content.skill.impl.fletching.BoltData;
import com.runeroyale.world.content.skill.impl.fletching.Fletching;
import com.runeroyale.world.content.skill.impl.herblore.Crushing;
import com.runeroyale.world.content.skill.impl.herblore.Herblore;
import com.runeroyale.world.content.skill.impl.herblore.PotionCombinating;
import com.runeroyale.world.content.skill.impl.herblore.WeaponPoison;
import com.runeroyale.world.content.skill.impl.prayer.BonesOnAltar;
import com.runeroyale.world.content.skill.impl.prayer.Prayer;
import com.runeroyale.world.content.skill.impl.slayer.SlayerTasks;
import com.runeroyale.world.content.skill.impl.smithing.EquipmentMaking;
import com.runeroyale.world.content.skill.impl.smithing.Smelting;
import com.runeroyale.world.entity.impl.npc.NPC;
import com.runeroyale.world.entity.impl.player.Player;

/**
 * This packet listener is called when a player 'uses' an item on another
 * entity.
 *
 * @author relex lawl
 */

public class UseItemPacketListener implements PacketListener {

    public final static int USE_ITEM = 122;
    public final static int ITEM_ON_NPC = 57;
    public final static int ITEM_ON_ITEM = 53;
    public final static int ITEM_ON_OBJECT = 192;
    public final static int ITEM_ON_GROUND_ITEM = 25;
    public static final int ITEM_ON_PLAYER = 14;

    /**
     * The PacketListener logger to debug information and print out errors.
     */
    // private final static Logger logger =
    // Logger.getLogger(UseItemPacketListener.class);
    private static void useItem(Player player, Packet packet) {
        if (player.isTeleporting() || player.getConstitution() <= 0)
            return;
        int interfaceId = packet.readLEShortA();
        int slot = packet.readShortA();
        int id = packet.readLEShort();
    }

    private static void itemOnItem(Player player, Packet packet) {
        int usedWithSlot = packet.readUnsignedShort();
        int itemUsedSlot = packet.readUnsignedShortA();
        if (usedWithSlot < 0 || itemUsedSlot < 0
                || itemUsedSlot > player.getInventory().capacity()
                || usedWithSlot > player.getInventory().capacity())
            return;
        Item usedWith = player.getInventory().getItems()[usedWithSlot];
        Item itemUsedWith = player.getInventory().getItems()[itemUsedSlot];
        if (player.getAttributes().getPlayerRights() == PlayerRights.DEVELOPER) {
            player.getPacketSender().sendMessage("ItemOnItem - <shad=000000><col=ffffff>[<col=ff774a>" + ItemDefinition.forId(itemUsedWith.getId()).getName() + ":" + itemUsedWith.getId() + ":" + itemUsedWith.getAmount() + " <col=ffffff>was used on <col=4AD2FF>" + ItemDefinition.forId(usedWith.getId()).getName() + ":" + usedWith.getId() + ":" + usedWith.getAmount() + "<col=ffffff>]");
        }
        if (usedWith.getId() == 9003 && itemUsedWith.getId() == 989) {
            CrystalChest.sendRewardInterface(player);
            return;
        }

        for (int i = 0; i < Firelighter.values().length; i++) {
            if (usedWith.getId() == Firelighter.values()[i].getLighterId() || itemUsedWith.getId() == Firelighter.values()[i].getLighterId()) {
                Firelighter.handleFirelighter(player, i);
                break;
            }
        }

        for (int i = 0; i < Crushing.values().length; i++) {
            if (usedWith.getId() == Crushing.values()[i].getInput() || itemUsedWith.getId() == Crushing.values()[i].getInput()) {
                Crushing.handleCrushing(player, i);
                break;
            }
        }

        for (int i = 0; i < BoltData.values().length; i++) {
            if (usedWith.getId() == BoltData.values()[i].getTip() || itemUsedWith.getId() == BoltData.values()[i].getTip()) {
                Fletching.tipBolt(player, BoltData.values()[i].getTip());
                break;
            }
        }
        WeaponPoison.execute(player, itemUsedWith.getId(), usedWith.getId());
        if (itemUsedWith.getId() == 590 || usedWith.getId() == 590)
            Firemaking.lightFire(player, itemUsedWith.getId() == 590 ? usedWith.getId() : itemUsedWith.getId(), false, 1);
        if (itemUsedWith.getDefinition().getName().contains("(") && usedWith.getDefinition().getName().contains("("))
            PotionCombinating.combinePotion(player, usedWith.getId(), itemUsedWith.getId());
        if (usedWith.getId() == Herblore.VIAL || itemUsedWith.getId() == Herblore.VIAL) {
            if (Herblore.makeUnfinishedPotion(player, usedWith.getId()) || Herblore.makeUnfinishedPotion(player, itemUsedWith.getId()))
                return;
        }
        if (Herblore.finishPotion(player, usedWith.getId(), itemUsedWith.getId()) || Herblore.finishPotion(player, itemUsedWith.getId(), usedWith.getId()))
            return;
        if (usedWith.getId() == 12934 || itemUsedWith.getId() == 12926 || usedWith.getId() == 12926 || itemUsedWith.getId() == 12934) {
            ToxicBlowpipe.loadPipe(player);
        }
        if (usedWith.getId() == 946 || itemUsedWith.getId() == 946)
            Fletching.openSelection(player, usedWith.getId() == 946 ? itemUsedWith.getId() : usedWith.getId());
        if (usedWith.getId() == 1777 || itemUsedWith.getId() == 1777)
            Fletching.openBowStringSelection(player, usedWith.getId() == 1777 ? itemUsedWith.getId() : usedWith.getId());
        if (usedWith.getId() == 53 || itemUsedWith.getId() == 53 || usedWith.getId() == 52 || itemUsedWith.getId() == 52)
            Fletching.makeArrows(player, usedWith.getId(), itemUsedWith.getId());
        if (itemUsedWith.getId() == 1755 || usedWith.getId() == 1755)
            Gems.selectionInterface(player, usedWith.getId() == 1755 ? itemUsedWith.getId() : usedWith.getId());
        if (itemUsedWith.getId() == 1755 || usedWith.getId() == 1755)
            Fletching.openGemCrushingInterface(player, usedWith.getId() == 1755 ? itemUsedWith.getId() : usedWith.getId());
        if (usedWith.getId() == 1733 || itemUsedWith.getId() == 1733)
            LeatherMaking.craftLeatherDialogue(player, usedWith.getId(), itemUsedWith.getId());
        Herblore.handleSpecialPotion(player, itemUsedWith.getId(), usedWith.getId());
        if (itemUsedWith.getId() == 1759 || usedWith.getId() == 1759) {
            Jewelry.stringAmulet(player, itemUsedWith.getId(), usedWith.getId());
        }
        ItemForging.forgeItem(player, itemUsedWith.getId(), usedWith.getId());
    }

    @SuppressWarnings("unused")
    private static void itemOnObject(Player player, Packet packet) {
        @SuppressWarnings("unused")
        int interfaceType = packet.readShort();
        final int objectId = packet.readShort();
        final int objectY = packet.readLEShortA();
        final int itemSlot = packet.readLEShort();
        final int objectX = packet.readLEShortA();
        final int itemId = packet.readShort();

        if (itemSlot < 0 || itemSlot > player.getInventory().capacity())
            return;
        final Item item = player.getInventory().getItems()[itemSlot];
        if (item == null)
            return;
        final GameObject gameObject = new GameObject(objectId, new Position(
                objectX, objectY, player.getPosition().getZ()));
        if (objectId > 0 && objectId != 6 && !RegionClipping.objectExists(gameObject)) {
            //	player.getPacketSender().sendMessage("An error occured. Error code: "+id).sendMessage("Please report the error to a staff member.");
            return;
        }
        player.setInteractingObject(gameObject);
        if (player.getAttributes().getPlayerRights() == PlayerRights.DEVELOPER) {
            if (GameObjectDefinition.forId(gameObject.getId()) != null && GameObjectDefinition.forId(gameObject.getId()).getName() != null) {
                player.getPacketSender().sendMessage("ItemOnObject - <shad=000000><col=ffffff>[<col=ff774a>" + ItemDefinition.forId(itemId).getName() + ":" + itemId + " <col=ffffff>was used on <col=4AD2FF>" + GameObjectDefinition.forId(gameObject.getId()).getName() + ":" + gameObject.getId() + "<col=ffffff>]");
            } else {
                player.getPacketSender().sendMessage("ItemOnObject - <shad=000000><col=ffffff>[<col=ff774a>" + ItemDefinition.forId(itemId).getName() + ":" + itemId + " <col=ffffff>was used on <col=4AD2FF>" + gameObject.getId() + "<col=ffffff>] @red@(null obj. def)");
            }
        }
        player.setWalkToTask(new WalkToTask(player, gameObject.getPosition().copy(),
                gameObject.getSize(), new FinalizedMovementTask() {
            @Override
            public void execute() {
                if (CookingData.forFish(item.getId()) != null && CookingData.isRange(objectId)) {
                    player.setPositionToFace(gameObject.getPosition());
                    Cooking.selectionInterface(player, CookingData.forFish(item.getId()));
                    return;
                }
                if (Prayer.isBone(itemId) && objectId == 409) {
                    BonesOnAltar.openInterface(
                            player, itemId);
                    return;
                }
                if (player.getFarming().plant(itemId, objectId,
                        objectX, objectY))
                    return;
                if (player.getFarming().useItemOnPlant(itemId,
                        objectX, objectY))
                    return;
                if (GameObjectDefinition.forId(objectId) != null && GameObjectDefinition.forId(objectId).getName() != null && GameObjectDefinition.forId(objectId).getName().equalsIgnoreCase("furnace") && ItemDefinition.forId(itemId) != null && ItemDefinition.forId(itemId).getName() != null && ItemDefinition.forId(itemId).getName().contains("ore")) {
                    Smelting.openInterface(player);
                    return;
                }
                if (GameObjectDefinition.forId(objectId) != null && GameObjectDefinition.forId(objectId).getName() != null && GameObjectDefinition.forId(objectId).getName().equalsIgnoreCase("furnace") && itemId == 2357) {
                    Jewelry.jewelryInterface(player);
                }
                switch (objectId) {
                    case 172:
                    case 173:
                        if (itemId == 9003) {
                            CrystalChest.sendRewardInterface(player);
                        }
                        break;
                    case 2644:
                        if (itemId == 1779) {
                            Flax.showSpinInterface(player);
                        }
                        break;
                    case 6189:
                    case 11666:
                        Jewelry.jewelryInterface(player);
                        break;
                    case 7836:
                    case 7808:
                        if (itemId == 6055) {
                            int amt = player.getInventory().getAmount(6055);
                            if (amt > 0) {
                                player.getInventory().delete(6055, amt);
                                player.getPacketSender().sendMessage("You put the weed in the compost bin.");
                                player.getSkillManager().addExperience(Skill.FARMING, 1 * amt);
                            }
                        }
                        break;
                    case 4306:
                        EquipmentMaking.handleAnvil(player);
                        break;
                }
            }
        }));
    }

    private static void itemOnNpc(final Player player, Packet packet) {
        final int id = packet.readShortA();
        final int index = packet.readShortA();
        final int slot = packet.readLEShort();
        if (index < 0 || index > World.getNpcs().capacity()) {
            return;
        }
        if (slot < 0 || slot > player.getInventory().getItems().length) {
            return;
        }
        NPC npc = World.getNpcs().get(index);
        if (npc == null) {
            return;
        }
        if (player.getInventory().getItems()[slot].getId() != id) {
            return;
        }
        if (player.getAttributes().getPlayerRights() == PlayerRights.DEVELOPER) {
            player.getPacketSender().sendMessage("ItemOnNPC - <shad=000000><col=ffffff>[<col=ff774a>" + ItemDefinition.forId(id).getName() + ":" + id + " <col=ffffff>was used on <col=4AD2FF>" + npc.getDefinition().getName() + ":" + npc.getId() + "<col=ffffff>]");
        }

        switch (id) {
            case 9003:
                if (player.getLastTomed() != npc.getId()) {
                    player.getPacketSender().sendMessage(NpcDefinition.forId(npc.getId()).getName() + " has been scanned to your tome.");
                    if (npc.getId() == 1158) {
                        player.setLastTomed(1160);
                        break;
                    }
                }
                player.setLastTomed(npc.getId());
                break;
            case 4837:
                if (NpcDefinition.forId(npc.getId()).getName().contains("ark wizar")) {
                    TaskManager.submit(new Task(1, player, true) {
                        int tick = 0;

                        @Override
                        public void execute() {
                            if (tick >= 8) {
                                stop();
                            }
                            switch (tick) {
                                case 0:
                                    player.getInventory().delete(new Item(4837, 1));
                                    break;
                                case 1:
                                    player.forceChat("Can I have your autograph?");
                                    break;
                                case 3:
                                    npc.forceChat("Yea noob lol here u go");
                                    break;
                                case 4:
                                    npc.performAnimation(new Animation(1249));
                                    break;
                                case 7:
                                    player.getInventory().add(new Item(22040, 1));
                                    player.getPacketSender().sendMessage("The Dark Wizard signs your Necromancy Book, transforming it.");
                                    break;
                            }
                            tick++;
                        }
                    });
                }
                break;
        }
        for (int i = 0; i < logData.values().length; i++) {
            if (logData.values()[i].getLogId() == id) {
                if (npc.getId() == 7377) {
                    if (player.getSummoning().getFamiliar() == null) {
                        player.getPacketSender().sendMessage("That isn't your familiar!");
                        return;
                    }
                    if (player.getSummoning().getFamiliar().getSummonNpc().getId() != 7377) {
                        player.getPacketSender().sendMessage("You must have your own Pyrefiend to use that effect.");
                        return;
                    }
                    Firemaking.lightFire(player, id, false, 1);
                }
                break;
            }
        }
    }

    @SuppressWarnings("unused")
    private static void itemOnGroundItem(Player player) {
        player.getPacketSender().sendMessage("Nothing interesting happens.");
        //System.out.println("itemongrounditem");
    }

    @SuppressWarnings("unused")
    private static void itemOnPlayer(Player player, Packet packet) {
        int interfaceId = packet.readUnsignedShortA();
        int targetIndex = packet.readUnsignedShort();
        int itemId = packet.readUnsignedShort();
        int slot = packet.readLEShort();
        if (slot < 0 || slot > player.getInventory().capacity() || targetIndex > World.getPlayers().capacity())
            return;
        Player target = World.getPlayers().get(targetIndex);
        if (target == null)
            return;
        switch (itemId) {
            case 962:
                if (!player.getInventory().contains(962) || player.getAttributes().getPlayerRights() == PlayerRights.ADMINISTRATOR)
                    return;
                player.setPositionToFace(target.getPosition());
                player.performGraphic(new Graphic(1006));
                player.performAnimation(new Animation(451));
                player.getPacketSender().sendMessage("You pull the Christmas cracker...");
                target.getPacketSender().sendMessage("" + player.getAttributes().getUserName() + " pulls a Christmas cracker on you..");
                player.getInventory().delete(962, 1);
                player.getPacketSender().sendMessage("The cracker explodes and you receive a Party hat!");
                int phat = 1038 + Misc.getRandom(10);
                player.getInventory().add(phat, 1);
                target.getPacketSender().sendMessage("" + player.getAttributes().getUserName() + " has received a Party hat!");
                PlayerLogs.log(player.getAttributes().getUserName(), "Opened a cracker containing a " + ItemDefinition.forId(phat).getName() + " on " + target.getAttributes().getUserName());
			/*	if(Misc.getRandom(1) == 1) {
				target.getPacketSender().sendMessage("The cracker explodes and you receive a Party hat!");
				target.getInventory().add((1038 + Misc.getRandom(5)*2), 1);
				player.getPacketSender().sendMessage(""+target.getAttributes().getUserName()+" has received a Party hat!");
			} else {
				player.getPacketSender().sendMessage("The cracker explodes and you receive a Party hat!");
				player.getInventory().add((1038 + Misc.getRandom(5)*2), 1);
				target.getPacketSender().sendMessage(""+player.getAttributes().getUserName()+" has received a Party hat!");
			}*/
                break;
            case 4566:
                player.performAnimation(new Animation(451));
                break;
            case 4155:
                if (player.getSlayer().getDuoPartner() != null) {
                    player.getPacketSender().sendMessage(
                            "You already have a duo partner.");
                    return;
                }
                if (player.getSlayer().getSlayerTask() != SlayerTasks.NO_TASK) {
                    player.getPacketSender().sendMessage(
                            "You already have a Slayer task. You must reset it first.");
                    return;
                }
                Player duoPartner = World.getPlayers().get(targetIndex);
                if (duoPartner != null) {
                    if (duoPartner.getSlayer().getDuoPartner() != null) {
                        player.getPacketSender().sendMessage(
                                "This player already has a duo partner.");
                        return;
                    }
                    if (duoPartner.getSlayer().getSlayerTask() != SlayerTasks.NO_TASK) {
                        player.getPacketSender().sendMessage("This player already has a Slayer task.");
                        return;
                    }
                    if (duoPartner.getSlayer().getSlayerMaster() != player.getSlayer().getSlayerMaster()) {
                        player.getPacketSender().sendMessage("You do not have the same Slayer master as that player.");
                        return;
                    }
                    if (duoPartner.busy() || duoPartner.getLocation() == Location.WILDERNESS) {
                        player.getPacketSender().sendMessage(
                                "This player is currently busy.");
                        return;
                    }
                    player.getPacketSender().sendMessage(
                            "You have invited " + duoPartner.getAttributes().getUserName()
                                    + " to join your Slayer duo team.");
                }
                break;
        }
    }

    @Override
    public void handleMessage(Player player, Packet packet) {
        if (player.getConstitution() <= 0)
            return;
        switch (packet.getOpcode()) {
            case ITEM_ON_ITEM:
                itemOnItem(player, packet);
                break;
            case USE_ITEM:
                useItem(player, packet);
                break;
            case ITEM_ON_OBJECT:
                itemOnObject(player, packet);
                break;
            case ITEM_ON_GROUND_ITEM:
                itemOnGroundItem(player);
                break;
            case ITEM_ON_NPC:
                itemOnNpc(player, packet);
                break;
            case ITEM_ON_PLAYER:
                itemOnPlayer(player, packet);
                break;
        }
    }
}
