package com.runeroyale.net.packet.impl;

import com.runeroyale.net.packet.Packet;
import com.runeroyale.net.packet.PacketListener;
import com.runeroyale.util.Misc;
import com.runeroyale.world.content.PlayerPunishment;
import com.runeroyale.world.content.clan.ClanChatManager;
import com.runeroyale.world.entity.impl.player.Player;

public class SendClanChatMessagePacketListener implements PacketListener {

    @Override
    public void handleMessage(Player player, Packet packet) {
        String clanMessage = Misc.readString(packet.getBuffer());
        if (clanMessage == null || clanMessage.length() < 1)
            return;
        if (PlayerPunishment.muted(player.getAttributes().getUserName()) || PlayerPunishment.IPMuted(player.getAttributes().getHostAddress())) {
            player.getPacketSender().sendMessage("You are muted and cannot chat.");
            return;
        }
        ClanChatManager.sendMessage(player, clanMessage);
    }

}
