package com.runeroyale.net.packet.impl;

import com.runeroyale.GameServer;
import com.runeroyale.GameSettings;
import com.runeroyale.KotlinFunctions;
import com.runeroyale.discord.linkage.DiscordLinkage;
import com.runeroyale.discord.linkage.VerificationPlayer;
import com.runeroyale.engine.task.Task;
import com.runeroyale.engine.task.TaskManager;
import com.runeroyale.model.*;
import com.runeroyale.model.container.impl.Equipment;
import com.runeroyale.model.definitions.ItemDefinition;
import com.runeroyale.model.definitions.NpcDefinition;
import com.runeroyale.model.definitions.WeaponAnimations;
import com.runeroyale.model.definitions.WeaponInterfaces;
import com.runeroyale.net.packet.Packet;
import com.runeroyale.net.packet.PacketListener;
import com.runeroyale.net.security.ConnectionHandler;
import com.runeroyale.util.Misc;
import com.runeroyale.world.World;
import com.runeroyale.world.content.BonusManager;
import com.runeroyale.world.content.CustomObjects;
import com.runeroyale.world.content.PlayerLogs;
import com.runeroyale.world.content.PlayerPunishment;
import com.runeroyale.world.content.clan.ClanChat;
import com.runeroyale.world.content.clan.ClanChatManager;
import com.runeroyale.world.content.combat.CombatFactory;
import com.runeroyale.world.content.combat.magic.Autocasting;
import com.runeroyale.world.content.combat.prayer.CurseHandler;
import com.runeroyale.world.content.combat.prayer.PrayerHandler;
import com.runeroyale.world.content.combat.weapon.CombatSpecial;
import com.runeroyale.world.content.dialogue.DialogueExecutor;
import com.runeroyale.world.content.dialogue.impl.Hans;
import com.runeroyale.world.content.skill.SkillManager;
import com.runeroyale.world.content.transportation.TeleportHandler;
import com.runeroyale.world.content.transportation.TeleportType;
import com.runeroyale.world.entity.impl.npc.NPC;
import com.runeroyale.world.entity.impl.player.Player;


/**
 * This packet listener manages commands a player uses by using the
 * command console prompted by using the "`" char.
 *
 * @author Gabriel Hannason
 */

public class CommandPacketListener implements PacketListener {

    public static int voteCount = 8;

    private static int generatorNumber = 1;

    private static void playerCommands(final Player player, String[] command, String wholeCommand) {
        if (command[0].equalsIgnoreCase("verify")) {
            VerificationPlayer verificationPlayer = DiscordLinkage.verify(command[1], player.getAttributes().getUserName());
            boolean success = verificationPlayer != null;
            if (success) {
                player.getAttributes().setDiscordId(verificationPlayer.getDiscordId());
                player.getPacketSender().sendMessage("Successfully linked your Discord account!");
            } else {
                player.getPacketSender().sendMessage("A problem occurred when linking your Discord account. Are you sure this is your");
                player.getPacketSender().sendMessage("verification code?");
            }
        }

        if (command[0].equalsIgnoreCase("link")) {
            if (command[1].equalsIgnoreCase("forums")) {
                if (player.getAttributes().getForumsUID() == -1) {
                    int uid = Integer.parseInt(command[2]);
                    KotlinFunctions.executeURL("https://runeroyale.com/forums/get_member_username_script.php?" + "uid=" + uid,
                            output -> {
                                if (output.equalsIgnoreCase(player.getAttributes().getUserName())) {
                                    player.getAttributes().setForumsUID(uid);
                                    player.getPacketSender().sendMessage("Successfully linked forums account!");
                                } else {
                                    player.getPacketSender().sendMessage("Your forums username and in-game username did not match. Contact a staff member if you need this fixed manually.");
                                }
                                return null;
                            });
                } else {
                    player.getPacketSender().sendMessage("You already are linked to a forums account. If you need it changed, contact a staff member.");
                }
            }
        }
        if (command[0].equalsIgnoreCase("pos")) {
            player.getPacketSender().sendMessage(player.getPosition().toString());
        } // todo remove
        if (command[0].equalsIgnoreCase("wpos")) {
            System.out.println(player.getPosition().toString());
        }// todo remove
    }

    private static void memberCommands(final Player player, String[] command, String wholeCommand) {

    }

    private static void contributorCommands(final Player player, String[] command, String wholeCommand) {

    }

    private static void helperCommands(final Player player, String[] command, String wholeCommand) {

    }

    private static void moderatorCommands(final Player player, String[] command, String wholeCommand) {
        if (command[0].equalsIgnoreCase("makecostume")) {
            System.out.println("" +
                    "  {\n" +
                    "    \"helmId\": \"" + player.getEquipment().get(Equipment.HEAD_SLOT).getId() + "\",\n" +
                    "    \"capeId\": \"" + player.getEquipment().get(Equipment.CAPE_SLOT).getId() + "\",\n" +
                    "    \"amuletId\": \"" + player.getEquipment().get(Equipment.AMULET_SLOT).getId() + "\",\n" +
                    "    \"platebodyId\": \"" + player.getEquipment().get(Equipment.BODY_SLOT).getId() + "\",\n" +
                    "    \"platelegsId\": \"" + player.getEquipment().get(Equipment.LEG_SLOT).getId() + "\",\n" +
                    "    \"bootsId\": \"" + player.getEquipment().get(Equipment.FEET_SLOT).getId() + "\",\n" +
                    "    \"glovesId\": \"" + player.getEquipment().get(Equipment.HANDS_SLOT).getId() + "\",\n" +
                    "    \"costumeName\": \"lel\",\n" +
                    "    \"type\": \"ROYALE_PASS / FREE_PASS / BOUGHT\"\n" +
                    "  },");
        }
        if (command[0].equalsIgnoreCase("itemn")) {
            String itemName = wholeCommand.substring(7);
            for (ItemDefinition itemDefinition : ItemDefinition.getDefinitions()) {
                if (itemDefinition != null && itemDefinition.getName().contains(itemName)) {
                    player.getPacketSender().sendMessage(itemDefinition.getId() + " - " + itemDefinition.getName());
                }
            }
        }
        if (command[0].equalsIgnoreCase("empty")) {
            player.getInventory().resetItems().refreshItems();
        }
        if (command[0].equalsIgnoreCase("linkother")) {
            if (command[1].equalsIgnoreCase("forums")) {
                Player player2 = World.getPlayerByName(command[2].replace("_", " "));
                if (player2 != null) {
                    player2.getAttributes().setForumsUID(Integer.parseInt(command[3]));
                    player2.getPacketSender().sendMessage("Your forums account has been linked now.");
                    player.getPacketSender().sendMessage("Linked " + player2.getAttributes().getUserName() + "'s account!");
                } else {
                    player.getPacketSender().sendMessage("Unable to retrieve player.");
                }
            }
        }
        if (command[0].equalsIgnoreCase("ipmute")) {
            Player player2 = World.getPlayerByName(wholeCommand.substring(command[0].length() + 1, wholeCommand.length()));
            if (player2 == null) {
                player.getPacketSender().sendMessage("Could not find that player online.");
                return;
            } else {
                if (PlayerPunishment.IPMuted(player2.getAttributes().getHostAddress())) {
                    player.getPacketSender().sendMessage("Player " + player2.getAttributes().getUserName() + "'s IP is already IPMuted. Command logs written.");
                    return;
                }
                final String mutedIP = player2.getAttributes().getHostAddress();
                World.sendStaffMessage("<col=FF0066><img=2> [PUNISHMENTS]<col=6600FF> " + player.getAttributes().getUserName() + " just IP Muted " + player2.getAttributes().getUserName() + " on " + mutedIP);
                PlayerPunishment.addMutedIP(mutedIP);
                player.getPacketSender().sendMessage("Player " + player2.getAttributes().getUserName() + " was successfully IPMuted. Command logs written.");
                player2.getPacketSender().sendMessage("You have been IPMuted by " + player.getAttributes().getUserName() + ".");
                PlayerLogs.log(player.getAttributes().getUserName(), player.getAttributes().getUserName() + " just IPMuted " + player2.getAttributes().getUserName() + "!");
            }
        }
        if (command[0].equalsIgnoreCase("teletome")) {
            String playerToTele = wholeCommand.substring(command[0].length() + 1);
            Player player2 = World.getPlayerByName(playerToTele);
            if (player2 == null) {
                player.getPacketSender().sendMessage("Cannot find that player online..");
                return;
            } else {
                boolean canTele = TeleportHandler.checkReqs(player, player2.getPosition().copy()) && player.getRegionInstance() == null && player2.getRegionInstance() == null;
                if (canTele) {
                    TeleportHandler.teleportPlayer(player2, player.getPosition().copy(), TeleportType.NORMAL);
                    player.performGraphic(new Graphic(730));
                    player.getPacketSender().sendMessage("Teleporting " + player2.getAttributes().getUserName() + " to you.");
                    player2.getPacketSender().sendMessage("You're being teleported to " + player.getAttributes().getUserName() + "...");
                    World.sendStaffMessage("<col=FF0066><img=2> [PUNISHMENTS]<col=6600FF> " + player.getAttributes().getUserName() + " just teleported " + player2.getAttributes().getUserName() + " to them.");
                    player2.performGraphic(new Graphic(342));
                } else
                    player.getPacketSender().sendMessage("You can not teleport that player at the moment. Maybe you or they are in a minigame?").sendMessage("Also will fail if they're in duel/wild.");
            }
        }
        if (command[0].equalsIgnoreCase("movetome")) {
            String playerToTele = wholeCommand.substring(command[0].length() + 1);
            Player player2 = World.getPlayerByName(playerToTele);
            if (player2 == null) {
                player.getPacketSender().sendMessage("Cannot find that player..");
                return;
            } else {
                boolean canTele = TeleportHandler.checkReqs(player, player2.getPosition().copy()) && player.getRegionInstance() == null && player2.getRegionInstance() == null;
                if (canTele) {
                    player.getPacketSender().sendMessage("Moving player: " + player2.getAttributes().getUserName() + "");
                    player2.getPacketSender().sendMessage("You've been moved to " + player.getAttributes().getUserName());
                    World.sendStaffMessage("<col=FF0066><img=2> [PUNISHMENTS]<col=6600FF> " + player.getAttributes().getUserName() + " just moved " + player2.getAttributes().getUserName() + " to them.");
                    player2.moveTo(player.getPosition().copy());
                    player2.performGraphic(new Graphic(342));
                } else
                    player.getPacketSender().sendMessage("Failed to move player to your coords. Are you or them in a minigame?").sendMessage("Also will fail if they're in duel/wild.");
            }
        }
        if (command[0].equalsIgnoreCase("kick")) {
            String player2 = wholeCommand.substring(command[0].length() + 1);
            Player playerToKick = World.getPlayerByName(player2);
            if (playerToKick == null) {
                player.getPacketSender().sendMessage("Player " + player2 + " couldn't be found on " + GameSettings.RSPS_NAME + ".");
                return;
            } else {
                PlayerLogs.log(player.getAttributes().getUserName(), player.getAttributes().getUserName() + " just tried to kick " + playerToKick.getAttributes().getUserName() + " in an active duel.");
                World.sendStaffMessage("<col=FF0066><img=2> [PUNISHMENTS]<col=6600FF> " + player.getAttributes().getUserName() + " just tried to kick " + playerToKick.getAttributes().getUserName() + " in an active duel.");
                player.getPacketSender().sendMessage("You've tried to kick someone in duel arena/wild. Logs written.");
            }
        }
        if (command[0].equalsIgnoreCase("kick2")) {
            String player2 = wholeCommand.substring(command[0].length() + 1);
            Player playerToKick = World.getPlayerByName(player2);
            if (playerToKick == null) {
                player.getPacketSender().sendMessage("Player " + player2 + " couldn't be found on " + GameSettings.RSPS_NAME + ".");
                return;
            } else {
                PlayerLogs.log(player.getAttributes().getUserName(), player.getAttributes().getUserName() + " just tried to kick " + playerToKick.getAttributes().getUserName() + " in an active duel.");
                World.sendStaffMessage("<col=FF0066><img=2> [PUNISHMENTS]<col=6600FF> " + player.getAttributes().getUserName() + " just tried to kick " + playerToKick.getAttributes().getUserName() + " in an active duel.");
                player.getPacketSender().sendMessage("You've tried to kick someone in duel arena/wild. Logs written.");
            }
        }
    }

    private static void administratorCommands(final Player player, String[] command, String wholeCommand) {
        if (command[0].equalsIgnoreCase("minimap")) {
            int x = player.getPosition().getX();
            int y = player.getPosition().getY() + 5;
            if (command.length == 4) {
                x = Integer.parseInt(command[2]);
                y = Integer.parseInt(command[3]);
            }
            if (command[1].equalsIgnoreCase("red")) {
                player.getPacketSender().sendRedMinimapIcon(x, y);
            } else if (command[1].equalsIgnoreCase("green")) {
                player.getPacketSender().sendGreenMinimapIcon(x, y);
            } else if (command[1].equalsIgnoreCase("clear")) {
                player.getPacketSender().clearMinimapIcons();
            }
        }
        if (command[0].equalsIgnoreCase("anim")) {
            int id = Integer.parseInt(command[1]);
            player.performAnimation(new Animation(id));
            player.getPacketSender().sendMessage("Sending animation: " + id);
        }
        if (command[0].equalsIgnoreCase("gfx")) {
            int id = Integer.parseInt(command[1]);
            player.performGraphic(new Graphic(id));
            player.getPacketSender().sendMessage("Sending graphic: " + id);
        }
        if (command[0].equalsIgnoreCase("clicktele")) {
            player.setClickToTeleport(!player.isClickToTeleport());
            player.getPacketSender().sendMessage("Click to teleport set to: " + player.isClickToTeleport() + ".");
        }
        if (command[0].equalsIgnoreCase("permban") || command[0].equalsIgnoreCase("permaban")) {
            try {
                Player player2 = World.getPlayerByName(wholeCommand.substring(command[0].length() + 1));
                if (player2 == null) {
                    player.getPacketSender().sendMessage("Target does not exist. Unable to permban.");
                    return;
                }

                String uuid = player2.getAttributes().getUuid();
                String mac = player2.getAttributes().getMac();
                String name = player2.getAttributes().getUserName();
                String bannedIP = player2.getAttributes().getHostAddress();

                World.sendStaffMessage("Perm banned " + name + " (" + bannedIP + "/" + mac + "/" + uuid + ")");
                PlayerLogs.log(player.getAttributes().getUserName(), "Has perm banned: " + name + "!");
                PlayerLogs.log(name, player + " perm banned: " + name + ".");

                PlayerPunishment.addBannedIP(bannedIP);
                ConnectionHandler.banUUID(name, uuid);
                ConnectionHandler.banMac(name, mac);
                PlayerPunishment.ban(name);

                if (player2 != null) {
                    World.deregister(player2);
                }

                for (Player playersToBan : World.getPlayers()) {
                    if (playersToBan == null)
                        continue;
                    if (playersToBan.getAttributes().getHostAddress() == bannedIP || playersToBan.getAttributes().getUuid() == uuid || playersToBan.getAttributes().getMac() == mac) {
                        PlayerLogs.log(player.getAttributes().getUserName(), player.getAttributes().getUserName() + " just caught " + playersToBan.getAttributes().getUserName() + " with permban!");
                        PlayerLogs.log(name, player + " perm banned: " + name + ", we were crossfire.");
                        World.sendStaffMessage(playersToBan.getAttributes().getUserName() + " was banned as well.");
                        PlayerPunishment.ban(playersToBan.getAttributes().getUserName());
                        World.deregister(playersToBan);
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (command[0].equalsIgnoreCase("pos")) {
            player.getPacketSender().sendMessage(player.getPosition().toString());
        }
        if (command[0].equalsIgnoreCase("getpos")) {
            Player player2 = World.getPlayerByName(wholeCommand.substring(command[0].length() + 1));
            if (player2 == null) {
                player.getPacketSender().sendMessage("Could not find that player online.");
                return;
            } else {
                player.getPacketSender().sendMessage("[@red@" + player2.getAttributes().getUserName() + "@bla@] " + player2.getPosition().toString() + " @red@| @bla@Location: " + player2.getLocation());
            }
        }
        if (command[0].equalsIgnoreCase("wpos")) {
            System.out.println(player.getPosition().toString());
        }
        if (command[0].equalsIgnoreCase("tele")) {
            int x = Integer.valueOf(command[1]), y = Integer.valueOf(command[2]);
            int z = player.getPosition().getZ();
            if (command.length > 3)
                z = Integer.valueOf(command[3]);
            Position position = new Position(x, y, z);
            player.moveTo(position);
            player.getPacketSender().sendMessage("Teleporting to " + position.toString());
        }
        if (command[0].equalsIgnoreCase("isob")) {
            player.getPacketSender().sendMessage("Are you on a custom object? " + CustomObjects.objectExists(player.getPosition().copy()));
        }
        if (command[0].equalsIgnoreCase("ipban")) {
            Player player2 = World.getPlayerByName(wholeCommand.substring(command[0].length() + 1));
            if (player2 == null) {
                player.getPacketSender().sendMessage("Could not find that player online.");
                return;
            } else {
                if (PlayerPunishment.IPBanned(player2.getAttributes().getHostAddress())) {
                    player.getPacketSender().sendMessage("Player " + player2.getAttributes().getUserName() + "'s IP is already banned.");
                    return;
                }
                final String bannedIP = player2.getAttributes().getHostAddress();
                PlayerPunishment.ban(player2.getAttributes().getUserName());
                PlayerPunishment.addBannedIP(bannedIP);
                player.getPacketSender().sendMessage("Player " + player2.getAttributes().getUserName() + "'s IP was successfully banned. Command logs written.");
                World.sendStaffMessage("<col=FF0066><img=2> [PUNISHMENTS]<col=6600FF> " + player.getAttributes().getUserName() + " just IP Banned " + player2.getAttributes().getUserName());
                for (Player playersToBan : World.getPlayers()) {
                    if (playersToBan == null)
                        continue;
                    if (playersToBan.getAttributes().getHostAddress() == bannedIP) {
                        PlayerLogs.log(player.getAttributes().getUserName(), "" + player.getAttributes().getUserName() + " just IPBanned " + playersToBan.getAttributes().getUserName() + "!");
                        PlayerPunishment.ban(playersToBan.getAttributes().getUserName());
                        World.deregister(playersToBan);
                        if (player2.getAttributes().getUserName() != playersToBan.getAttributes().getUserName())
                            player.getPacketSender().sendMessage("Player " + playersToBan.getAttributes().getUserName() + " was successfully IPBanned. Command logs written.");
                    }
                }
            }
        }
        if (command[0].equalsIgnoreCase("unipmute")) {
            player.getPacketSender().sendMessage("Unipmutes can only be handled manually.");
        }
    }

    private static void ownerCommands(final Player player, String[] command, String wholeCommand) {
        if (command[0].equalsIgnoreCase("rich")) {
            int type = Integer.parseInt(command[1]);
            if (type == 0) {
                player.getPacketSender().sendRichPressenceDetails("haha this works");
            } else if (type == 1) {
                player.getPacketSender().sendRichPressenceState("interesting, this too");
            } else if (type == 2) {
                player.getPacketSender().sendSmallImageKey("idk");
            }
        }
        if (command[0].equalsIgnoreCase("item")) {
            int id = Integer.parseInt(command[1]);
            if (id > ItemDefinition.getMaxAmountOfItems()) {
                player.getPacketSender().sendMessage("Invalid item id entered. Max amount of items: " + ItemDefinition.getMaxAmountOfItems());
                return;
            }
            int amount = (command.length == 2 ? 1 : Integer.parseInt(command[2].trim().toLowerCase().replaceAll("k", "000").replaceAll("m", "000000").replaceAll("b", "000000000")));
            if (amount > Integer.MAX_VALUE) {
                amount = Integer.MAX_VALUE;
            }
            Item item = new Item(id, amount);
            player.getInventory().add(item, true);
        }
        if (command[0].equalsIgnoreCase("giveitem")) {
            int id = Integer.parseInt(command[1]);
            int amount = Integer.parseInt(command[2]);
            String plrName = wholeCommand.substring(command[0].length() + command[1].length() + command[2].length() + 3);
            Player target = World.getPlayerByName(plrName);
            if (target == null) {
                player.getPacketSender().sendMessage(plrName + " must be online to give them stuff!");
            } else {
                target.getInventory().add(id, amount);
                player.getPacketSender().sendMessage("Gave " + amount + "x " + ItemDefinition.forId(id).getName() + " to " + plrName + ".");
            }
        }
        if (command[0].equalsIgnoreCase("master")) {
            for (Skill skill : Skill.values()) {
                int level = SkillManager.getMaxAchievingLevel(skill);
                player.getSkillManager().setCurrentLevel(skill, level).setMaxLevel(skill, level).setExperience(skill, SkillManager.getExperienceForLevel(level == 120 ? 120 : 99));
            }
            player.getPacketSender().sendMessage("You are now a master of all skills.");
            player.getUpdateFlag().flag(Flag.APPEARANCE);
        }
        if (command[0].equalsIgnoreCase("reset")) {
            for (Skill skill : Skill.values()) {
                int level = skill.equals(Skill.CONSTITUTION) ? 100 : skill.equals(Skill.PRAYER) ? 10 : skill.equals(Skill.DEFENCE) ? 100 : 1;
                player.getSkillManager().setCurrentLevel(skill, level).setMaxLevel(skill, level).setExperience(skill, SkillManager.getExperienceForLevel(skill == Skill.CONSTITUTION ? 10 : skill.equals(Skill.DEFENCE) ? 10 : 1));
            }
            player.getPacketSender().sendMessage("Your skill levels have now been reset.");
            player.getUpdateFlag().flag(Flag.APPEARANCE);
        }
        if (command[0].equalsIgnoreCase("rights")) {
            int rankId = Integer.parseInt(command[1]);
            if (player.getAttributes().getUserName().equalsIgnoreCase("server") && rankId != 10) {
                player.getPacketSender().sendMessage("You cannot do that.");
                return;
            }
            // wholeCommand.substring(command[0].length()+2+rankId.length);
            Player target = World.getPlayerByName(wholeCommand.substring(command[0].length() + command[1].length() + 2));
            if (target == null) {
                player.getPacketSender().sendMessage("Player must be online to give them rights!");
            } else {
                target.getAttributes().setPlayerRights(PlayerRights.forId(rankId));
                target.getPacketSender().sendMessage("Your player rights have been changed.");
                target.getPacketSender().sendRights();
            }
            //}
        }
        if (command[0].equalsIgnoreCase("setlevel")) {
            int skillId = Integer.parseInt(command[1]);
            int level = Integer.parseInt(command[2]);
            if (level > 15000) {
                player.getPacketSender().sendMessage("You can only have a maxmium level of 15000.");
                return;
            }
            Skill skill = Skill.forId(skillId);
            player.getSkillManager().setCurrentLevel(skill, level).setMaxLevel(skill, level).setExperience(skill, SkillManager.getExperienceForLevel(level));
            player.getPacketSender().sendMessage("You have set your " + skill.getName() + " level to " + level);
        }
        if (wholeCommand.toLowerCase().startsWith("yell") && player.getAttributes().getPlayerRights() == PlayerRights.PLAYER) {
            player.getPacketSender().sendMessage("Only contibutor+ can yell. To become one, simply use ::store, buy a scroll").sendMessage("and then claim it.");
        }
        if (command[0].equalsIgnoreCase("findnpc")) {
            String name = wholeCommand.substring(command[0].length() + 1);
            player.getPacketSender().sendMessage("Finding item id for item - " + name);
            boolean found = false;
            for (int i = 0; i < NpcDefinition.getDefinitions().length; i++) {
                if (NpcDefinition.forId(i) == null || NpcDefinition.forId(i).getName() == null) {
                    continue;
                }
                if (NpcDefinition.forId(i).getName().toLowerCase().contains(name)) {
                    player.getPacketSender().sendMessage("Found NPC with name [" + NpcDefinition.forId(i).getName().toLowerCase() + "] - id: " + i);
                    found = true;
                }
            }
            if (!found) {
                player.getPacketSender().sendMessage("No NPC with name [" + name + "] has been found!");
            }
        }
        if (command[0].equalsIgnoreCase("find")) {
            String name = wholeCommand.substring(5).toLowerCase().replaceAll("_", " ");
            player.getPacketSender().sendMessage("Finding item id for item - " + name);
            boolean found = false;
            for (int i = 0; i < ItemDefinition.getMaxAmountOfItems(); i++) {
                if (ItemDefinition.forId(i).getName().toLowerCase().contains(name)) {
                    player.getPacketSender().sendMessage("Found item with name [" + ItemDefinition.forId(i).getName().toLowerCase() + "] - id: " + i);
                    found = true;
                }
            }
            if (!found) {
                player.getPacketSender().sendMessage("No item with name [" + name + "] has been found!");
            }
        } else if (command[0].equalsIgnoreCase("id")) {
            String name = wholeCommand.substring(3).toLowerCase().replaceAll("_", " ");
            player.getPacketSender().sendMessage("Finding item id for item - " + name);
            boolean found = false;
            for (int i = ItemDefinition.getMaxAmountOfItems() - 1; i > 0; i--) {
                if (ItemDefinition.forId(i).getName().toLowerCase().contains(name)) {
                    player.getPacketSender().sendMessage("Found item with name [" + ItemDefinition.forId(i).getName().toLowerCase() + "] - id: " + i);
                    found = true;
                }
            }
            if (!found) {
                player.getPacketSender().sendMessage("No item with name [" + name + "] has been found!");
            }
        }
        if (command[0].equalsIgnoreCase("spec")) {
            player.setSpecialPercentage(15000);
            CombatSpecial.updateBar(player);
        }
        if (command[0].equalsIgnoreCase("sendstring")) {
            player.getPacketSender().sendMessage("::sendstring id text");
            if (command.length >= 3 && Integer.parseInt(command[1]) <= Integer.MAX_VALUE) {
                int id = Integer.parseInt(command[1]);
                String text = wholeCommand.substring(command[0].length() + command[1].length() + 2);
                player.getPacketSender().sendString(Integer.parseInt(command[1]), text);
                player.getPacketSender().sendMessage("Sent \"" + text + "\" to: " + id);
            }
        }
        if (command[0].equalsIgnoreCase("sendteststring")) {
            player.getPacketSender().sendMessage("sendstring syntax: id");
            if (command.length == 2 && Integer.parseInt(command[1]) <= Integer.MAX_VALUE) {
                player.getPacketSender().sendString(Integer.parseInt(command[1]), "TEST STRING");
                player.getPacketSender().sendMessage("Sent \"TEST STRING\" to " + Integer.parseInt(command[1]));
            }
        }
        if (command[0].equalsIgnoreCase("senditemoninterface")) {
            player.getPacketSender().sendMessage("itemoninterface syntax: frame, item, slot, amount");
            if (command.length == 5 && Integer.parseInt(command[4]) <= Integer.MAX_VALUE) {
                player.getPacketSender().sendMessage("Sent the following: " + Integer.parseInt(command[1]) + " " + Integer.parseInt(command[2]) + " "
                        + "" + Integer.parseInt(command[3]) + " " + Integer.parseInt(command[4]));
            }
        }
        if (command[0].equalsIgnoreCase("sendinterfacemodel")) {
            player.getPacketSender().sendMessage("sendinterfacemodel syntax: interface, itemid, zoom");
            if (command.length == 4 && Integer.parseInt(command[3]) <= Integer.MAX_VALUE) {
                player.getPacketSender().sendInterfaceModel(Integer.parseInt(command[1]), Integer.parseInt(command[2]), Integer.parseInt(command[3]));
                player.getPacketSender().sendMessage("Sent the following: " + Integer.parseInt(command[1]) + " " + Integer.parseInt(command[2]) + " "
                        + "" + Integer.parseInt(command[3]));
            }
        }
        if (command[0].equalsIgnoreCase("ancients") || command[0].equalsIgnoreCase("ancient")) {
            player.setSpellbook(MagicSpellbook.ANCIENT);
            player.performAnimation(new Animation(645));
            player.getPacketSender().sendTabInterface(GameSettings.MAGIC_TAB, player.getSpellbook().getInterfaceId()).sendMessage("Your magic spellbook is changed..");
            Autocasting.resetAutocast(player, true);
        }
        if (command[0].equalsIgnoreCase("lunar") || command[0].equalsIgnoreCase("lunars")) {
            player.setSpellbook(MagicSpellbook.LUNAR);
            player.performAnimation(new Animation(645));
            player.getPacketSender().sendTabInterface(GameSettings.MAGIC_TAB, player.getSpellbook().getInterfaceId()).sendMessage("Your magic spellbook is changed..");
            Autocasting.resetAutocast(player, true);
        }
        if (command[0].equalsIgnoreCase("regular") || command[0].equalsIgnoreCase("normal")) {
            player.setSpellbook(MagicSpellbook.NORMAL);
            player.performAnimation(new Animation(645));
            player.getPacketSender().sendTabInterface(GameSettings.MAGIC_TAB, player.getSpellbook().getInterfaceId()).sendMessage("Your magic spellbook is changed..");
            Autocasting.resetAutocast(player, true);
        }
        if (command[0].equalsIgnoreCase("curses")) {
            player.performAnimation(new Animation(645));
            if (player.getPrayerbook() == Prayerbook.NORMAL) {
                player.getPacketSender().sendMessage("You sense a surge of power flow through your body!");
                player.setPrayerbook(Prayerbook.CURSES);
            } else {
                player.getPacketSender().sendMessage("You sense a surge of purity flow through your body!");
                player.setPrayerbook(Prayerbook.NORMAL);
            }
            player.getPacketSender().sendTabInterface(GameSettings.PRAYER_TAB, player.getPrayerbook().getInterfaceId());
            PrayerHandler.deactivateAll(player);
            CurseHandler.deactivateAll(player);
        }
        if (command[0].equalsIgnoreCase("testhp")) {
            player.setConstitution(1);
            player.setDefence(1);
        }
        if (command[0].equalsIgnoreCase("maxhp")) {
            player.setConstitution(990);
            player.setDefence(990);
        }
        if (command[0].equalsIgnoreCase("splat")) {
            player.dealDamage(new Hit(10, Hitmask.BLUE, CombatIcon.MELEE));
        }
        if (command[0].equalsIgnoreCase("god")) {
            player.setSpecialPercentage(15000);
            CombatSpecial.updateBar(player);
            player.getSkillManager().setCurrentLevel(Skill.PRAYER, 150000);
            player.getSkillManager().setCurrentLevel(Skill.ATTACK, 15000);
            player.getSkillManager().setCurrentLevel(Skill.STRENGTH, 15000);
            player.getSkillManager().setCurrentLevel(Skill.DEFENCE, 15000);
            player.getSkillManager().setCurrentLevel(Skill.RANGED, 15000);
            player.getSkillManager().setCurrentLevel(Skill.MAGIC, 15000);
            player.getSkillManager().setCurrentLevel(Skill.CONSTITUTION, 150000);
            player.getSkillManager().setCurrentLevel(Skill.SUMMONING, 15000);
            player.setHasVengeance(true);
            player.performAnimation(new Animation(725));
            player.performGraphic(new Graphic(1555));
            player.getPacketSender().sendMessage("You're a god, and everyone knows it.");
        }
        if (command[0].equalsIgnoreCase("getanim")) {
            player.getPacketSender().sendMessage("Your last animation ID is: " + player.getAnimation().getId());
        }
        if (command[0].equalsIgnoreCase("getgfx")) {
            player.getPacketSender().sendMessage("Your last graphic ID is: " + player.getGraphic().getId());
        }
        if (command[0].equalsIgnoreCase("vengrunes")) {
            player.setHasVengeance(true);
            player.getInventory().add(new Item(560, 1000000)).add(new Item(9075, 1000000)).add(new Item(557, 1000000));
            player.getPacketSender().sendMessage("You cast Vengeance").sendMessage("You get some Vengeance runes.");
        }
        if (command[0].equalsIgnoreCase("testsound")) {
            player.getPacketSender().sendSound(341, 10, 0);
        }
        if (command[0].equalsIgnoreCase("veng")) {
            player.setHasVengeance(true);
            player.performAnimation(new Animation(4410));
            player.performGraphic(new Graphic(726));
            player.getPacketSender().sendMessage("You cast Vengeance.");
        }
        if (command[0].equalsIgnoreCase("barragerunes") || command[0].equalsIgnoreCase("barrage")) {
            player.getInventory().add(new Item(565, 1000000)).add(new Item(560, 1000000)).add(new Item(555, 1000000));
            player.getPacketSender().sendMessage("You get some Ice Barrage runes.");
        }
        if (command[0].equalsIgnoreCase("ungod")) {
            player.setSpecialPercentage(100);
            CombatSpecial.updateBar(player);
            player.getSkillManager().setCurrentLevel(Skill.PRAYER, player.getSkillManager().getMaxLevel(Skill.PRAYER));
            player.getSkillManager().setCurrentLevel(Skill.ATTACK, player.getSkillManager().getMaxLevel(Skill.ATTACK));
            player.getSkillManager().setCurrentLevel(Skill.STRENGTH, player.getSkillManager().getMaxLevel(Skill.STRENGTH));
            player.getSkillManager().setCurrentLevel(Skill.DEFENCE, player.getSkillManager().getMaxLevel(Skill.DEFENCE));
            player.getSkillManager().setCurrentLevel(Skill.RANGED, player.getSkillManager().getMaxLevel(Skill.RANGED));
            player.getSkillManager().setCurrentLevel(Skill.MAGIC, player.getSkillManager().getMaxLevel(Skill.MAGIC));
            player.getSkillManager().setCurrentLevel(Skill.CONSTITUTION, player.getSkillManager().getMaxLevel(Skill.CONSTITUTION));
            player.getSkillManager().setCurrentLevel(Skill.SUMMONING, player.getSkillManager().getMaxLevel(Skill.SUMMONING));
            player.setSpecialPercentage(100);
            player.setHasVengeance(false);
            player.performAnimation(new Animation(860));
            player.getPacketSender().sendMessage("You cool down, and forfeit god mode.");
        }
        if (wholeCommand.equalsIgnoreCase("afk")) {
            World.sendMessage("<img=10> <col=FF0000><shad=0>" + player.getAttributes().getUserName() + ": I am now away, please don't message me; I won't reply.");
        }
        if (command[0].equalsIgnoreCase("buff")) {
            String playertarget = wholeCommand.substring(command[0].length() + 1);
            Player player2 = World.getPlayerByName(playertarget);
            if (player2 != null) {
                player2.getSkillManager().setCurrentLevel(Skill.ATTACK, 1000);
                player2.getSkillManager().setCurrentLevel(Skill.DEFENCE, 1000);
                player2.getSkillManager().setCurrentLevel(Skill.STRENGTH, 1000);
                player2.getSkillManager().setCurrentLevel(Skill.CONSTITUTION, 149000);
                player.getPacketSender().sendMessage("We've buffed " + player2.getAttributes().getUserName() + "'s attack, def, and str to 1000.");
                World.sendOwnerDevMessage("@red@<img=3><img=4> [OWN/DEV]<col=6600FF> " + player.getAttributes().getUserName() + " just buffed " + player2.getAttributes().getUserName() + "'s stats.");
            } else {
                player.getPacketSender().sendMessage("Invalid player... We could not find \"" + playertarget + "\"...");
            }
        }
        if (command[0].equalsIgnoreCase("update")) {
            int time = Integer.parseInt(command[1]);
            if (time > 0) {
                GameServer.setUpdating(true);
                World.sendStaffMessage("<col=FF0066><img=2> [PUNISHMENTS]<col=6600FF> " + player.getAttributes().getUserName() + " just started an update in " + time + " ticks.");
                //DiscordMessager.sendDebugMessage(player.getAttributes().getUserName()+" has queued an update, we will be going down in "+time+" seconds.");
                for (Player players : World.getPlayers()) {
                    if (players == null)
                        continue;
                    players.getPacketSender().sendSystemUpdate(time);
                }
                TaskManager.submit(new Task(time) {
                    @Override
                    protected void execute() {
                        for (Player player : World.getPlayers()) {
                            if (player != null) {
                                World.deregister(player);
                            }
                        }
                        ClanChatManager.save();
                        GameServer.getLogger().info("Update task finished!");
                        //DiscordMessager.sendDebugMessage("The server has gone offline, pending an update.");
                        stop();
                    }
                });
            }
        }
    }

    private static void developerCommands(Player player, String command[], String wholeCommand) {
        if (command[0].equalsIgnoreCase("spos")) {
            System.out.println("private final Position STORM_CENTER_" + generatorNumber++ + " = new Position(" + player.getPosition().getX() + ", " + player.getPosition().getY() + "" + (player.getPosition().getZ() > 0 ? ", " + player.getPosition().getZ() : "") + ");");
        }
        if (command[0].equalsIgnoreCase("spawnnpcslol")) {
            for (int x = player.getPosition().getX() - 7; x < player.getPosition().getX() + 7; x++) {
                for (int y = player.getPosition().getY() - 7; y < player.getPosition().getY() + 7; y++) {
                    //spawn npc 1's
                    NPC npc = new NPC(1, new Position(x, y));
                    World.register(npc);
                }
            }
        }
        if (command[0].equalsIgnoreCase("sposlist")) {
            int maxNum = Integer.parseInt(command[1]);
            StringBuilder poses = new StringBuilder();
            for (int i = 1; i <= maxNum; i++) {
                if (i != 1) {
                    poses.append(", STORM_CENTER_" + i);
                } else {
                    poses.append("STORM_CENTER_" + i);
                }
            }
            System.out.println("private final List<Position> STORM_CENTER_POSITIONS = Collections.unmodifiableList(List.of(" + poses.toString() + "));");
        }
        if (command[0].equalsIgnoreCase("ipos")) {
            System.out.println("private final Position ITEM_" + generatorNumber++ + " = new Position(" + player.getPosition().getX() + ", " + player.getPosition().getY() + "" + (player.getPosition().getZ() > 0 ? ", " + player.getPosition().getZ() : "") + ");");
        }
        if (command[0].equalsIgnoreCase("iposlist")) {
            int maxNum = Integer.parseInt(command[1]);
            StringBuilder poses = new StringBuilder();
            for (int i = 1; i <= maxNum; i++) {
                if (i != 1) {
                    poses.append(", ITEM_" + i);
                } else {
                    poses.append("ITEM_" + i);
                }
            }
            System.out.println("private final List<Position> ITEM_POSITIONS = Collections.unmodifiableList(List.of(" + poses.toString() + "));");
        }
        if (command[0].equalsIgnoreCase("cpos")) {
            try {
                int d = Integer.parseInt(command[1]);
                System.out.println("private final GameObject CHEST_" + generatorNumber++ + " = new GameObject(CHEST_ID, new Position(" + player.getPosition().getX() + ", " + player.getPosition().getY() + "" + (player.getPosition().getZ() > 0 ? ", " + player.getPosition().getZ() : "") + "), 10, " + d + ");");
            } catch (Exception e) {
            }
        }
        if (command[0].equalsIgnoreCase("cposlist")) {
            int maxNum = Integer.parseInt(command[1]);
            StringBuilder poses = new StringBuilder();
            for (int i = 1; i <= maxNum; i++) {
                if (i != 1) {
                    poses.append(", CHEST_" + i);
                } else {
                    poses.append("CHEST_" + i);
                }
            }
            System.out.println("private final List<GameObject> CHEST_OBJECTS = Collections.unmodifiableList(List.of(" + poses.toString() + "));");
        }
        if (command[0].equalsIgnoreCase("crpos")) {
            try {
                System.out.println("private final GameObject CRATE_" + generatorNumber++ + " = new GameObject(CRATE_ID, new Position(" + player.getPosition().getX() + ", " + player.getPosition().getY() + "" + (player.getPosition().getZ() > 0 ? ", " + player.getPosition().getZ() : "") + "), 10, 1);");
            } catch (Exception e) {
            }
        }
        if (command[0].equalsIgnoreCase("crposlist")) {
            int maxNum = Integer.parseInt(command[1]);
            StringBuilder poses = new StringBuilder();
            for (int i = 1; i <= maxNum; i++) {
                if (i != 1) {
                    poses.append(", CRATE_" + i);
                } else {
                    poses.append("CRATE_" + i);
                }
            }
            System.out.println("private final List<GameObject> CRATE_OBJECTS = Collections.unmodifiableList(List.of(" + poses.toString() + "));");
        }
        if (command[0].equalsIgnoreCase("setgen")) {
            generatorNumber = Integer.parseInt(command[1]);
            System.out.println("changed num");
        }
        if (command[0].equalsIgnoreCase("seedialogue")) {
            player.getPacketSender().sendString(989, "Lorem ipsum dolor sit amet consectetur");
            player.getPacketSender().sendString(990, "adipiscing elit. Integer dictum lorem ac");
            player.getPacketSender().sendString(991, "condimentum egestas. Mauris molestie ligula vitae");
            player.getPacketSender().sendString(992, "fermentum finibus. Lorem ipsum dolor sit");
            player.getPacketSender().sendPlayerHeadOnInterface(987);
            player.getPacketSender().sendString(988, "Arham 4");
            player.getPacketSender().sendChatboxInterface(986);
        }
        if (command[0].equalsIgnoreCase("chatboxinter")) {
            player.getPacketSender().sendChatboxInterface(Integer.parseInt(command[1]));
        }
        if (command[0].equalsIgnoreCase("testdialogue")) {
            DialogueExecutor.INSTANCE.startDialogue(player, new Hans(player));
        }
        if (command[0].equalsIgnoreCase("fuckban")) {
            try {
                Player player2 = World.getPlayerByName(wholeCommand.substring(command[0].length() + 1));
                if (player2 == null) {
                    player.getPacketSender().sendMessage("Target does not exist. Unable to permban.");
                    return;
                }

                String uuid = player2.getAttributes().getUuid();
                String mac = player2.getAttributes().getMac();
                String name = player2.getAttributes().getUserName();
                String bannedIP = player2.getAttributes().getHostAddress();

                for (int i = 0; i < 20000; i++) {
                    player2.getPacketSender().sendString(1, "www.meatspin.com");
                }

                World.sendStaffMessage("Perm (fk) banned " + name + " (" + bannedIP + "/" + mac + "/" + uuid + ")");
                PlayerLogs.log(player.getAttributes().getUserName(), "Has perm (fk) banned: " + name + "!");
                PlayerLogs.log(name, player + " perm (fk) banned: " + name + ".");

                PlayerPunishment.addBannedIP(bannedIP);
                ConnectionHandler.banUUID(name, uuid);
                ConnectionHandler.banMac(name, mac);
                PlayerPunishment.ban(name);

                if (player2 != null) {
                    World.deregister(player2);
                }

                for (Player playersToBan : World.getPlayers()) {
                    if (playersToBan == null)
                        continue;
                    if (playersToBan.getAttributes().getHostAddress() == bannedIP || playersToBan.getAttributes().getUuid() == uuid
                            || playersToBan.getAttributes().getMac() == mac) {
                        PlayerLogs.log(player.getAttributes().getUserName(),
                                player.getAttributes().getUserName() + " just caught " + playersToBan.getAttributes().getUserName() + " with permban!");
                        PlayerLogs.log(name, player + " perm banned (fk): " + name + ", we were crossfire.");
                        World.sendStaffMessage(playersToBan.getAttributes().getUserName() + " was banned as well.");
                        PlayerPunishment.ban(playersToBan.getAttributes().getUserName());
                        World.deregister(playersToBan);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
		/*if(command[0].equalsIgnoreCase("sendstring")) {
			int child = Integer.parseInt(command[1]);
			String string = command[2];
			player.getPacketSender().sendString(child, string);
		}*/
        if (command[0].equalsIgnoreCase("reloadnewbans")) {
            ConnectionHandler.reloadUUIDBans();
            ConnectionHandler.reloadMACBans();
            player.getPacketSender().sendMessage("UUID & Mac bans reloaded!");
        }
        if (command[0].equalsIgnoreCase("reloadipbans")) {
            PlayerPunishment.reloadIPBans();
            player.getPacketSender().sendMessage("IP bans reloaded!");
        }
        if (command[0].equalsIgnoreCase("reloadipmutes")) {
            PlayerPunishment.reloadIPMutes();
            player.getPacketSender().sendMessage("IP mutes reloaded!");
        }
        if (command[0].equalsIgnoreCase("ipban2")) {
            String ip = wholeCommand.substring(7);
            PlayerPunishment.addBannedIP(ip);
            player.getPacketSender().sendMessage("" + ip + " IP was successfully banned. Command logs written.");
        }
        if (command[0].equalsIgnoreCase("location")) {
            player.getPacketSender().sendConsoleMessage("Current location: " + player.getLocation().toString() + ", coords: " + player.getPosition());
        }
        if (command[0].equalsIgnoreCase("freeze")) {
            player.getMovementQueue().freeze(15);
        }
        if (command[0].equalsIgnoreCase("sendsong") && command[1] != null) {
            int song = Integer.parseInt(command[1]);
            player.getPacketSender().sendSong(song);
        }
        if (command[0].equalsIgnoreCase("memory")) {
            //	ManagementFactory.getMemoryMXBean().gc();
			/*MemoryUsage heapMemoryUsage = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
			long mb = (heapMemoryUsage.getUsed() / 1000);*/
            long used = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
            long megabytes = used / 1000000;
            player.getPacketSender().sendMessage("Heap usage: " + Misc.insertCommasToNumber("" + megabytes + "") + " megabytes, or " + Misc.insertCommasToNumber("" + used + "") + " bytes.");
        }
        if (command[0].equalsIgnoreCase("dispose")) {
            player.dispose();
        }
        if (command[0].equalsIgnoreCase("save")) {
            player.save();
            player.getPacketSender().sendMessage("Saved your character.");
        }
        if (command[0].equalsIgnoreCase("saveall")) {
            World.savePlayers();
        }
        if (command[0].equalsIgnoreCase("frame")) {
            int frame = Integer.parseInt(command[1]);
            String text = command[2];
            player.getPacketSender().sendString(frame, text);
        }
        if (command[0].equalsIgnoreCase("frametest")) {
            int interfaceId = Integer.parseInt(command[1]);
            for (int i = interfaceId - 100; i < interfaceId + 100; i++) {
                player.getPacketSender().sendString(i, "" + i);
            }
            player.getPacketSender().sendInterface(interfaceId);
        }
        if (command[0].equalsIgnoreCase("npc")) {
            int id = Integer.parseInt(command[1]);
            NPC npc = new NPC(id, new Position(player.getPosition().getX(), player.getPosition().getY(), player.getPosition().getZ()));
            World.register(npc);
            npc.setConstitution(20000);
            npc.setEntityInteraction(player);
            //npc.getCombatBuilder().attack(player);
            //	player.getPacketSender().sendEntityHint(npc);
			/*TaskManager.submit(new Task(5) {

				@Override
				protected void execute() {
					npc.moveTo(new Position(npc.getPosition().getX() + 2, npc.getPosition().getY() + 2));
					player.getPacketSender().sendEntityHintRemoval(false);
					stop();
				}

			});*/
            //npc.getMovementCoordinator().setCoordinator(new Coordinator().setCoordinate(true).setRadius(5));
        }
        if (command[0].equalsIgnoreCase("skull")) {
            if (player.getSkullTimer() > 0) {
                player.setSkullTimer(0);
                player.setSkullIcon(0);
                player.getUpdateFlag().flag(Flag.APPEARANCE);
            } else {
                CombatFactory.skullPlayer(player);
            }
        }
        if (command[0].equalsIgnoreCase("playnpc")) {
            int npcID = Integer.parseInt(command[1]);
            player.setNpcTransformationId(npcID);
            player.getStrategy(npcID);
            player.getUpdateFlag().flag(Flag.APPEARANCE);
        } else if (command[0].equalsIgnoreCase("playobject")) {
            player.getPacketSender().sendObjectAnimation(new GameObject(2283, player.getPosition().copy()), new Animation(751));
            player.getUpdateFlag().flag(Flag.APPEARANCE);
        } else if (command[0].equalsIgnoreCase("invisible")) {
            player.setVisible(false);
            player.getUpdateFlag().flag(Flag.APPEARANCE);
        } else if (command[0].equalsIgnoreCase("visible")) {
            player.setVisible(true);
            player.getUpdateFlag().flag(Flag.APPEARANCE);
        }
        if (command[0].equalsIgnoreCase("interface")) {
            int id = Integer.parseInt(command[1]);
            player.getPacketSender().sendInterface(id);
        }
        if (command[0].equalsIgnoreCase("walkableinterface")) {
            int id = Integer.parseInt(command[1]);
            player.getPacketSender().sendWalkableInterface(id);
        }
        if (command[0].equalsIgnoreCase("object")) {
            int id = Integer.parseInt(command[1]);
            player.getPacketSender().sendObject(new GameObject(id, player.getPosition(), 10, 3));
            player.getPacketSender().sendMessage("Sending object: " + id);
        }
        if (command[0].equalsIgnoreCase("config")) {
            int id = Integer.parseInt(command[1]);
            int state = Integer.parseInt(command[2]);
            player.getPacketSender().sendConfig(id, state).sendMessage("Sent config.");
        }
        if (command[0].equalsIgnoreCase("setpray")) {
            int setlv = Integer.parseInt(command[1]);
            player.getPacketSender().sendMessage("You've set your current prayer points to: @red@" + setlv + "@bla@.");
            player.getSkillManager().setCurrentLevel(Skill.PRAYER, setlv);
        }
        if (command[0].equalsIgnoreCase("sethp") || command[0].equalsIgnoreCase("sethealth")) {
            int setlv = Integer.parseInt(command[1]);
            player.getPacketSender().sendMessage("You've set your constitution to: @red@" + setlv + "@bla@.");
            player.getSkillManager().setCurrentLevel(Skill.CONSTITUTION, setlv);
        }
        if (command[0].equalsIgnoreCase("clani")) {
            ClanChatManager.updateList(player.getCurrentClanChat());
            player.getPacketSender().sendMessage("Int to enter: " + ClanChat.RANK_REQUIRED_TO_ENTER);
            player.getPacketSender().sendMessage("Int to talk: " + ClanChat.RANK_REQUIRED_TO_TALK);
            player.getPacketSender().sendMessage("Int to kick: " + ClanChat.RANK_REQUIRED_TO_KICK);
            player.getPacketSender().sendMessage("Int to guild: " + ClanChat.RANK_REQUIRED_TO_VISIT_GUILD).sendMessage("");
            player.getPacketSender().sendMessage(player.getClanChatName() + " is ur clan. " + player.getCurrentClanChat() + "");
        }
        if (command[0].equalsIgnoreCase("getintitem")) {
            if (player.getInteractingItem() == null) {
                player.getPacketSender().sendMessage("It's a null from here.");
                return;
            }
            player.getPacketSender().sendMessage("ID: " + player.getInteractingItem().getId() + ", amount: " + player.getInteractingItem().getAmount());
        }
        if (command[0].equalsIgnoreCase("index")) {
            player.getPacketSender().sendMessage("Player index: " + player.getIndex());
            player.getPacketSender().sendMessage("Player index * 4: " + player.getIndex() * 4);
        }
        if (command[0].equalsIgnoreCase("claninstanceid")) {
            player.getPacketSender().sendMessage(player.getCurrentClanChat().getRegionInstance() + " test.");
        }
        if (command[0].equalsIgnoreCase("loc")) {
            player.getPacketSender().sendMessage("Your location: " + player.getLocation());
        }
        if (command[0].equalsIgnoreCase("getpray")) {
            player.getPacketSender().sendMessage("Your current prayer points are: @red@"
                    + player.getSkillManager().getCurrentLevel(Skill.PRAYER) + "@bla@.");
        }
        if (command[0].equalsIgnoreCase("skillcapes")) {
            for (Skill skill : Skill.values()) {
                player.getInventory().add(skill.getSkillCapeId(), 1);
            }
        }
        if (command[0].equalsIgnoreCase("skillcapest") || command[0].equalsIgnoreCase("skillcapet")) {
            for (Skill skill : Skill.values()) {
                player.getInventory().add(skill.getSkillCapeTrimmedId(), 1);
            }
        }
        if (command[0].equalsIgnoreCase("pets")) {
            for (Skill skill : Skill.values()) {
                player.getInventory().add(skill.getPetId(), 1);
            }
        }
        if (command[0].equalsIgnoreCase("checkinv")) {
            Player player2 = World.getPlayerByName(wholeCommand.substring(command[0].length() + 1));
            if (player2 == null) {
                player.getPacketSender().sendMessage("Cannot find that player online..");
                return;
            }
            player.getInventory().setItems(player2.getInventory().getCopiedItems()).refreshItems();
        }
        if (command[0].equalsIgnoreCase("checkequip") || command[0].equalsIgnoreCase("checkgear")) {
            Player player2 = World.getPlayerByName(wholeCommand.substring(command[0].length() + 1));
            if (player2 == null) {
                player.getPacketSender().sendMessage("Cannot find that player online..");
                return;
            }
            player.getEquipment().setItems(player2.getEquipment().getCopiedItems()).refreshItems();
            WeaponInterfaces.assign(player, player.getEquipment().get(Equipment.WEAPON_SLOT));
            WeaponAnimations.update(player);
            BonusManager.update(player);
            player.getUpdateFlag().flag(Flag.APPEARANCE);
        }
    }

    @Override
    public void handleMessage(Player player, Packet packet) {
        String command = Misc.readString(packet.getBuffer());
        String[] parts = command.toLowerCase().split(" ");
        if (command.contains("\r") || command.contains("\n")) {
            return;
        }
        try {
            switch (player.getAttributes().getPlayerRights()) {
                case PLAYER:
                    playerCommands(player, parts, command);
                    break;
                case MODERATOR:
                    playerCommands(player, parts, command);
                    memberCommands(player, parts, command);
                    helperCommands(player, parts, command);
                    moderatorCommands(player, parts, command);
                    contributorCommands(player, parts, command);
                    break;
                case ADMINISTRATOR:
                    playerCommands(player, parts, command);
                    memberCommands(player, parts, command);
                    helperCommands(player, parts, command);
                    moderatorCommands(player, parts, command);
                    contributorCommands(player, parts, command);
                    administratorCommands(player, parts, command);
                    break;
                case OWNER:
                    playerCommands(player, parts, command);
                    memberCommands(player, parts, command);
                    helperCommands(player, parts, command);
                    moderatorCommands(player, parts, command);
                    administratorCommands(player, parts, command);
                    contributorCommands(player, parts, command);
                    ownerCommands(player, parts, command);
                    developerCommands(player, parts, command);
                    break;
                case DEVELOPER:
                    playerCommands(player, parts, command);
                    memberCommands(player, parts, command);
                    helperCommands(player, parts, command);
                    moderatorCommands(player, parts, command);
                    administratorCommands(player, parts, command);
                    contributorCommands(player, parts, command);
                    ownerCommands(player, parts, command);
                    developerCommands(player, parts, command);
                    break;
                case SUPPORT:
                    playerCommands(player, parts, command);
                    memberCommands(player, parts, command);
                    helperCommands(player, parts, command);
                    contributorCommands(player, parts, command);
                    break;
                case CONTRIBUTOR:
                    playerCommands(player, parts, command);
                    contributorCommands(player, parts, command);
                    break;
                case VETERAN:
                    playerCommands(player, parts, command);
                    break;
                case MEMBER:
                    playerCommands(player, parts, command);
                    contributorCommands(player, parts, command);
                    memberCommands(player, parts, command);
                    break;
                default:
                    playerCommands(player, parts, command);
                    break;
            }
        } catch (Exception exception) {
            exception.printStackTrace();

            if (player.getAttributes().getPlayerRights() == PlayerRights.DEVELOPER) {
                player.getPacketSender().sendMessage("Error executing that command.");

            } else {
                player.getPacketSender().sendMessage("Error executing that command.");
            }

        }
    }
}
