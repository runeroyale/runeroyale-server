package com.runeroyale.net.packet.impl;

import com.runeroyale.model.Item;
import com.runeroyale.net.packet.Packet;
import com.runeroyale.net.packet.PacketListener;
import com.runeroyale.world.entity.impl.player.Player;

/**
 * This packet listener is called when a player drops an item they
 * have placed in their inventory.
 *
 * @author relex lawl
 */

public class DropItemPacketListener implements PacketListener {

    public static void destroyItemInterface(Player player, Item item) {//Destroy item created by Remco
        player.setUntradeableDropItem(item);
        String[][] info = {//The info the dialogue gives
                {"Are you sure you want to discard this item?", "14174"},
                {"Yes.", "14175"}, {"No.", "14176"}, {"", "14177"},
                {"This item will vanish once it hits the floor.", "14182"}, {"You cannot get it back if discarded.", "14183"},
                {item.getDefinition().getName(), "14184"}};
        player.getPacketSender().sendItemOnInterface(14171, item.getId(), 0, item.getAmount());
        for (int i = 0; i < info.length; i++)
            player.getPacketSender().sendString(Integer.parseInt(info[i][1]), info[i][0]);
        player.getPacketSender().sendChatboxInterface(14170);
    }

    @Override
    public void handleMessage(Player player, Packet packet) {
        int id = packet.readUnsignedShortA();
        int interfaceIndex = packet.readUnsignedShort();
        int itemSlot = packet.readUnsignedShortA();
        if (player.getConstitution() <= 0 || player.getInterfaceId() > 0)
            return;
        if (itemSlot < 0 || itemSlot > player.getInventory().capacity())
            return;
        if (player.getConstitution() <= 0 || player.isTeleporting())
            return;
        Item item = player.getInventory().getItems()[itemSlot];
        if (item == null)
            return;
        if (item.getId() != id) {
            return;
        }
        player.getPacketSender().sendInterfaceRemoval();
        player.getCombatBuilder().cooldown(false);
        player.getCurrentRoom().dropItem(player, item, itemSlot);
    }
}
