package com.runeroyale.net.packet.impl;

import com.runeroyale.net.packet.Packet;
import com.runeroyale.net.packet.PacketListener;
import com.runeroyale.world.entity.impl.player.Player;

/**
 * This packet listener is called when a player's region has been loaded.
 *
 * @author relex lawl
 */

public class FinalizedMapRegionChangePacketListener implements PacketListener {

    @Override
    public void handleMessage(Player player, Packet packet) {
    }
}