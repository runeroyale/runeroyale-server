package com.runeroyale.net.packet.impl;

import com.runeroyale.model.definitions.NpcDefinition;
import com.runeroyale.net.packet.Packet;
import com.runeroyale.net.packet.PacketListener;
import com.runeroyale.world.entity.impl.player.Player;

public class ExamineNpcPacketListener implements PacketListener {

    @Override
    public void handleMessage(Player player, Packet packet) {
        int npc = packet.readShort();
        if (npc <= 0) {
            return;
        }
        NpcDefinition npcDef = NpcDefinition.forId(npc);
        if (npcDef != null) {
            player.getPacketSender().sendMessage(npcDef.getExamine());
        }
    }

}
