package com.runeroyale.net.packet.impl;

import com.runeroyale.model.container.impl.Inventory;
import com.runeroyale.net.packet.Packet;
import com.runeroyale.net.packet.PacketListener;
import com.runeroyale.world.entity.impl.player.Player;

/**
 * This packet listener is called when an item is dragged onto another slot.
 *
 * @author relex lawl
 */

public class SwitchItemSlotPacketListener implements PacketListener {

    @Override
    public void handleMessage(Player player, Packet packet) {
        if (player.getConstitution() <= 0)
            return;
        int interfaceId = packet.readLEShortA();
        packet.readByteC();
        int fromSlot = packet.readLEShortA();
        int toSlot = packet.readLEShort();
        switch (interfaceId) {
            case Inventory.INTERFACE_ID:
                if (fromSlot >= 0 && fromSlot < player.getInventory().capacity() && toSlot >= 0 && toSlot < player.getInventory().capacity() && toSlot != fromSlot) {
                    player.getInventory().swap(fromSlot, toSlot).refreshItems();
                }
                break;
        }
    }
}
