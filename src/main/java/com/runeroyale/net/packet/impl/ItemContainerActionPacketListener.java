package com.runeroyale.net.packet.impl;

import com.runeroyale.model.Animation;
import com.runeroyale.model.Flag;
import com.runeroyale.model.Item;
import com.runeroyale.model.container.impl.BeastOfBurden;
import com.runeroyale.model.container.impl.Equipment;
import com.runeroyale.model.container.impl.PriceChecker;
import com.runeroyale.model.definitions.WeaponAnimations;
import com.runeroyale.model.definitions.WeaponInterfaces;
import com.runeroyale.model.input.impl.EnterAmountToMakeSmithing;
import com.runeroyale.model.input.impl.EnterAmountToRemoveFromBob;
import com.runeroyale.model.input.impl.EnterAmountToStore;
import com.runeroyale.net.packet.Packet;
import com.runeroyale.net.packet.PacketListener;
import com.runeroyale.world.content.BonusManager;
import com.runeroyale.world.content.ItemDegrading;
import com.runeroyale.world.content.combat.CombatFactory;
import com.runeroyale.world.content.combat.magic.Autocasting;
import com.runeroyale.world.content.combat.weapon.CombatSpecial;
import com.runeroyale.world.content.skill.impl.crafting.Jewelry;
import com.runeroyale.world.content.skill.impl.smithing.EquipmentMaking;
import com.runeroyale.world.content.skill.impl.smithing.SmithingData;
import com.runeroyale.world.entity.impl.player.Player;

public class ItemContainerActionPacketListener implements PacketListener {

    public static final int FIRST_ITEM_ACTION_OPCODE = 145;
    public static final int SECOND_ITEM_ACTION_OPCODE = 117;
    public static final int THIRD_ITEM_ACTION_OPCODE = 43;
    public static final int FOURTH_ITEM_ACTION_OPCODE = 129;
    public static final int FIFTH_ITEM_ACTION_OPCODE = 135;
    public static final int SIXTH_ITEM_ACTION_OPCODE = 138;

    /**
     * Manages an item's first action.
     *
     * @param player The player clicking the item.
     * @param packet The packet to read values from.
     */
    private static void firstAction(Player player, Packet packet) {
        int interfaceId = packet.readShortA();
        int slot = packet.readShortA();
        int id = packet.readShortA();
        Item item = new Item(id);
        if (player.getAttributes().getPlayerRights().isDeveloperOnly()) {
            player.getPacketSender().sendMessage("firstAction itemContainer. IF: " + interfaceId + " slot: " + slot + ", id: " + id);
        }
        switch (interfaceId) {
            case Equipment.INVENTORY_INTERFACE_ID:
                item = slot < 0 ? null : player.getEquipment().getItems()[slot];
                if (item == null || item.getId() != id)
                    return;
                boolean stackItem = item.getDefinition().isStackable() && player.getInventory().getAmount(item.getId()) > 0;
                int inventorySlot = player.getInventory().getEmptySlot();
                if (inventorySlot != -1) {
                    Item itemReplacement = new Item(-1, 0);
                    player.getEquipment().setItem(slot, itemReplacement);
                    if (!stackItem)
                        player.getInventory().setItem(inventorySlot, item);
                    else
                        player.getInventory().add(item.getId(), item.getAmount());
                    BonusManager.update(player);
                    if (item.getDefinition().getEquipmentSlot() == Equipment.WEAPON_SLOT) {
                        WeaponInterfaces.assign(player, player.getEquipment().get(Equipment.WEAPON_SLOT));
                        WeaponAnimations.update(player);
                        if (player.getAutocastSpell() != null || player.isAutocast()) {
                            Autocasting.resetAutocast(player, true);
                            player.getPacketSender().sendMessage("Autocast spell cleared.");
                        }
                        player.setSpecialActivated(false);
                        CombatSpecial.updateBar(player);
                        if (player.hasStaffOfLightEffect()) {
                            player.setStaffOfLightEffect(-1);
                            player.getPacketSender().sendMessage("You feel the spirit of the Staff of Light begin to fade away...");
                        }
                    }
                    player.getEquipment().refreshItems();
                    player.getInventory().refreshItems();
                    player.getUpdateFlag().flag(Flag.APPEARANCE);
                } else {
                    player.getInventory().full();
                }
                break;
            case 4233:
                Jewelry.jewelryMaking(player, "RING", id, 1);
                break;
            case 4239:
                Jewelry.jewelryMaking(player, "NECKLACE", id, 1);
                break;
            case 4245:
                Jewelry.jewelryMaking(player, "AMULET", id, 1);
                break;
            case 1119: //smithing interface row 1
            case 1120: // row 2
            case 1121: // row 3
            case 1122: // row 4
            case 1123: // row 5
                int barsRequired = SmithingData.getBarAmount(item);
                Item bar = new Item(player.getSelectedSkillingItem(), barsRequired);
                int x = 1;
                if (x > (player.getInventory().getAmount(bar.getId()) / barsRequired))
                    x = (player.getInventory().getAmount(bar.getId()) / barsRequired);
                EquipmentMaking.smithItem(player, new Item(player.getSelectedSkillingItem(), barsRequired), new Item(item.getId(), SmithingData.getItemAmount(item)), x);
                break;
        }

        if (BeastOfBurden.beastOfBurdenSlot(interfaceId) >= 0) {
            if (player.getInterfaceId() == BeastOfBurden.INTERFACE_ID && player.getSummoning().getBeastOfBurden() != null) {
                player.getSummoning().getBeastOfBurden().switchItem(player.getInventory(), item, BeastOfBurden.beastOfBurdenSlot(interfaceId), false, true);
            }
        } else if (PriceChecker.priceCheckerSlot(interfaceId) >= 0) {
        }
    }

    /**
     * Manages an item's second action.
     *
     * @param player The player clicking the item.
     * @param packet The packet to read values from.
     */
    private static void secondAction(Player player, Packet packet) {
        int interfaceId = packet.readLEShortA();
        int id = packet.readLEShortA();
        int slot = packet.readLEShort();
        Item item = new Item(id);
        if (player.getAttributes().getPlayerRights().isDeveloperOnly()) {
            player.getPacketSender().sendMessage("secondAction itemContainer. IF: " + interfaceId + " slot: " + slot + ", id: " + id);
        }
        switch (interfaceId) {
            case BeastOfBurden.INTERFACE_ID:
                if (player.getInterfaceId() == BeastOfBurden.INTERFACE_ID && player.getSummoning().getBeastOfBurden() != null) {
                    if (item.getDefinition().isStackable()) {
                        player.getPacketSender().sendMessage("You cannot store stackable items.");
                        return;
                    }
                    player.getInventory().switchItem(player.getSummoning().getBeastOfBurden(), new Item(id, 5), slot, false, true);
                }
                break;
            case 4233:
                Jewelry.jewelryMaking(player, "RING", id, 5);
                break;
            case 4239:
                Jewelry.jewelryMaking(player, "NECKLACE", id, 5);
                break;
            case 4245:
                Jewelry.jewelryMaking(player, "AMULET", id, 5);
                break;
            case 1119: //smithing interface row 1
            case 1120: // row 2
            case 1121: // row 3
            case 1122: // row 4
            case 1123: // row 5
                int barsRequired = SmithingData.getBarAmount(item);
                Item bar = new Item(player.getSelectedSkillingItem(), barsRequired);
                int x = 5;
                if (x > (player.getInventory().getAmount(bar.getId()) / barsRequired))
                    x = (player.getInventory().getAmount(bar.getId()) / barsRequired);
                EquipmentMaking.smithItem(player, new Item(player.getSelectedSkillingItem(), barsRequired), new Item(item.getId(), SmithingData.getItemAmount(item)), x);
                break;
        }

        if (BeastOfBurden.beastOfBurdenSlot(interfaceId) >= 0) {
            if (player.getInterfaceId() == BeastOfBurden.INTERFACE_ID && player.getSummoning().getBeastOfBurden() != null) {
                player.getSummoning().getBeastOfBurden().switchItem(player.getInventory(), new Item(id, 5), BeastOfBurden.beastOfBurdenSlot(interfaceId), false, true);
            }
        } else if (PriceChecker.priceCheckerSlot(interfaceId) >= 0) {
        }
    }

    /**
     * Manages an item's third action.
     *
     * @param player The player clicking the item.
     * @param packet The packet to read values from.
     */
    private static void thirdAction(Player player, Packet packet) {
        int interfaceId = packet.readLEShort();
        int id = packet.readShortA();
        int slot = packet.readShortA();
        Item item1 = new Item(id);
        if (player.getAttributes().getPlayerRights().isDeveloperOnly()) {
            player.getPacketSender().sendMessage("thirdAction itemContainer. IF: " + interfaceId + " slot: " + slot + ", id: " + id);
        }
        if (player.getPowerUpHandler().operatePowerUp(player)) {
            return;
        }
        switch (interfaceId) {
            case Equipment.INVENTORY_INTERFACE_ID:
                if (!player.getEquipment().contains(id))
                    return;
                //player.getCostume().operate(player);
                switch (id) {
                    case 2550:
                        int recoilcharges = (ItemDegrading.maxRecoilCharges - player.getRecoilCharges());
                        player.getPacketSender().sendMessage("You have " + recoilcharges + " recoil " + (recoilcharges == 1 ? "charge" : "charges") + " remaining.");
                        break;
                    case 2568:
                        int forgingcharges = (ItemDegrading.maxForgingCharges - player.getForgingCharges());
                        player.getPacketSender().sendMessage("You have " + forgingcharges + " forging " + (forgingcharges == 1 ? "charge" : "charges") + " remaining.");
                        break;
                    case 1712: //glory start
                    case 1710:
                    case 1708:
                    case 1706: //glory end
                    case 11118: //cb brace start
                    case 11120:
                    case 11122:
                    case 11124: //cb brace end
                    case 2552: //duel start
                    case 2554:
                    case 2556:
                    case 2558:
                    case 2560:
                    case 2562:
                    case 2564:
                    case 2566: //duel end
                    case 3853: //games start
                    case 3855:
                    case 3857:
                    case 3859:
                    case 3861:
                    case 3863:
                    case 3865:
                    case 3867: //games end
                    case 11194: //digsite start
                    case 11193:
                    case 11192:
                    case 11191:
                    case 11190: //digsite start
                        //String jewelName = JewelryTeleports.stripName(id);
                        //JewelryTeleports.handleDialogue(player, id, JewelryTeleports.jewelIndex(jewelName));
                        player.getPacketSender().sendMessage("You cannot operate this item while wearing it.");
                        break;
                    //case 13738:
                    case 13740:
                    case 13742:
                        //case 13744:
                        if (player.isSpiritDebug()) {
                            player.getPacketSender().sendMessage("You toggle your Spirit Shield to not display specific messages.");
                            player.setSpiritDebug(false);
                        } else if (player.isSpiritDebug() == false) {
                            player.getPacketSender().sendMessage("You toggle your Spirit Shield to display specific messages.");
                            player.setSpiritDebug(true);
                        }
                        break;
                    case 4566:
                        player.performAnimation(new Animation(451));
                        break;
                    case 1704:
                        player.getPacketSender().sendMessage("Your amulet has run out of charges.");
                        break;
                    case 11126:
                        player.getPacketSender().sendMessage("Your bracelet has run out of charges.");
                        break;
                    case 9759:
                    case 9760:
                        if (player.getCombatBuilder().isAttacking() || player.getCombatBuilder().isBeingAttacked()) {
                            player.getPacketSender().sendMessage("You cannot configure this right now.");
                            return;
                        }
                        player.getPacketSender().sendInterfaceRemoval();
                        player.setBonecrushEffect(!player.getBonecrushEffect());
                        player.getPacketSender().sendMessage("<img=10> You have " + (player.getBonecrushEffect() ? "activated" : "disabled") + " your cape's Bonecrusher effect.");
                        break;
                    case 18508:
                    case 18509:
                        if (player.getCombatBuilder().isAttacking() || player.getCombatBuilder().isBeingAttacked()) {
                            player.getPacketSender().sendMessage("You cannot configure this right now.");
                            return;
                        }
                        player.getPacketSender().sendInterfaceRemoval();
                        break;
                    case 22052:
                    case 14019:
                    case 14022:
                    case 20081:
                        if (player.getCombatBuilder().isAttacking() || player.getCombatBuilder().isBeingAttacked()) {
                            player.getPacketSender().sendMessage("You cannot configure this right now.");
                            return;
                        }
                        player.getPacketSender().sendInterfaceRemoval();
                        break;
                    case 11283:
                        if (player.getDfsCharges() > 0) {
                            if (player.getCombatBuilder().isAttacking()) {
                                CombatFactory.handleDragonFireShield(player, player.getCombatBuilder().getVictim());
                            } else {
                                player.getPacketSender().sendMessage("You can only use this in combat.");
                            }
                        } else {
                            player.getPacketSender().sendMessage("Your shield doesn't have enough power yet. It has " + player.getDfsCharges() + "/20 dragon-fire charges.");
                        }
                        break;
                }
                break;
            case BeastOfBurden.INTERFACE_ID:
                if (player.getInterfaceId() == BeastOfBurden.INTERFACE_ID && player.getSummoning().getBeastOfBurden() != null) {
                    Item storeItem = new Item(id, 10);
                    if (storeItem.getDefinition().isStackable()) {
                        player.getPacketSender().sendMessage("You cannot store stackable items.");
                        return;
                    }
                    player.getInventory().switchItem(player.getSummoning().getBeastOfBurden(), storeItem, slot, false, true);
                }
                break;
            case 4233:
                Jewelry.jewelryMaking(player, "RING", id, 10);
                break;
            case 4239:
                Jewelry.jewelryMaking(player, "NECKLACE", id, 10);
                break;
            case 4245:
                Jewelry.jewelryMaking(player, "AMULET", id, 10);
                break;
            case 1119: //smithing interface row 1
            case 1120: // row 2
            case 1121: // row 3
            case 1122: // row 4
            case 1123: // row 5
                int barsRequired = SmithingData.getBarAmount(item1);
                Item bar = new Item(player.getSelectedSkillingItem(), barsRequired);
                int x = 10;
                if (x > (player.getInventory().getAmount(bar.getId()) / barsRequired))
                    x = (player.getInventory().getAmount(bar.getId()) / barsRequired);
                EquipmentMaking.smithItem(player, new Item(player.getSelectedSkillingItem(), barsRequired), new Item(item1.getId(), SmithingData.getItemAmount(item1)), x);
                break;
        }

        if (BeastOfBurden.beastOfBurdenSlot(interfaceId) >= 0) {
            if (player.getInterfaceId() == BeastOfBurden.INTERFACE_ID && player.getSummoning().getBeastOfBurden() != null) {
                player.getSummoning().getBeastOfBurden().switchItem(player.getInventory(), new Item(id, 10), BeastOfBurden.beastOfBurdenSlot(interfaceId), false, true);
            }
        }
    }

    /**
     * Manages an item's fourth action.
     *
     * @param player The player clicking the item.
     * @param packet The packet to read values from.
     */
    private static void fourthAction(Player player, Packet packet) {
        int slot = packet.readShortA();
        int interfaceId = packet.readShort();
        int id = packet.readShortA();
        if (player.getAttributes().getPlayerRights().isDeveloperOnly()) {
            player.getPacketSender().sendMessage("fourthAction itemContainer. IF: " + interfaceId + " slot: " + slot + ", id: " + id);
        }
        switch (interfaceId) {
            case 1119: //smithing interface row 1
            case 1120: // row 2
            case 1121: // row 3
            case 1122: // row 4
            case 1123: // row 5
                Item item111 = new Item(id);
                int barsRequired = SmithingData.getBarAmount(item111);
                Item bar = new Item(player.getSelectedSkillingItem(), barsRequired);
                int x = (player.getInventory().getAmount(bar.getId()) / barsRequired);
                EquipmentMaking.smithItem(player, new Item(player.getSelectedSkillingItem(), barsRequired), new Item(item111.getId(), SmithingData.getItemAmount(item111)), x);
                break;
            case BeastOfBurden.INTERFACE_ID:
                if (player.getInterfaceId() == BeastOfBurden.INTERFACE_ID && player.getSummoning().getBeastOfBurden() != null) {
                    Item storeItem = new Item(id, 29);
                    if (storeItem.getDefinition().isStackable()) {
                        player.getPacketSender().sendMessage("You cannot store stackable items.");
                        return;
                    }
                    player.getInventory().switchItem(player.getSummoning().getBeastOfBurden(), storeItem, slot, false, true);
                }
                break;
        }

        if (BeastOfBurden.beastOfBurdenSlot(interfaceId) >= 0) {
            if (player.getInterfaceId() == BeastOfBurden.INTERFACE_ID && player.getSummoning().getBeastOfBurden() != null) {
                player.getSummoning().getBeastOfBurden().switchItem(player.getInventory(), new Item(id, 29), BeastOfBurden.beastOfBurdenSlot(interfaceId), false, true);
            }
        }
    }

    /**
     * Manages an item's fifth action.
     *
     * @param player The player clicking the item.
     * @param packet The packet to read values from.
     */
    private static void fifthAction(Player player, Packet packet) {
        int slot = packet.readLEShort();
        int interfaceId = packet.readShortA();
        int id = packet.readLEShort();
        if (player.getAttributes().getPlayerRights().isDeveloperOnly()) {
            player.getPacketSender().sendMessage("fifthAction itemContainer. IF: " + interfaceId + " slot: " + slot + ", id: " + id);
        }
        switch (interfaceId) {
            case 1119: //smithing interface row 1
            case 1120: // row 2
            case 1121: // row 3
            case 1122: // row 4
            case 1123: // row 5
                System.out.println(player.getInterfaceId() + " is interfaceid");
                if (player.getInterfaceId() == 994) {
                    player.setInputHandling(new EnterAmountToMakeSmithing(id, slot));
                    player.getPacketSender().sendEnterAmountPrompt("How many would you like to smith?");
                }

			/*
			Item item111 = new Item(id);
			int barsRequired = SmithingData.getBarAmount(item111);
			Item bar = new Item(player.getSelectedSkillingItem(), barsRequired);
			int x = (player.getInventory().getAmount(bar.getId()) / barsRequired);
			EquipmentMaking.smithItem(player, new Item(player.getSelectedSkillingItem(), barsRequired), new Item(item111.getId(), SmithingData.getItemAmount(item111)), x);
			*/
                break;
            case BeastOfBurden.INTERFACE_ID:
                if (player.getInterfaceId() == BeastOfBurden.INTERFACE_ID && player.getSummoning().getBeastOfBurden() != null) {
                    Item storeItem = new Item(id, 10);
                    if (storeItem.getDefinition().isStackable()) {
                        player.getPacketSender().sendMessage("You cannot store stackable items.");
                        return;
                    }
                    player.setInputHandling(new EnterAmountToStore(id, slot));
                    player.getPacketSender().sendEnterAmountPrompt("How many would you like to store?");
                }
                break;
        }

        if (BeastOfBurden.beastOfBurdenSlot(interfaceId) >= 0) {
            if (player.getInterfaceId() == BeastOfBurden.INTERFACE_ID && player.getSummoning().getBeastOfBurden() != null) {
                player.setInputHandling(new EnterAmountToRemoveFromBob(id, slot));
                player.getPacketSender().sendEnterAmountPrompt("How many would you like to remove?");
            }
        } else if (PriceChecker.priceCheckerSlot(interfaceId) >= 0) {

        }
    }

    private static void sixthAction(Player player, Packet packet) {
        int interfaceId = packet.readShortA();
        int slot = packet.readShortA();
        int id = packet.readShortA();
        if (player.getAttributes().getPlayerRights().isDeveloperOnly()) {
            player.getPacketSender().sendMessage("sixthAction itemContainer. IF: " + interfaceId + " slot: " + slot + ", id: " + id);
        }
        switch (interfaceId) {
        }
    }

    @Override
    public void handleMessage(Player player, Packet packet) {
        if (player.getConstitution() <= 0)
            return;
        switch (packet.getOpcode()) {
            case FIRST_ITEM_ACTION_OPCODE:
                firstAction(player, packet);
                break;
            case SECOND_ITEM_ACTION_OPCODE:
                secondAction(player, packet);
                break;
            case THIRD_ITEM_ACTION_OPCODE:
                thirdAction(player, packet);
                break;
            case FOURTH_ITEM_ACTION_OPCODE:
                fourthAction(player, packet);
                break;
            case FIFTH_ITEM_ACTION_OPCODE:
                fifthAction(player, packet);
                break;
            case SIXTH_ITEM_ACTION_OPCODE:
                sixthAction(player, packet);
                break;
        }
    }
}
