package com.runeroyale.net.packet.impl;

import com.runeroyale.net.packet.Packet;
import com.runeroyale.net.packet.PacketListener;
import com.runeroyale.world.entity.impl.player.Player;

/**
 * This packet listener handles player's mouse click on the
 * "Click here to continue" option, etc.
 *
 * @author relex lawl
 */

public class DialoguePacketListener implements PacketListener {

    public static final int DIALOGUE_OPCODE = 40;

    @Override
    public void handleMessage(Player player, Packet packet) {
        switch (packet.getOpcode()) {
            case DIALOGUE_OPCODE:
                if (player.getDialogueIterator() != null) player.getDialogueIterator().progressDialogue();
                break;
        }
    }
}
