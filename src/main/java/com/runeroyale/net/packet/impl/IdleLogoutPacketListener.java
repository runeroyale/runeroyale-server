package com.runeroyale.net.packet.impl;

import com.runeroyale.model.PlayerRights;
import com.runeroyale.net.packet.Packet;
import com.runeroyale.net.packet.PacketListener;
import com.runeroyale.world.entity.impl.player.Player;

//CALLED EVERY 3 MINUTES OF INACTIVITY

public class IdleLogoutPacketListener implements PacketListener {

    @Override
    public void handleMessage(Player player, Packet packet) {
        if (player.getAttributes().getPlayerRights() == PlayerRights.MODERATOR || player.getAttributes().getPlayerRights() == PlayerRights.ADMINISTRATOR || player.getAttributes().getPlayerRights() == PlayerRights.OWNER || player.getAttributes().getPlayerRights() == PlayerRights.DEVELOPER)
            return;
		/*if(player.logout() && (player.getSkillManager().getSkillAttributes().getCurrentTask() == null || !player.getSkillManager().getSkillAttributes().getCurrentTask().isRunning())) {
			World.getPlayers().remove(player);
		}*/
        player.setInactive(true);
    }
}
