package com.runeroyale;

import com.runeroyale.util.ShutdownHook;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The starting point of Ruse.
 *
 * @author Gabriel
 * @author Samy
 */
public class GameServer {

    private static final GameLoader loader = new GameLoader(GameSettings.GAME_PORT);
    private static final Logger logger = Logger.getLogger("Ruse");
    private static boolean updating;

    public static void main(String[] params) {
        addShutdownHook(new ShutdownHook());
        try {
            logInformation("Initializing the loader...");
            initializeGameLoader();

            finalizeGameLoader();
            logInformation("The loader has finished loading utility tasks.");

            logInformation(GameSettings.RSPS_NAME + " is now online on port " + GameSettings.GAME_PORT + "!");
        } catch (Exception ex) {
            handleException(ex);
        }
    }

    private static void addShutdownHook(ShutdownHook shutdownHook) {
        Runtime.getRuntime().addShutdownHook(shutdownHook);
    }

    private static void handleException(Exception ex) {
        log(Level.SEVERE, "Could not start " + GameSettings.RSPS_NAME + "! Program terminated.", ex);

        exit();
    }

    private static void exit() {
        System.exit(1);
    }

    private static void log(Level level, String message, Exception ex) {
        logger.log(level, message, ex);
    }

    private static void finalizeGameLoader() throws IOException, InterruptedException {
        loader.finish();
    }

    private static void initializeGameLoader() {
        loader.init();
    }

    private static void logInformation(String message) {
        logger.info(message);
    }

    public static GameLoader getLoader() {
        return loader;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static boolean isUpdating() {
        return GameServer.updating;
    }

    public static void setUpdating(boolean updating) {
        GameServer.updating = updating;
    }
}