package com.runeroyale.util;

import com.runeroyale.GameServer;
import com.runeroyale.world.World;
import com.runeroyale.world.content.clan.ClanChatManager;
import com.runeroyale.world.entity.impl.player.Player;
import com.runeroyale.world.entity.impl.player.PlayerHandler;

import java.util.logging.Logger;

public class ShutdownHook extends Thread {

    /**
     * The ShutdownHook logger to print out information.
     */
    private static final Logger logger = Logger.getLogger(ShutdownHook.class.getName());

    @Override
    public void run() {
        logger.info("The shutdown hook is processing all required actions...");
        //World.savePlayers();
        GameServer.setUpdating(true);
        for (Player player : World.getPlayers()) {
            if (player != null) {
                //	World.deregister(player);
                PlayerHandler.handleLogout(player, false);
            }
        }
        ClanChatManager.save();
        logger.info("The shudown hook actions have been completed, shutting the server down...");
    }
}
