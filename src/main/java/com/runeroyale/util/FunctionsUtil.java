package com.runeroyale.util;

public class FunctionsUtil {
    public static int[] cases(int... cases) {
        return cases;
    }

    /**
     * label is just here for readability's sake
     */
    public static void runCase(String label, int[] cases, Runnable result, int id) {
        for (int aCase : cases) {
            if (aCase == id) {
                result.run();
                break;
            }
        }
    }
}
