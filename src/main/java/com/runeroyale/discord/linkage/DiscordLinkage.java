package com.runeroyale.discord.linkage;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.runeroyale.util.Misc;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static com.runeroyale.KotlinFunctions.withAppropriateFilePathing;

public class DiscordLinkage {
    public static void saveNewVerificationCode(String discordId, String verificationCode) {
        Gson gson = new Gson();

        List<VerificationPlayer> entries = gson.fromJson(getLinkageJson(), new TypeToken<List<VerificationPlayer>>() {
        }.getType());
        VerificationPlayer newEntry = new VerificationPlayer(discordId, verificationCode, null);

        entries.add(newEntry);

        saveJson(entries);
    }

    public static boolean alreadyHasVerificationCode(String discordId) {
        VerificationPlayer[] entries = new Gson().fromJson(getLinkageJson(), VerificationPlayer[].class);
        for (VerificationPlayer entry : entries) {
            if (entry.getDiscordId().equalsIgnoreCase(discordId)) {
                return true;
            }
        }
        return false;
    }

    public static String getVerificationCode(String discordId) {
        VerificationPlayer[] entries = new Gson().fromJson(getLinkageJson(), VerificationPlayer[].class);
        for (VerificationPlayer entry : entries) {
            if (entry.getDiscordId().equalsIgnoreCase(discordId)) {
                return entry.getVerificationCode();
            }
        }
        return null;
    }

    public static VerificationPlayer verify(String verificationCode, String username) {
        VerificationPlayer[] entries = new Gson().fromJson(getLinkageJson(), VerificationPlayer[].class);
        for (VerificationPlayer entry : entries) {
            if (entry.getVerificationCode().equalsIgnoreCase(verificationCode)) {
                entry.setUsername(username);
                saveJson(entries);
                return entry;
            }
        }
        return null;
    }

    private static boolean verificationCodeExists(String verificationCode) {
        VerificationPlayer[] entries = new Gson().fromJson(getLinkageJson(), VerificationPlayer[].class);
        for (VerificationPlayer entry : entries) {
            if (entry.getVerificationCode().equalsIgnoreCase(verificationCode)) {
                return true;
            }
        }
        return false;
    }

    private static String getLinkageJson() {
        String costumeJson = null;

        try {
            File file = new File(withAppropriateFilePathing("./data/def/json/discord-linkage.json"));

            List<String> allFileLines = Files.readAllLines(file.toPath());

            StringBuilder builder = new StringBuilder();

            for (String line : allFileLines) {
                builder.append(line);
            }

            costumeJson = builder.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return costumeJson;
    }

    private static void saveJson(Object object) {
        try (FileWriter writer = new FileWriter(withAppropriateFilePathing("./data/def/json/discord-linkage.json"))) {
            Gson builder = new GsonBuilder().setPrettyPrinting().create();
            writer.write(builder.toJson(object));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String generateVerificationCode() {
        String[] string = "abcdefghijklmnopqrstuvwxyz0123456789".split("");
        StringBuilder code = new StringBuilder();
        for (int i = 0; i < 9; i++) {
            code.append(Misc.randomElement(string));
        }
        if (verificationCodeExists(code.toString())) {
            return generateVerificationCode();
        }
        return code.toString();
    }

    public static boolean alreadyHasVerified(String discordId) {
        VerificationPlayer[] entries = new Gson().fromJson(getLinkageJson(), VerificationPlayer[].class);
        for (VerificationPlayer entry : entries) {
            if (entry.getDiscordId().equalsIgnoreCase(discordId) && entry.getUsername() != null) {
                return true;
            }
        }
        return false;
    }
}
