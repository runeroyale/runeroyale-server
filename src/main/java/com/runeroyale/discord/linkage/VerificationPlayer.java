package com.runeroyale.discord.linkage;

public class VerificationPlayer {
    private final String discordId;
    private String verificationCode;
    private String username;

    VerificationPlayer(String discordId, String verificationCode, String username) {
        this.discordId = discordId;
        this.verificationCode = verificationCode;
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDiscordId() {
        return discordId;
    }
}
