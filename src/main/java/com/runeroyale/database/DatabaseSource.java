package com.runeroyale.database;

import com.runeroyale.config.ConfigurationKt;
import com.runeroyale.config.DatabaseDetailDto;
import com.runeroyale.config.DatabaseDetailsKt;
import com.runeroyale.database.DatabaseDetails.DatabaseType;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseSource {

    private static final Logger logger = Logger.getLogger(DatabaseSource.class.getName());
    private static HashMap<DatabaseType, HikariDataSource> connectionPools = new HashMap<>();

    private DatabaseSource() {
        DatabaseDetails allDetails = new DatabaseDetails();
        allDetails.populate();


        for (Entry<DatabaseType, DatabaseDetailDto> details : allDetails.getDatabases().entrySet()) {
            logger.log(Level.INFO, "Configuring BoneCP SQL Connection Pool for MySQL Database `{0}`...",
                    details.getValue().getType());
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            HikariConfig config = new HikariConfig();
            config.setJdbcUrl("jdbc:mysql://" + DatabaseDetailsKt.getDatabase().getHost() + ":" + DatabaseDetailsKt.getDatabase().getPort() + "/"
                    + details.getValue().getDatabaseName() + "?serverTimezone=CST");
            config.setUsername(details.getValue().getUsername());
            config.setPassword(details.getValue().getPassword());
            config.addDataSourceProperty("cachePrepStmts", true);
            config.addDataSourceProperty("prepStmtCacheSize", 250);
            config.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
            config.addDataSourceProperty("useServerPrepStmts", true);
            config.addDataSourceProperty("useLocalSessionState", true);
            config.addDataSourceProperty("rewriteBatchedStatements", true);
            config.addDataSourceProperty("cacheResultSetMetadata", true);
            config.addDataSourceProperty("cacheServerConfiguration", true);
            config.addDataSourceProperty("elideSetAutoCommits", true);
            config.addDataSourceProperty("maintainTimeStats", false);

            HikariDataSource connection = new HikariDataSource(config);
            connectionPools.put(details.getKey(), connection);
        }
        logger.log(Level.INFO, "Configured BoneCP SQL Connection Pool, total connection pools: {0}", connectionPools.size());
    }

    public static Connection getForumsConnection() throws SQLException {
        return getConnection(DatabaseType.FORUMS);
    }

    public static Connection getHighscoresConnection() throws SQLException {
        return getConnection(DatabaseType.HIGHSCORES);
    }

    public static Connection getVoteConnection() throws SQLException {
        return getConnection(DatabaseType.VOTE);
    }

    public static void loadConnection(boolean actuallyLoad) {
        if (actuallyLoad) {
            if (connectionPools == null || connectionPools.size() == 0) {
                new DatabaseSource();
            }
        }
    }

    private static Connection getConnection(DatabaseType type) throws SQLException {
        if (!ConfigurationKt.getConfig().getSqlEnabled()) {
            return null;
        }
        HikariDataSource connectionPool = getConnectionPool(type);
        return connectionPool.getConnection();
    }

    private static HikariDataSource getConnectionPool(DatabaseType type) {
        return connectionPools.get(type);
    }
}
