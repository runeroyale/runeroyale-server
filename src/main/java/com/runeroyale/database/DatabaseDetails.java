package com.runeroyale.database;

import com.runeroyale.config.DatabaseDetailDto;
import com.runeroyale.config.DatabaseDetailsKt;

import java.util.HashMap;

public class DatabaseDetails {
    private static HashMap<DatabaseType, DatabaseDetailDto> databases = new HashMap<>();

    public void populate() {
        for (DatabaseDetailDto databaseDetail : DatabaseDetailsKt.getDatabase().getDetails()) {
            DatabaseType type = DatabaseType.valueOf(databaseDetail.getType());
            databases.put(type, databaseDetail);
        }
    }

    public HashMap<DatabaseType, DatabaseDetailDto> getDatabases() {
        return databases;
    }

    public DatabaseDetailDto getHighscoresSingleton() {
        return databases.get(DatabaseType.HIGHSCORES);
    }

    public DatabaseDetailDto getVotingSingleton() {
        return databases.get(DatabaseType.VOTE);
    }

    public DatabaseDetailDto getForumsSingleton() {
        return databases.get(DatabaseType.FORUMS);
    }

    public enum DatabaseType {
        OTHER,
        FORUMS,
        STORE,
        VOTE,
        HIGHSCORES
    }
}