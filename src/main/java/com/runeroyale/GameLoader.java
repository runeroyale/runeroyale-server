package com.runeroyale;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.runeroyale.config.ConfigurationKt;
import com.runeroyale.database.DatabaseSource;
import com.runeroyale.discord.RuneRoyaleDiscordBot;
import com.runeroyale.engine.GameEngine;
import com.runeroyale.engine.task.TaskManager;
import com.runeroyale.engine.task.impl.ServerTimeUpdateTask;
import com.runeroyale.model.container.impl.Shop.ShopManager;
import com.runeroyale.model.definitions.ItemDefinition;
import com.runeroyale.model.definitions.NPCDrops;
import com.runeroyale.model.definitions.NpcDefinition;
import com.runeroyale.model.definitions.WeaponInterfaces;
import com.runeroyale.net.PipelineFactory;
import com.runeroyale.net.security.ConnectionHandler;
import com.runeroyale.world.clip.region.RegionClipping;
import com.runeroyale.world.content.CustomObjects;
import com.runeroyale.world.content.PlayerPunishment;
import com.runeroyale.world.content.Scoreboards;
import com.runeroyale.world.content.clan.ClanChatManager;
import com.runeroyale.world.content.combat.effect.CombatPoisonEffect.CombatPoisonData;
import com.runeroyale.world.content.combat.strategy.CombatStrategies;
import com.runeroyale.world.entity.impl.npc.NPC;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.util.HashedWheelTimer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Credit: lare96, Gabbe
 */
public final class GameLoader {

    private final ExecutorService serviceLoader = Executors.newSingleThreadExecutor(new ThreadFactoryBuilder().setNameFormat("GameLoadingThread").build());
    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor(new ThreadFactoryBuilder().setNameFormat("GameThread").build());
    private final GameEngine engine;
    private final int port;

    protected GameLoader(int port) {
        this.port = port;
        this.engine = new GameEngine();
    }

    public void init() {
        Preconditions.checkState(!serviceLoader.isShutdown(), "The bootstrap has been bound already!");
        executeServiceLoad();
        serviceLoader.shutdown();
    }

    public void finish() throws IOException, InterruptedException {
        if (!serviceLoader.awaitTermination(15, TimeUnit.MINUTES))
            throw new IllegalStateException("The background service load took too long!");
        ExecutorService networkExecutor = Executors.newCachedThreadPool();
        ServerBootstrap serverBootstrap = new ServerBootstrap(new NioServerSocketChannelFactory(networkExecutor, networkExecutor));
        serverBootstrap.setPipelineFactory(new PipelineFactory(new HashedWheelTimer()));
        serverBootstrap.bind(new InetSocketAddress(port));
        executor.scheduleAtFixedRate(engine, 0, GameSettings.ENGINE_PROCESSING_CYCLE_RATE, TimeUnit.MILLISECONDS);
        TaskManager.submit(new ServerTimeUpdateTask());
    }

    private void executeServiceLoad() {
		/*if (GameSettings.MYSQL_ENABLED) {
			serviceLoader.execute(() -> MySQLController.init());
		}*/
        serviceLoader.execute(() -> ConnectionHandler.init());
        serviceLoader.execute(() -> PlayerPunishment.init());
        serviceLoader.execute(() -> RegionClipping.init());
        serviceLoader.execute(() -> CustomObjects.init());
        serviceLoader.execute(() -> ItemDefinition.init());
        serviceLoader.execute(() -> Scoreboards.init());
        serviceLoader.execute(() -> ClanChatManager.init());
        serviceLoader.execute(() -> CombatPoisonData.init());
        serviceLoader.execute(() -> CombatStrategies.init());
        serviceLoader.execute(() -> NpcDefinition.parseNpcs().load());
        serviceLoader.execute(() -> NPCDrops.parseDrops().load());
        serviceLoader.execute(() -> WeaponInterfaces.parseInterfaces().load());
        serviceLoader.execute(() -> ShopManager.parseShops().load());
        serviceLoader.execute(() -> NPC.init());
        serviceLoader.execute(() -> RuneRoyaleDiscordBot.main(new String[]{String.valueOf(ConfigurationKt.getConfig().getBotEnabled())}));
        serviceLoader.execute(() -> DatabaseSource.loadConnection(ConfigurationKt.getConfig().getSqlEnabled()));
    }

    public GameEngine getEngine() {
        return engine;
    }
}