package com.runeroyale.engine.task.impl;

import com.runeroyale.engine.task.Task;
import com.runeroyale.model.Locations;
import com.runeroyale.util.Misc;
import com.runeroyale.world.World;

/**
 * @author Gabriel Hannason
 */
public class ServerTimeUpdateTask extends Task {

    private int tick = 0;

    public ServerTimeUpdateTask() {
        super(40);
    }

    @Override
    protected void execute() {
        World.updateServerTime();

        if (tick >= 6 && (Locations.PLAYERS_IN_WILD >= 3 || Locations.PLAYERS_IN_DUEL_ARENA >= 3)) {
            if (Locations.PLAYERS_IN_WILD > Locations.PLAYERS_IN_DUEL_ARENA || Misc.getRandom(3) == 1 && Locations.PLAYERS_IN_WILD >= 2) {
                World.sendMessage("<img=10> @blu@There are currently " + Locations.PLAYERS_IN_WILD + " players roaming the Wilderness!");
            }
            tick = 0;
        }

        tick++;
    }
}