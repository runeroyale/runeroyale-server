package com.runeroyale.engine.task.impl;

import com.runeroyale.engine.task.Task;
import com.runeroyale.model.*;
import com.runeroyale.world.World;
import com.runeroyale.world.content.PlayerPanel;
import com.runeroyale.world.entity.impl.npc.NPC;
import com.runeroyale.world.entity.impl.player.Player;

/**
 * Represents a player's death task, through which the process of dying is handled,
 * the animation, dropping items, etc.
 *
 * @author relex lawl, redone by Gabbe.
 */

public class PlayerDeathTask extends Task {

    Position oldPosition;
    NPC death;
    Player killer;
    private Player player;
    private int ticks = 5;

    /**
     * The PlayerDeathTask constructor.
     *
     * @param player The player setting off the task.
     */
    public PlayerDeathTask(Player player) {
        super(1, player, false);
        this.player = player;
    }

    @Override
    public void execute() {
        if (player == null) {
            stop();
            return;
        }
        try {
            switch (ticks) {
                case 5:
                    player.getPacketSender().sendInterfaceRemoval();
                    player.getMovementQueue().setLockMovement(true).reset();
                    break;
                case 3:
                    player.performAnimation(new Animation(0x900));
                    player.getPacketSender().sendMessage("Oh dear, you are dead!");
                    break;
                case 1:
                    this.oldPosition = player.getPosition().copy();
                    DamageDealer damageDealer = player.getCombatBuilder().getLastDamageDealer(true);
                    killer = damageDealer == null ? null : damageDealer.getPlayer();

                    player.getMovementQueue().setFollowCharacter(null);
                    player.getCombatBuilder().cooldown(false);

                    if (killer != null) {
                        killer.getPlayerKillingAttributes().add(player);
                        player.getPlayerKillingAttributes().setPlayerDeaths(player.getPlayerKillingAttributes().getPlayerDeaths() + 1);
                        PlayerPanel.refreshPanel(player);
                    }
                    player.getPacketSender().sendInterfaceRemoval();
                    player.setEntityInteraction(null);
                    player.setTeleporting(false);
                    player.setWalkToTask(null);
                    player.getSkillManager().stopSkilling();
                    break;
                case 0:
                    if (death != null) {
                        World.deregister(death);
                    }
                    player.restart();
                    player.getUpdateFlag().flag(Flag.APPEARANCE);
                    player.getCurrentRoom().handleDeath(killer, player);
                    player.getPowerUpHandler().handleKillPowerUp(killer, player, player.getCostume().getPowerUp());
                    player = null;
                    oldPosition = null;
                    stop();
                    break;
            }
            ticks--;
        } catch (Exception e) {
            setEventRunning(false);
            e.printStackTrace();
            if (player != null) {
                player.setConstitution(player.getSkillManager().getMaxLevel(Skill.CONSTITUTION));
            }
        }
    }

}
