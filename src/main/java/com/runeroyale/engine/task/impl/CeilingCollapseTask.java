package com.runeroyale.engine.task.impl;


import com.runeroyale.engine.task.Task;
import com.runeroyale.model.CombatIcon;
import com.runeroyale.model.Graphic;
import com.runeroyale.model.Hit;
import com.runeroyale.model.Hitmask;
import com.runeroyale.model.Locations.Location;
import com.runeroyale.util.Misc;
import com.runeroyale.world.entity.impl.player.Player;

/**
 * Barrows
 *
 * @author Gabriel Hannason
 */
public class CeilingCollapseTask extends Task {

    private Player player;

    public CeilingCollapseTask(Player player) {
        super(9, player, false);
        this.player = player;
    }

    @Override
    public void execute() {
        if (player == null || !player.isRegistered() || player.getLocation() != Location.KRAKEN || player.getLocation() != Location.ZULRAH || player.getPosition().getY() < 8000) {
            player.getPacketSender().sendCameraNeutrality();
            stop();
            return;
        }
        player.performGraphic(new Graphic(60));
        player.getPacketSender().sendMessage("Some rocks fall from the ceiling and hit you.");
        player.forceChat("Ouch!");
        player.dealDamage(new Hit(30 + Misc.getRandom(20), Hitmask.RED, CombatIcon.BLOCK));
    }
}
