package com.runeroyale.engine.task.impl;

import com.runeroyale.engine.task.Task;
import com.runeroyale.model.Skill;
import com.runeroyale.model.container.impl.Equipment;
import com.runeroyale.world.content.combat.prayer.PrayerHandler;
import com.runeroyale.world.entity.impl.player.Player;

public class PlayerRegenConstitutionTask extends Task {

    public static final int TIMER = 101;
    private Player player;

    public PlayerRegenConstitutionTask(Player player) {
        super(TIMER, player, false);
        this.player = player;
    }

    @Override
    public void execute() {
        if (player == null || !player.isRegistered()) {
            stop();
            return;
        }
        if (player.getConstitution() == 0 || player.isDying()) {
            return;
        }

        //int max = player.getSkillManager().getMaxLevel(Skill.CONSTITUTION);
        //int current = player.getSkillManager().getCurrentLevel(Skill.CONSTITUTION);
        int heal = 10;

        if (player.getEquipment().getItems()[Equipment.HANDS_SLOT].getId() == 11133) //regen bracelet
            heal *= 10;
        if (player.getSkillManager().skillCape(Skill.CONSTITUTION) || PrayerHandler.isActivated(player, PrayerHandler.RAPID_HEAL))
            heal *= 2;

        player.heal(heal);
		
		/*if (current < max) {
			
			if (current + heal > max) {
				player.getSkillManager().setCurrentLevel(Skill.CONSTITUTION, max);
			} else {
				player.getSkillManager().setCurrentLevel(Skill.CONSTITUTION, current+heal);
			}
			
		}*/

    }

}
