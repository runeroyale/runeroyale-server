package com.runeroyale.engine.task.impl;

import com.runeroyale.engine.task.Task;
import com.runeroyale.engine.task.TaskManager;
import com.runeroyale.model.Animation;
import com.runeroyale.model.DamageDealer;
import com.runeroyale.model.definitions.NPCDrops;
import com.runeroyale.world.World;
import com.runeroyale.world.content.KillsTracker;
import com.runeroyale.world.content.KillsTracker.KillsEntry;
import com.runeroyale.world.content.combat.strategy.impl.KalphiteQueen;
import com.runeroyale.world.content.combat.strategy.impl.Nex;
import com.runeroyale.world.content.combat.strategy.impl.ZulrahLogic;
import com.runeroyale.world.entity.impl.npc.NPC;
import com.runeroyale.world.entity.impl.player.Player;

/**
 * Represents an npc's death task, which handles everything
 * an npc does before and after their death animation (including it),
 * such as dropping their drop table items.
 *
 * @author relex lawl
 */

public class NPCDeathTask extends Task {

    /**
     * The npc setting off the death task.
     */
    private final NPC npc;
    /**
     * The amount of ticks on the task.
     */
    private int ticks = 2;
    /**
     * The player who killed the NPC
     */
    private Player killer = null;

    /**
     * The NPCDeathTask constructor.
     *
     * @param npc The npc being killed.
     */
    public NPCDeathTask(NPC npc) {
        super(2);
        this.npc = npc;
        this.ticks = 2;
    }

    @SuppressWarnings("incomplete-switch")
    @Override
    public void execute() {
        try {
            npc.setEntityInteraction(null);
            switch (ticks) {
                case 2:
                    npc.getMovementQueue().setLockMovement(true).reset();

                    DamageDealer damageDealer = npc.getCombatBuilder().getTopDamageDealer(true, null);
                    killer = damageDealer == null ? null : damageDealer.getPlayer();

                    if (!(npc.getId() >= 6142 && npc.getId() <= 6145) && !(npc.getId() > 5070 && npc.getId() < 5081))
                        npc.performAnimation(new Animation(npc.getDefinition().getDeathAnimation()));

                    /** CUSTOM NPC DEATHS **/
                    if (npc.getId() == 13447) {
                        Nex.handleDeath();
                    }

                    break;
                case 0:
                    if (killer != null) {

                        boolean boss = (npc.getDefaultConstitution() > 2000);
                        if (!Nex.nexMinion(npc.getId()) && npc.getId() != 1158 && !(npc.getId() >= 3493 && npc.getId() <= 3497)) {
                            KillsTracker.submit(killer, new KillsEntry(npc.getDefinition().getName(), 1, boss));
                            if (boss) {
                            }
                        }

                        switch (killer.getLastCombatType()) {

                        }

                        /** LOCATION KILLS **/
                        if (npc.getLocation().handleKilledNPC(killer, npc)) {
                            stop();
                            return;
                        }

                        /** PARSE DROPS **/
                        NPCDrops.dropItems(killer, npc);

                        /** SLAYER **/
                        killer.getSlayer().killedNpc(npc);
                    }
                    stop();
                    break;
            }
            ticks--;
        } catch (Exception e) {
            e.printStackTrace();
            stop();
        }
    }

    @Override
    public void stop() {
        setEventRunning(false);

        npc.setDying(false);

        //respawn
        if (npc.getDefinition().getRespawnTime() > 0) {
            TaskManager.submit(new NPCRespawnTask(npc, npc.getDefinition().getRespawnTime()));
        }

        World.deregister(npc);

        if (npc.getId() == 1158 || npc.getId() == 1160) {
            KalphiteQueen.death(npc.getId(), npc.getPosition());
        }
        if (Nex.nexMob(npc.getId())) {
            Nex.death(npc.getId());
        }
    }
}
