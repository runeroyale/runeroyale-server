package com.runeroyale.engine;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.runeroyale.engine.task.TaskManager;
import com.runeroyale.world.World;
import com.runeroyale.world.content.clan.ClanChatManager;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;
import java.util.concurrent.TimeUnit;

/**
 * @author lare96
 * @author Gabriel Hannason
 */
public final class GameEngine implements Runnable {

    private final ScheduledExecutorService logicService = GameEngine.createLogicService();

    /**
     * STATIC
     **/

    public static ScheduledExecutorService createLogicService() {
        ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
        executor.setRejectedExecutionHandler(new CallerRunsPolicy());
        executor.setThreadFactory(new ThreadFactoryBuilder().setNameFormat("LogicServiceThread").build());
        executor.setKeepAliveTime(45, TimeUnit.SECONDS);
        executor.allowCoreThreadTimeOut(true);
        return Executors.unconfigurableScheduledExecutorService(executor);
    }

    @Override
    public void run() {
        try {
            TaskManager.sequence();
            World.sequence();
        } catch (Throwable e) {
            e.printStackTrace();
            World.savePlayers();
            ClanChatManager.save();
        }
    }

    public void submit(Runnable t) {
        try {
            logicService.execute(t);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}