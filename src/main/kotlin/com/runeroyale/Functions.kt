@file:JvmName("KotlinFunctions")

package com.runeroyale

import com.runeroyale.engine.task.Task
import com.runeroyale.engine.task.TaskManager
import com.runeroyale.model.Item
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.math.BigInteger
import java.net.HttpURLConnection
import java.net.URL
import java.security.MessageDigest

fun executeTask(ticks: Int, immediate: Boolean = false, task: () -> Unit) {
    TaskManager.submit(object : Task(ticks, immediate) {
        override fun execute() {
            task()
        }
    })
}

fun Long.millisToTicks(): Int {
    return (this / 600).toInt()
}

fun Int.minutesToMillis(): Long {
    return (this * 60000).toLong()
}

fun Int.secondsToMillis(): Long {
    return (this * 1000).toLong()
}

val Int.asItem: Item
    get() = Item(this)

fun String.asMD5(): String {
    val m = MessageDigest.getInstance("MD5")
    m.reset()
    m.update(toByteArray())
    val digest = m.digest()
    val bigInt = BigInteger(1, digest)
    return bigInt.toString(16)
}

fun String.executeURL(action: (output: String) -> Unit) {
    try {
        val conn = URL(this).openConnection() as HttpURLConnection
        conn.connect()
        val `in` = conn.inputStream
        val reader = BufferedReader(InputStreamReader(`in`))
        val output = reader.readLine()
        action(output)
    } catch (e: IllegalStateException) {
    }
}

fun String.executeURL() {
    executeURL({})
}

fun String.withAppropriateFilePathing(): String {
    return replace("/", File.separator).replace("\\", File.separator)
}

fun <T> List<T>.getAllBut(index: Int): List<T> {
    val list = arrayListOf<T>()
    forEachIndexed { i, element -> if (i != index) list.add(element) }
    return list
}

fun <T> List<T>.getFrom(startIndex: Int): List<T> {
    val list = arrayListOf<T>()
    forEachIndexed { i, element -> if (i >= startIndex) list.add(element) }
    return list
}