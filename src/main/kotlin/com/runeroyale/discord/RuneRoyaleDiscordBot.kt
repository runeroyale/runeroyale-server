package com.runeroyale.discord

import com.runeroyale.discord.commands.HelpCommand
import com.runeroyale.discord.commands.LinkCommand
import com.runeroyale.discord.commands.SyncCommand
import de.btobastian.sdcf4j.handler.JDA3Handler
import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.JDABuilder
import net.dv8tion.jda.core.entities.Game

/**
 * @author Arham 4
 */
object RuneRoyaleDiscordBot {

    private const val TOKEN = "NDQzOTE3MzgxMzI5MjIzNjgx.DhC5Cg.zpkRgEMboI4tF5s2DJeq7re3ax8"

    @JvmStatic
    fun main(args: Array<String>) {
        val run = args[0].toBoolean()
        if (run) {
            BOT
        }
    }

    val BOT: JDA by lazy {
        fun registerListeners(registrants: () -> Unit) {
            registrants.invoke()
        }

        fun registerCommands(registrants: () -> Unit) {
            registrants.invoke()
        }

        val jda = JDABuilder(AccountType.BOT).setToken(TOKEN).buildAsync()
        val cmd = JDA3Handler(jda)

        registerListeners {
            //jda.addEventListener(MessageReceivedListener())
        }
        registerCommands {
            cmd.registerCommand(HelpCommand(cmd))
            cmd.registerCommand(LinkCommand())
            cmd.registerCommand(SyncCommand())
        }

        jda.presence.game = Game.playing("RuneRoyale")

        jda
    }
}