package com.runeroyale.discord

import net.dv8tion.jda.core.entities.TextChannel

private const val PRIVATE_CHANNEL = "451180549244059679"
private const val SPAM_CHANNEL = "455863277801570304"

fun isInSpam(channel: TextChannel): Boolean {
    return channel.id == SPAM_CHANNEL || channel.id == PRIVATE_CHANNEL
}