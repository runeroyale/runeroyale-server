package com.runeroyale.discord.commands

import com.runeroyale.database.DatabaseSource
import com.runeroyale.database.performDatabaseFunction
import com.runeroyale.discord.DiscordRanks.Companion.getForumsIdForDiscordId
import com.runeroyale.discord.isInSpam
import com.runeroyale.getAllBut
import com.runeroyale.getFrom
import com.runeroyale.world.entity.impl.player.io.read.impl.PlayerJsonReader
import com.runeroyale.world.entity.impl.player.io.read.impl.PlayerLoader
import com.runeroyale.world.entity.impl.player.io.read.impl.PlayerValidator
import de.btobastian.sdcf4j.Command
import de.btobastian.sdcf4j.CommandExecutor
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Role
import net.dv8tion.jda.core.entities.TextChannel

class SyncCommand : CommandExecutor {

    @Command(aliases = ["::sync"], usage = "::sync IN GAME USERNAME", description = "Synchronizes your Discord ranks with the forums.")
    fun onSyncCommand(args: Array<String>, channel: TextChannel, member: Member): String {
        if (isInSpam(channel)) {
            if (args.isEmpty()) {
                return "You need to supply your in-game username to use this command!"
            }
            val username = args.wholeArgs()
            val playerLoader = PlayerLoader(PlayerJsonReader(), PlayerValidator())
            val attributes = playerLoader.getPlayerAttributesFromJson(username) ?: return "That account does not exist!"
            val forumsUID = attributes.forumsUID
            val discordId = attributes.discordId ?: return "You have not linked your Discord and in-game account!"
            if (discordId != member.user.id) {
                return "You aren't " + attributes.javaClass.getMethod("getUserName").defaultValue + "!"
            }
            if (forumsUID == -1) {
                return "You have not linked your forums and in-game account!"
            }
            var index = 0
            var primaryUsergroup = member.roles[index]
            var secondaryUsergroups = member.roles.getAllBut(index)
            while (getForumsIdForDiscordId(primaryUsergroup.id) == null) {
                primaryUsergroup = member.roles[++index]
                secondaryUsergroups = member.roles.getFrom(index + 1)
            }

            DatabaseSource.getForumsConnection().performDatabaseFunction("UPDATE mybb_users SET " +
                    "usergroup=${getForumsIdForDiscordId(primaryUsergroup.id)!!.toShort()}, " +
                    "additionalgroups='${secondaryUsergroups.toForumsString()}' WHERE uid=$forumsUID;")
            return "You have now been assigned ${primaryUsergroup.name} and all your secondary roles to your " +
                    "forums account!"
        }
        return ""
    }

    private fun List<Role>.toForumsString(): String {
        var forumsString = ""
        forEach { role -> if (getForumsIdForDiscordId(role.id) != null) forumsString += "${role.name}, " }
        return forumsString.substring(0, forumsString.length - 2)
    }

    private fun Array<String>.wholeArgs(): String {
        var output = ""
        forEach { args -> output += "$args " }
        return output.substring(0, output.length - 1)
    }
}