package com.runeroyale.discord.commands

import com.runeroyale.discord.isInSpam
import com.runeroyale.discord.linkage.DiscordLinkage
import de.btobastian.sdcf4j.Command
import de.btobastian.sdcf4j.CommandExecutor
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.entities.User

class LinkCommand : CommandExecutor {

    @Command(aliases = ["::link"], description = "Sends a secret code to the user to link their Discord account to their in-game account.")
    fun onLinkCommand(channel: TextChannel, user: User): String {
        if (isInSpam(channel)) {
            if (!DiscordLinkage.alreadyHasVerified(user.id)) {
                if (DiscordLinkage.alreadyHasVerificationCode(user.id)) {
                    user.openPrivateChannel().queue { channel ->
                        channel.sendMessage("You already have a verification code. In game, do the following " +
                                "command: ```::verify " + DiscordLinkage.getVerificationCode(user.id) + "```").queue()
                    }
                    return "You already have a verification code! <:arham:453672496311435275>"
                } else {
                    val verificationCode = DiscordLinkage.generateVerificationCode()
                    DiscordLinkage.saveNewVerificationCode(user.id, verificationCode)
                    user.openPrivateChannel().queue { channel ->
                        channel.sendMessage("In game, do the following " +
                                "command: ```::verify " + DiscordLinkage.getVerificationCode(user.id) + "```").queue()
                    }
                    return "Your verification code has been sent via PM to you."
                }
            } else {
                return "You have already verified your account! <:arham:453672496311435275>"
            }
        }
        return ""
    }
}