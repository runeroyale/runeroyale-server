package com.runeroyale.discord.commands

import com.runeroyale.discord.isInSpam
import de.btobastian.sdcf4j.Command
import de.btobastian.sdcf4j.CommandExecutor
import de.btobastian.sdcf4j.CommandHandler
import net.dv8tion.jda.core.entities.TextChannel

class HelpCommand(private val commandHandler: CommandHandler) : CommandExecutor {

    @Command(aliases = ["::help", "::commands"], description = "Displays this page.")
    fun onHelpCommand(channel: TextChannel): String {
        if (isInSpam(channel)) {
            val builder = StringBuilder()
            builder.append("```xml")
            for (simpleCommand in commandHandler.commands) {
                if (!simpleCommand.commandAnnotation.showInHelpPage) {
                    continue
                }
                builder.append("\n")
                if (!simpleCommand.commandAnnotation.requiresMention) {
                    builder.append(commandHandler.defaultPrefix)
                }
                var usage = simpleCommand.commandAnnotation.usage
                if (usage.isEmpty()) {
                    usage = simpleCommand.commandAnnotation.aliases[0]
                }
                builder.append(usage)
                val description = simpleCommand.commandAnnotation.description
                if (description != "none") {
                    builder.append(" | ").append(description)
                }
            }
            builder.append("\n```")
            return builder.toString()
        }
        return ""
    }

}