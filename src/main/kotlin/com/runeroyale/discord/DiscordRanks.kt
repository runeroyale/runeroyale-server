package com.runeroyale.discord

enum class DiscordRanks(val discordId: String, val forumsId: Int) {
    ADMINISTRATOR("449734411710103552", 4),
    DEVELOPER("449733248046727178", 8),
    CONTRIBUTOR("451797258682368010", 9),
    VIP("449732848321036290", 10),
    SUPPORTER("449736046008205323", 11),
    ALPHA_TESTER("456904685929103360", 12);

    companion object {
        fun getForumsIdForDiscordId(discordId: String): Int? {
            values().forEach { if (discordId == it.discordId) return it.forumsId }
            return null
        }
    }
}