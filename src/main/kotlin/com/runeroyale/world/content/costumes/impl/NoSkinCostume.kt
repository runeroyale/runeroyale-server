package com.runeroyale.world.content.costumes.impl

import com.runeroyale.world.content.costumes.Costume
import com.runeroyale.world.content.costumes.Outfit

class NoSkinCostume : Costume("No Skin", Outfit.create { })