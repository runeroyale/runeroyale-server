package com.runeroyale.world.content.costumes

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.runeroyale.world.content.costumes.impl.ChickenCostume
import com.runeroyale.world.content.costumes.impl.NoSkinCostume
import com.runeroyale.world.content.costumes.impl.NoobCostume
import java.lang.reflect.Type


class CostumeDeserializer : JsonDeserializer<Costume> {
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Costume {
        val title = json.asJsonObject.get("title").asString
        return when (title) {
            "No Skin" -> context.deserialize<Costume>(json, NoSkinCostume::class.java)
            "Chicken" -> context.deserialize<Costume>(json, ChickenCostume::class.java)
            "Noob" -> context.deserialize<Costume>(json, NoobCostume::class.java)
            else -> throw IllegalArgumentException("Can't find costume!")
        }
    }
}