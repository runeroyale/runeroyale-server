package com.runeroyale.world.content.costumes

import com.runeroyale.world.entity.impl.player.Player

abstract class Costume(val title: String,
                       val outfit: Outfit,
                       val modifiers: CostumeModifiers = CostumeModifiers.create { }) {

    open fun operate(player: Player) {}
    open fun reset(player: Player) {}
}