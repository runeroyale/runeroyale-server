package com.runeroyale.world.content.costumes

import com.runeroyale.asItem
import com.runeroyale.model.Flag
import com.runeroyale.model.container.impl.Equipment
import com.runeroyale.world.entity.impl.player.Player

class Outfit private constructor(val helm: Int, val cape: Int, val amulet: Int, val platebody: Int, val platelegs: Int, val boots: Int, val gloves: Int) {

    private constructor(builder: Builder) : this(builder.helm, builder.cape, builder.amulet, builder.platebody, builder.platelegs, builder.boots, builder.gloves)

    companion object {
        fun create(init: Builder.() -> Unit) = Builder(init).build()
    }

    class Builder private constructor() {
        constructor(init: Builder.() -> Unit) : this() {
            init()
        }

        var helm: Int = -1
        var cape: Int = -1
        var amulet: Int = -1
        var platebody: Int = -1
        var platelegs: Int = -1
        var boots: Int = -1
        var gloves: Int = -1

        fun helm(init: Builder.() -> Int) = apply { helm = init() }
        fun cape(init: Builder.() -> Int) = apply { cape = init() }
        fun amulet(init: Builder.() -> Int) = apply { amulet = init() }
        fun platebody(init: Builder.() -> Int) = apply { platebody = init() }
        fun platelegs(init: Builder.() -> Int) = apply { platelegs = init() }
        fun boots(init: Builder.() -> Int) = apply { boots = init() }
        fun gloves(init: Builder.() -> Int) = apply { gloves = init() }

        fun build() = Outfit(this)
    }

    fun wear(player: Player) {
        println(helm)
        player.equipment.resetItems()
        player.equipment.set(Equipment.HEAD_SLOT, helm.asItem)
        player.equipment.set(Equipment.CAPE_SLOT, cape.asItem)
        player.equipment.set(Equipment.AMULET_SLOT, amulet.asItem)
        player.equipment.set(Equipment.BODY_SLOT, platebody.asItem)
        player.equipment.set(Equipment.LEG_SLOT, platelegs.asItem)
        player.equipment.set(Equipment.FEET_SLOT, boots.asItem)
        player.equipment.set(Equipment.HANDS_SLOT, gloves.asItem)
        player.updateFlag.flag(Flag.APPEARANCE)
        player.equipment.refreshItems()
    }
}
