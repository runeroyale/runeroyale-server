package com.runeroyale.world.content.costumes.impl

import com.runeroyale.executeTask
import com.runeroyale.millisToTicks
import com.runeroyale.minutesToMillis
import com.runeroyale.model.Flag
import com.runeroyale.secondsToMillis
import com.runeroyale.world.content.costumes.Costume
import com.runeroyale.world.content.costumes.Outfit
import com.runeroyale.world.entity.impl.player.Player

class ChickenCostume : Costume("Chicken", Outfit.create {
    helm = 11021
    cape = 1052
    amulet = 1731
    platebody = 11020
    platelegs = 11022
    boots = 11019
    gloves = 6629
}) {

    override fun operate(player: Player) {
        if (!player.lastChickenUsage.elapsed(5.minutesToMillis())) {
            player.packetSender.sendMessage("You can only turn invisible every 5 minutes.")
            return
        }
        player.isVisible = false
        player.updateFlag.flag(Flag.APPEARANCE)
        executeTask(5.secondsToMillis().millisToTicks(), task = {
            player.isVisible = true
            player.updateFlag.flag(Flag.APPEARANCE)
        })
        player.lastChickenUsage.reset()
    }

    override fun reset(player: Player) {
        player.lastChickenUsage.headStart(5.minutesToMillis())
    }
}