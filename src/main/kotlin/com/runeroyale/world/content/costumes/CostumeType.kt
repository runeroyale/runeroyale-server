package com.runeroyale.world.content.costumes

enum class CostumeType {
    BOUGHT, ROYALE_PASS, FREE_PASS
}