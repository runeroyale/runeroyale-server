package com.runeroyale.world.content.costumes

class CostumeModifiers private constructor(val damageDealt: Double, val damageReceived: Double, val initialHealth: Int) {

    private constructor(builder: Builder) : this(builder.damageDealt, builder.damageReceived, builder.initialHealth)

    companion object {
        fun create(init: Builder.() -> Unit) = Builder(init).build()
    }

    class Builder private constructor() {
        constructor(init: Builder.() -> Unit) : this() {
            init()
        }

        var damageDealt: Double = 1.0
        var damageReceived: Double = 1.0
        var initialHealth: Int = 99

        fun damageDealt(init: Builder.() -> Double) = apply { damageDealt = init() }
        fun damageReceived(init: Builder.() -> Double) = apply { damageReceived = init() }
        fun initialHealth(init: Builder.() -> Int) = apply { initialHealth = init() }

        fun build() = CostumeModifiers(this)
    }
}
