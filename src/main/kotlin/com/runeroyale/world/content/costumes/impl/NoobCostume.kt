package com.runeroyale.world.content.costumes.impl

import com.runeroyale.world.content.costumes.Costume
import com.runeroyale.world.content.costumes.Outfit

class NoobCostume : Costume("Noob", Outfit.create {
    helm = 1155
    cape = 1019
    amulet = 552
    platebody = 1117
    platelegs = 1075
    gloves = 1059
    boots = 1061
})