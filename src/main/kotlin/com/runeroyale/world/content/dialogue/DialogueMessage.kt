package com.runeroyale.world.content.dialogue

import com.runeroyale.model.definitions.NpcDefinition
import com.runeroyale.world.entity.impl.player.Player

abstract class DialogueMessage(val action: DialogueAction.() -> Unit) {
    open val message: String = ""
    abstract fun display(player: Player)
    fun entityDisplay(frameIds: Array<Int>, expression: DialogueExpression?, title: String, player: Player, headDisplay: (Int) -> Unit) {
        val split = message.splitDialogue(this)
        val startingId = frameIds[split.size - 1]
        val nameId = startingId - 1
        val headId = startingId - 2
        val interfaceId = startingId - 3
        split.forEachIndexed { index, line -> player.packetSender.sendString(startingId + index, line) }
        headDisplay(headId)
        player.packetSender.sendString(nameId, title)
        if (expression != null) {
            player.packetSender.sendInterfaceAnimation(headId, expression.animation)
        }
        player.packetSender.sendChatboxInterface(interfaceId)
    }
}

class PlayerMessage(override val message: String, private val expression: DialogueExpression, action: DialogueAction.() -> Unit) : DialogueMessage(action) {
    private val frameIds = arrayOf(971, 976, 982, 989)
    override fun display(player: Player) = entityDisplay(frameIds, expression, player.attributes.javaClass.getMethod("getUserName").defaultValue as String, player) { headId ->
        player.packetSender.sendPlayerHeadOnInterface(headId)
    }
}

class NPCMessage(override val message: String, private val npcId: Int, private val expression: DialogueExpression, action: DialogueAction.() -> Unit) : DialogueMessage(action) {
    private val frameIds = arrayOf(4885, 4890, 4896, 4903)
    override fun display(player: Player) = entityDisplay(frameIds, expression, NpcDefinition.forId(npcId).name, player) { headId ->
        player.packetSender.sendNpcHeadOnInterface(npcId, headId)
    }
}

class ItemMessage(private val itemId: Int, private val title: String, override val message: String, action: DialogueAction.() -> Unit) : DialogueMessage(action) {
    private val frameIds = arrayOf(4885, 4890, 4896, 4903)
    override fun display(player: Player) = entityDisplay(frameIds, null, title, player) { headId ->
        player.packetSender.sendInterfaceModel(headId, itemId, 150) // not sure if zoom should be 150
    }
}

class PlainMessage(override val message: String, val continueText: String = "", val noContinue: Boolean, action: DialogueAction.() -> Unit) : DialogueMessage(action) {
    private val frameIds = arrayOf(357, 360, 364, 369, 375)
    private val frameIdsNoContinue = arrayOf(12789, 12791, 12794, 12798, 12803)
    override fun display(player: Player) {
        val split = message.splitDialogue(this)
        val startingId = if (noContinue) frameIdsNoContinue[split.size - 1] else frameIds[split.size - 1]
        val interfaceId = startingId - 1
        split.forEachIndexed { index, line ->
            player.packetSender.sendString(startingId + index, line)
        }
        if (!noContinue) {
            player.packetSender.sendString(startingId + split.size, continueText)
        }
        player.packetSender.sendChatboxInterface(interfaceId)

    }
}

/**
 * An empty option lambda means that the dialogue will default to closing.
 */
class OptionMessage(vararg val options: String,
                    val option1: Dialogue.() -> Unit, val option2: Dialogue.() -> Unit,
                    val option3: Dialogue.() -> Unit, val option4: Dialogue.() -> Unit,
                    val option5: Dialogue.() -> Unit, action: DialogueAction.() -> Unit,
                    val title: String = "Choose an option") : DialogueMessage(action) {
    val frameIds = arrayOf(13760, 2461, 2471, 2482, 2494)
    override fun display(player: Player) {
        val startingId = frameIds[options.size - 1]
        val titleId = startingId - 1
        val interfaceId = startingId - 2
        player.packetSender.sendString(titleId, title)
        options.forEachIndexed { index, option -> player.packetSender.sendString(startingId + index, option) }
        player.packetSender.sendChatboxInterface(interfaceId)
    }
}

/**
 * If one wants a custom close action (for example, giving an item AFTER the dialogue is finished), then they must
 * add a close message to the queue themselves. By default, a dialogue ends when the queue is empty, with no action
 * being commenced.
 */
class CloseMessage(action: DialogueAction.() -> Unit = {}) : DialogueMessage(action) {
    override fun display(player: Player) {
        player.packetSender.sendInterfaceRemoval()
        player.dialogueIterator = null
    }
}

/**
 * A custom made splitDialogue extension function to determine the splits in dialogue itself rather than having to go through
 * trial and error to splitDialogue the lines of messages to fit the message frame. One can make new lines themselves
 * by adding "\n" to the location.
 */
fun String.splitDialogue(dialogueMessageType: DialogueMessage): Array<String> {
    val message = this
    val messageSplit = ArrayList<String>()
    var currentLine = ""
    var chars = 0
    val limit = if (dialogueMessageType is PlainMessage) 75 else 50
    for (word in message.split(" ")) {
        val length = word.length + 2
        if (word.contains("\n")) {
            chars = limit * (messageSplit.size + 1)
            messageSplit.add("$currentLine ${word.replace("\n", "")}")
            currentLine = ""
        } else {
            if (length + chars >= limit * (messageSplit.size + 1)) {
                messageSplit.add(currentLine)
                currentLine = word
            } else {
                currentLine += " $word"
            }
            chars += length
        }
    }
    if (currentLine.isNotEmpty()) {
        messageSplit.add(currentLine)
    }
    return messageSplit.toTypedArray()
}