package com.runeroyale.world.content.dialogue

import com.runeroyale.world.entity.impl.player.Player

object DialogueExecutor {
    @JvmStatic
    fun startDialogue(player: Player, dialogue: Dialogue) {
        player.dialogueIterator = makeIterator(player, dialogue)
        player.dialogueIterator.progressDialogue()
    }

    private fun makeIterator(player: Player, dialogue: Dialogue): DialogueIterator {
        return DialogueIterator(player, dialogue)
    }
}

class DialogueIterator(private val player: Player, private val dialogue: Dialogue) {
    private lateinit var currentMessage: DialogueMessage
    private var lastMessage: DialogueMessage? = null

    init {
        dialogue.execute()
    }

    fun progressDialogue() {
        assignCurrentMessage()
        currentMessage.display(player)
        currentMessage.action(DialogueAction)
        lastMessage = currentMessage
    }

    private fun assignCurrentMessage() {
        currentMessage = if (dialogue.queue.peek() != null) dialogue.queue.poll() else CloseMessage()
    }

    fun handleWalk() {
        if (dialogue.closeable) player.packetSender.sendInterfaceRemoval()
    }

    fun reloadDialogue() {
        dialogue.queue.clear()
        dialogue.execute()
        assignCurrentMessage()
        currentMessage.display(player)
    }

    fun forceCloseDialogue(player: Player) {
        currentMessage = CloseMessage()
        currentMessage.display(player)
    }

    fun handleOptionClick(buttonId: Int) {
        val optionMessage = currentMessage as OptionMessage
        val option1 = optionMessage.frameIds[optionMessage.options.size - 1]
        when (buttonId) {
            option1 -> optionMessage.option1(dialogue)
            option1 + 1 -> optionMessage.option2(dialogue)
            option1 + 2 -> optionMessage.option3(dialogue)
            option1 + 3 -> optionMessage.option4(dialogue)
            option1 + 4 -> optionMessage.option5(dialogue)
        }
        progressDialogue()
    }
}