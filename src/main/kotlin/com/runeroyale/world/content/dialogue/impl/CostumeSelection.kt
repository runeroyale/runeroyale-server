package com.runeroyale.world.content.dialogue.impl

import com.runeroyale.world.content.battleroyale.costumes.Costume
import com.runeroyale.world.content.dialogue.Dialogue
import com.runeroyale.world.entity.impl.player.Player

class CostumeSelection(player: Player, boughtCostumes: List<Costume>) : Dialogue(player) {

    val costumes = boughtCostumes

    override fun execute() {
        when {
            costumes.size > 4 -> showEquipCostumeDialogueWithNextPageOption(0)
            costumes.size == 4 -> showEquipCostumeDialogueWithFourCostumes(0)
            else -> showEquipCostumeDialogue(0)
        }
    }

    private fun showEquipCostumeDialogue(index: Int) {
        // todo add back option
        when {
            costumes.size - index == 3 -> optionMessage(costumes[0 + index].costumeName, costumes[1 + index].costumeName, costumes[2 + index].costumeName,
                    option1 = {
                        player.wearCostume(costumes[0 + index])
                    },
                    option2 = {
                        player.wearCostume(costumes[1 + index])
                    },
                    option3 = {
                        player.wearCostume(costumes[2 + index])
                    })
            costumes.size - index == 2 -> optionMessage(costumes[0 + index].costumeName, costumes[1 + index].costumeName,
                    option1 = {
                        player.wearCostume(costumes[0 + index])
                    },
                    option2 = {
                        player.wearCostume(costumes[1 + index])
                    })
            costumes.size - index == 1 -> optionMessage(costumes[0 + index].costumeName,
                    option1 = {
                        player.wearCostume(costumes[0 + index])
                    })
        }
    }

    private fun showEquipCostumeDialogueWithFourCostumes(index: Int) {
        // todo add back option
        optionMessage(costumes[0 + index].costumeName, costumes[1 + index].costumeName, costumes[2 + index].costumeName, costumes[3 + index].costumeName,
                option1 = {
                    player.wearCostume(costumes[0 + index])
                },
                option2 = {
                    player.wearCostume(costumes[1 + index])
                },
                option3 = {
                    player.wearCostume(costumes[2 + index])
                },
                option4 = {
                    player.wearCostume(costumes[3 + index])
                })
    }

    private fun showEquipCostumeDialogueWithNextPageOption(index: Int) {
        optionMessage(costumes[0 + index].costumeName, costumes[1 + index].costumeName, costumes[2 + index].costumeName, costumes[3 + index].costumeName, "Go To Next Page",
                option1 = {
                    player.wearCostume(costumes[0 + index])
                },
                option2 = {
                    player.wearCostume(costumes[1 + index])
                },
                option3 = {
                    player.wearCostume(costumes[2 + index])
                },
                option4 = {
                    player.wearCostume(costumes[3 + index])
                },
                option5 = {
                    when {
                        costumes.size - (3 + index) > 4 -> showEquipCostumeDialogueWithNextPageOption(index + 4)
                        costumes.size - (3 + index) == 4 -> showEquipCostumeDialogueWithFourCostumes(index + 4)
                        else -> showEquipCostumeDialogue(index + 4)
                    }
                })
    }
}