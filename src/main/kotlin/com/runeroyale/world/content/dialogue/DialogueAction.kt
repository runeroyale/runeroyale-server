package com.runeroyale.world.content.dialogue

import com.runeroyale.model.Animation
import com.runeroyale.model.Item


object DialogueAction {
    fun Dialogue.openNewDialogue(dialogue: Dialogue) {
        DialogueExecutor.startDialogue(player, dialogue)
    }

    fun Dialogue.giveItem(item: Item) {
        player.inventory.add(item)
    }

    fun Dialogue.giveItem(id: Int) {
        giveItem(Item(id))
    }

    fun Dialogue.performAnimation(animation: Animation) {
        player.performAnimation(animation)
    }

    fun Dialogue.performAnimation(id: Int) {
        performAnimation(Animation(id))
    }
}