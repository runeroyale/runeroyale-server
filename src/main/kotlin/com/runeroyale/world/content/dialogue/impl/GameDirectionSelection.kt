package com.runeroyale.world.content.dialogue.impl

import com.runeroyale.world.content.battleroyale.locations.CardinalDirection
import com.runeroyale.world.content.battleroyale.rooms.waitingroom.Transitionee
import com.runeroyale.world.content.battleroyale.rooms.waitingroom.WaitingRoomTransitioner
import com.runeroyale.world.content.dialogue.Dialogue
import com.runeroyale.world.entity.impl.player.Player

class GameDirectionSelection(player: Player, private val transitioner: WaitingRoomTransitioner)
    : Dialogue(player, closeable = false) {
    override fun execute() {
        var transitionee: Transitionee? = null
        // todo add something extra for duos if there team mate is going there.
        transitioner.run {
            optionMessage("NORTH ${northernPeople.withPeople()}", "EAST ${easternPeople.withPeople()}",
                    "SOUTH ${southernPeople.withPeople()}", "WEST ${westernPeople.withPeople()}",
                    title = "Map location: $gameLocation",
                    option1 = {
                        transitionee = addTransitionee(player, CardinalDirection.NORTH)
                        incrementNorthernPeople()
                    },
                    option2 = {
                        transitionee = addTransitionee(player, CardinalDirection.EAST)
                        incrementEasternPeople()
                    },
                    option3 = {
                        transitionee = addTransitionee(player, CardinalDirection.SOUTH)
                        incrementSouthernPeople()
                    },
                    option4 = {
                        transitionee = addTransitionee(player, CardinalDirection.WEST)
                        incrementWesternPeople()
                    })
            closeMessage {
                openNewDialogue(WaitingRoomWait(transitionee!!, transitioner))
                transitioner.updateDialogueForEveryone()
            }
        }
    }

    private fun Int.withPeople(): String {
        return if (this == 1) {
            "($this person)"
        } else {
            "($this people)"
        }
    }
}