package com.runeroyale.world.content.dialogue.impl

import com.runeroyale.world.content.Emotes
import com.runeroyale.world.content.dialogue.Dialogue
import com.runeroyale.world.entity.impl.player.Player

class Hans(player: Player) : Dialogue(player) {
    private val npcId = 0

    override fun execute() {
        playerMessage("Wow, this surprisingly works!") {
            performAnimation(Emotes.EmoteData.DANCE.animation)
        }
        npcMessage(npcId, "Yeah man, I didn't think the day would come. But it has, alas, and I am just waiting for one more thing: option dialogues.")
        optionMessage("Option 1", "Option 2", "Option 3",
                option1 = {
                    playerMessage("Hey man, this is option 1")
                    npcMessage(npcId, "Seems to have worked.") // makes weird head, need to see
                    playerMessage("Yes indeed.")
                    itemMessage(4151, message = "To items! Take a whip!") {
                        giveItem(4151)
                    }
                    playerMessage("Thank you!")
                    plainMessage("You proceed to express your gratitude... in other ways.")
                    plainMessage("Now, let's test the custom close dialogue by making you cry.")
                    closeMessage { performAnimation(Emotes.EmoteData.CRY.animation) }
                },
                option2 = {
                    playerMessage("Hey dude, this is option 2")
                    npcMessage(npcId, "I believe this works.. too?")
                    playerMessage("Yes indeed, now we need to see if option3 closes the dialogue.")
                }) // option 3 is not implemented to see if it will close the dialogue
    }
}