package com.runeroyale.world.content.dialogue.impl

import com.runeroyale.world.content.battleroyale.GameType
import com.runeroyale.world.content.battleroyale.locations.RoomLocations
import com.runeroyale.world.content.dialogue.Dialogue
import com.runeroyale.world.entity.impl.player.Player
import com.runeroyale.world.entity.impl.player.PlayerState

class LobbyPortal(player: Player, val mayEnter: Boolean) : Dialogue(player) {
    override fun execute() {
        if (mayEnter) {
            plainMessage("Select the game mode you would like to play.")
            optionMessage("SOLO", "DUOS",
                    option1 = {
                        movePlayerToWaitingRoom(player, GameType.SOLO)
                    },
                    option2 = {
                        movePlayerToWaitingRoom(player, GameType.DUOS)
                    })
        } else {
            plainMessage("There seems to be a problem with getting you in the game. Check your inventory space and ensure it is clear. If the problem persists, contact a staff member.")
        }
    }

    private fun movePlayerToWaitingRoom(player: Player, gameType: GameType) {
        player.moveTo(RoomLocations.WAITING_ROOM)
        player.state = PlayerState.IN_WAITING_ROOM
        player.gameType = gameType
    }
}