package com.runeroyale.world.content.dialogue.impl

import com.runeroyale.world.content.battleroyale.locations.CardinalDirection
import com.runeroyale.world.content.battleroyale.rooms.waitingroom.Transitionee
import com.runeroyale.world.content.battleroyale.rooms.waitingroom.WaitingRoomTransitioner
import com.runeroyale.world.content.dialogue.Dialogue

class WaitingRoomWait(val transitionee: Transitionee, val transitioner: WaitingRoomTransitioner)
    : Dialogue(transitionee.player, closeable = false) {
    override fun execute() {
        // todo add something extra for duos if there team mate is going there.
        plainMessage("Waiting for other players' responses...\n ${getPlayers().appropriateText()}" +
                " going the direction you are going.", continueText = "Back")
        closeMessage {
            transitioner.removeTransitionee(transitionee)
            decrementValue()
            openNewDialogue(GameDirectionSelection(player, transitioner))
            transitioner.updateDialogueForEveryone()
        }
    }

    private fun getPlayers(): Int {
        transitioner.run {
            return when (transitionee.directionChosen) {
                CardinalDirection.NORTH -> northernPeople
                CardinalDirection.EAST -> easternPeople
                CardinalDirection.SOUTH -> southernPeople
                CardinalDirection.WEST -> westernPeople
            }
        }
    }

    private fun Int.appropriateText(): String {
        return if (this == 1) {
            "Only you are"
        } else {
            "There are currently ${getPlayers()} players"
        }
    }

    private fun decrementValue() {
        transitioner.run {
            when (transitionee.directionChosen) {
                CardinalDirection.NORTH -> decrementNorthernPeople()
                CardinalDirection.EAST -> decrementEasternPeople()
                CardinalDirection.SOUTH -> decrementSouthernPeople()
                CardinalDirection.WEST -> decrementWesternPeople()
            }
        }
    }
}