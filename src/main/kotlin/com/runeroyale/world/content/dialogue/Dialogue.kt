package com.runeroyale.world.content.dialogue

import com.runeroyale.world.entity.impl.player.Player
import java.util.*

abstract class Dialogue(val player: Player, val closeable: Boolean = true) {
    val queue: Queue<DialogueMessage> = LinkedList<DialogueMessage>()

    abstract fun execute()

    fun playerMessage(message: String, expression: DialogueExpression = DialogueExpression.NORMAL, action: DialogueAction.() -> Unit = {}) {
        queue.add(PlayerMessage(message, expression, action))
    }

    fun npcMessage(npcId: Int, message: String, expression: DialogueExpression = DialogueExpression.NORMAL, action: DialogueAction.() -> Unit = {}) {
        queue.add(NPCMessage(message, npcId, expression, action))
    }

    fun itemMessage(itemId: Int, title: String = "", message: String, action: DialogueAction.() -> Unit = {}) {
        queue.add(ItemMessage(itemId, title, message, action))
    }

    fun plainMessage(message: String, continueText: String = "Click here to continue", action: DialogueAction.() -> Unit = {}) {
        queue.add(PlainMessage(message, continueText, noContinue = false, action = action))
    }

    fun plainMessageNoContinue(message: String, action: DialogueAction.() -> Unit = {}) {
        queue.add(PlainMessage(message, noContinue = true, action = action))
    }

    fun optionMessage(vararg options: String,
                      option1: Dialogue.() -> Unit = {}, option2: Dialogue.() -> Unit = {},
                      option3: Dialogue.() -> Unit = {}, option4: Dialogue.() -> Unit = {},
                      option5: Dialogue.() -> Unit = {}, action: DialogueAction.() -> Unit = {}, title: String = "Choose an option") {
        queue.add(OptionMessage(*options, option1 = option1, option2 = option2, option3 = option3, option4 = option4, option5 = option5, action = action, title = title))
    }

    fun closeMessage(action: DialogueAction.() -> Unit = {}) {
        queue.add(CloseMessage(action))
    }
}