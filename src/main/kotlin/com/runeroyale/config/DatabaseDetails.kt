package com.runeroyale.config

import com.gmail.arhamjsiddiqui.silentbot.data.YAMLParse

data class DatabaseDetailsDto(val host: String, val port: Int, val details: Array<DatabaseDetailDto>)
data class DatabaseDetailDto(val type: String, val databaseName: String, val username: String, val password: String)

val database: DatabaseDetailsDto = YAMLParse.parseDto("databases.yaml", DatabaseDetailsDto::class)!!