package com.runeroyale.config

import com.gmail.arhamjsiddiqui.silentbot.data.YAMLParse

data class ConfigDto(val botEnabled: Boolean, val sqlEnabled: Boolean)

val config: ConfigDto = YAMLParse.parseDto("config.yaml", ConfigDto::class)!!